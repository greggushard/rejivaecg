<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Network;
use App\Models\User;
use GeniusTS\Roles\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $networks = Network::all()->pluck('id');
        $roles = Role::all()->pluck('id');

        foreach (range(1,15000) as $index) {
            $i = str_random(12);
            $user = User::create([
                'name' => $faker->name,
                'email' => 'greggushard+' . $i . '@icloud.com',
                'password' => bcrypt('secret'),
            ]);

            $user->roles()->attach($roles->random(1));
            $user->networks()->attach($networks->random(1));

        }
    }

}
