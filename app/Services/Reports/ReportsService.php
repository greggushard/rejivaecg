<?php
namespace App\Services\Reports;

use App\Models\Alert;
use App\Models\Clip;
use PDF;
use Storage;
use Carbon\Carbon;
use App\Models\Report;
use Illuminate\Support\Facades\Auth;

/**
 * Class ReportsService
 * @package App\Services
 */
class ReportsService
{

    /**
     * @var Report
     */
    public $model;

    /**
     * ReportsService constructor.
     */
    public function __construct()
    {
        $this->model = new Report();
    }

    /**
     * Generate a patient transmission report
     * @param $data
     * @return mixed
     */
    public function generatePatientTransmissionReport($data)
    {
        $query_string = http_build_query($data['cover_sheet_data']);
        $pdf = PDF::loadView('reports.patient.patient-transmission', ['data' => $data]);
        $pdf->setOptions(
                            [
                                'header-html'       => url('/') . '/reports/reportHeader?'.$query_string.'&type=patient',
                        ]);
        $pdfFileName = str_slug($data['cover_sheet_data']['patient']['name']) . '-' .  Carbon::now()->toDateString() .'-patient-transmission-snappy.pdf';
        $report = $this->saveReport($data['cover_sheet_data']['patient']['id'], $data['cover_sheet_data']['patient']['session_id'], $pdfFileName, 'patient-transmission');
        if(Storage::disk('local')->put('patient-transmission-reports/' . Carbon::now()->toDateString() . '/' . $pdfFileName, $pdf->output())) {
            return $report;
        } else {
            return [
                        'error' =>  'There was an error generating this report.'
            ];
        }
    }

    /**
     * Generate a Holter ECG Summary Report
     * @param $data
     * @return Report|array
     */
    public function generateHolterReport($data)
    {
        $alerts = Alert::with('patient', 'recording.clip', 'event')
                        ->where('flagged_at', '!=', null)
                        ->where('patient_id', $data['cover_sheet_data']['patient']['id'])
                        ->where('reviewed_at', null)
                        ->orderBy('id', 'desc')
                        ->get();

        $query_string = http_build_query($data['cover_sheet_data']);
        $data['holter_report_data'] = [
            'clipped_recordings' => $alerts
        ];
        $pdf = PDF::loadView('reports.patient.holter', ['data' => $data]);
        $pdf->setOptions(
            [
                'header-html'       => url('/') . '/reports/reportHeader?'.$query_string.'&type=holter',
            ]);
        $pdfFileName = str_slug($data['cover_sheet_data']['patient']['name']) . '-' .  Carbon::now()->toDateString() .'-holter-summary-snappy.pdf';
        $report = $this->saveReport($data['cover_sheet_data']['patient']['id'], $data['cover_sheet_data']['patient']['session_id'], $pdfFileName, 'holter-summary');
        if(Storage::disk('local')->put('holter-summary-reports/' . Carbon::now()->toDateString() . '/' . $pdfFileName, $pdf->output())) {
            return $report;
        } else {
            return [
                'error' =>  'There was an error generating this report.'
            ];
        }
    }

    /**
     * Generate a full disclosure report
     * @param $data
     * @return mixed
     */
    public function generateFullDisclosureReport($data)
    {
        $pdfFileName = str_slug($data['cover_sheet_data']['patient']['name']) . '-' .  Carbon::now()->toDateString() .'-full-disclosure.pdf';
        $pdf = PDF::loadView('reports.patient.full-disclosure', ['data' => $data]);
        $pdf->marginRight('5mm');
        $pdf->marginLeft('5mm');
        $pdf->marginTop('15mm');
        $pdf->marginBottom('5mm');
    }

    /**
     * Temporary preview function for the Full Disclosure Patient Report
     * @param $data
     * @return mixed
     */
    public function previewFullDisclousreReport($data)
    {
        $query_string = http_build_query($data['cover_sheet_data']);
        $pdf = PDF::loadView('reports.patient.patient-transmission', ['data' => $data]);
        $pdf->setOptions(
            [
                'header-html'       => url('/') . '/reports/reportHeader?'.$query_string.'&type=disclosure',
            ]);
        $pdfFileName = str_slug($data['cover_sheet_data']['patient']['name']) . '-' .  Carbon::now()->toDateString() .'-patient-transmission-snappy.pdf';
        Storage::disk('local')->put('report-previews/' . Carbon::now()->toDateString() . '/' . $pdfFileName, $pdf->output());
        return storage_path().'/app/report-previews/' . Carbon::now()->toDateString() . '/' . $pdfFileName;
    }

    /**
     * Get a report from local storage
     * @param $id
     * @return mixed
     */
    public function initiateDownloadRequest($id)
    {
        $report = $this->model->findOrFail($id);
        return Storage::disk('local')
                            ->getDriver()
                            ->getAdapter()
                            ->applyPathPrefix($report->report_type.'-reports/' . $report->created_at->format('Y-m-d') . '/'.$report->file_slug);
    }

    /**
     * Store a report in the database
     * @param $patient
     * @param $recording_session
     * @param $fileName
     * @param $type
     * @return Report
     */
    protected function saveReport($patient, $recording_session, $fileName, $type)
    {
        $this->model->patient_id = $patient;
        $this->model->recording_session_id = $recording_session;
        $this->model->file_slug = $fileName;
        $this->model->report_type = $type;
        $this->model->created_by = Auth::guard('api')->user()->id;
        $this->model->save();
        return $this->model;
    }

}