<?php
namespace App\Services\Events;

use App\Models\Event;
use App\Models\Notification;
use App\Models\EcgEventsCount;
use App\Models\Recording;
use App\Services\Alerts\AlertService;
use App\Services\NotificationService;
use App\Events\GeneratePatientNotification;
use App\Events\GenerateNetworkWideNotification;

/**
 * Class CreateEventService
 * @package App\Services\Events
 */
class CreateEventService
{

    /**
     * @var
     */
    protected $eventToAlert;

    /**
     * Create a new event entry
     * @param $event
     */
    public function createNewEvent($recording_session_id, $events)
    {
        \Log::info(json_encode($events));
        $event = new Event();
        $event->event_payload = json_encode($events);
        $event->recording()->associate($recording_session_id);
        $this->incrementEventCount($events, $recording_session_id);
        $event->save();
        $this->eventToAlert = $event;
        $this->detectAlertsInEvent($events);
        return $event;
    }

    /**
     * Detect any Red or Yellow alerts within an events JSON payload
     * @param $events
     */
    public function detectAlertsInEvent($events)
    {
        $alert_on_events = [];
        $flag_type = false;
        foreach ($events['events_filtered_json_data'] as $event) {
            if ($event->DESCRIPTION_SHORT == 'AFib rapid') {
                $flag_type = 'red';
                array_push($alert_on_events, $event->DESCRIPTION_SHORT);
            } elseif ($event->DESCRIPTION_SHORT == 'Pause') {
                $flag_type = 'red';
                array_push($alert_on_events, $event->DESCRIPTION_SHORT);
            } elseif ($event->DESCRIPTION_SHORT == 'VT') {
                $flag_type = 'red';
                array_push($alert_on_events, $event->DESCRIPTION_SHORT);
            } elseif ($event->DESCRIPTION_SHORT == 'VF') {
                $flag_type = 'yellow';
                array_push($alert_on_events, $event->DESCRIPTION_SHORT);
            } elseif ($event->DESCRIPTION_SHORT == 'Mobitz I') {
                $flag_type = 'yellow';
                array_push($alert_on_events, $event->DESCRIPTION_SHORT);
            } elseif ($event->DESCRIPTION_SHORT == 'Mobitz II') {
                $flag_type = 'yellow';
                array_push($alert_on_events, $event->DESCRIPTION_SHORT);
            } elseif ($event->DESCRIPTION_SHORT == 'AFib slow') {
                $flag_type = 'yellow';
                array_push($alert_on_events, $event->DESCRIPTION_SHORT);
            } elseif ($event->DESCRIPTION_SHORT == 'AFib normal') {
                $flag_type = 'yellow';
                array_push($alert_on_events, $event->DESCRIPTION_SHORT);
            } elseif ($event->DESCRIPTION_SHORT == 'SVTA') {
                $flag_type = 'yellow';
                array_push($alert_on_events, $event->DESCRIPTION_SHORT);
            }
            if (count($alert_on_events) > 0 && isset($flag_type)) {
                if ($flag_type == 'yellow') {
                    $this->generateYellowAlert($alert_on_events[0]);
                } elseif ($flag_type == 'red') {
                    $this->generateRedAlert($alert_on_events[0]);
                }
            }
        }
    }

    /**
     * Generate a red alert notification
     * @param $recording_session_id
     * @param $event
     */
    protected function generateRedAlert($event)
    {
        $patient = $this->eventToAlert->recording->patient;
        $service = new NotificationService(new Notification());
        $notification = $service->storeNotification('red_alert',
            '<span class="label label-danger">A red alert was captured for patient ' . $patient->name_first . ' ' . $patient->name_last . ' : ' . $event . '</span>');
        $service->assignNotificationsToUsersInNetwork($notification, \App\Models\Network::with('users')->get()->first()->users());
        event(new GenerateNetworkWideNotification($notification));
        event(new GeneratePatientNotification($patient, $notification));

        (new AlertService())->storeNewAlert([
            'type' => 'red',
            'patient_id' => $patient->id,
            'significant_event' => $event,
            'recording_id' => $this->eventToAlert->recording_id,
            'event_id' => $this->eventToAlert->id
        ]);
    }

    /**
     * Generate a yellow alert notification
     * @param $recording_session_id
     * @param $event
     */
    protected function generateYellowAlert($event)
    {
        $patient = $this->eventToAlert->recording->patient;
        $service = new NotificationService(new Notification());
        $notification = $service->storeNotification('yellow_alert',
            '<span class="label label-warning">A yellow alert was captured for patient ' . $patient->name_first . ' ' . $patient->name_last . ' : ' . $event . '</span>');
        $service->assignNotificationsToUsersInNetwork($notification, \App\Models\Network::with('users')->get()->first()->users());
        event(new GenerateNetworkWideNotification($notification));
        event(new GeneratePatientNotification($patient, $notification));

        (new AlertService())->storeNewAlert([
            'type' => 'yellow',
            'patient_id' => $patient->id,
            'significant_event' => $event,
            'recording_id' => $this->eventToAlert->recording_id,
            'event_id' => $this->eventToAlert->id
        ]);
    }

    /**
     * Increment the event count in the database given the event in the recording cycle
     * @param $events
     */
    protected function incrementEventCount($events, $recording_session_id)
    {
        $session = Recording::find($recording_session_id)->recording_session->id;
        $eventCounts = EcgEventsCount::where('recording_session_id', $session)
                                     ->get()->first();
        foreach ($events['events_filtered_json_data'] as $event) {
            if($event->DESCRIPTION_SHORT == 'Noise') {
                $eventCounts->increment('noise');
            } elseif($event->DESCRIPTION_SHORT == 'Unclassified rhythm') {
                $eventCounts->increment('unclassified_rhythm');
            } elseif($event->DESCRIPTION_SHORT == 'UNCLASSIFIED BEATS') {
                $eventCounts->increment('unclassified_beats');
            } elseif($event->DESCRIPTION_SHORT == 'NSR') {
                $eventCounts->increment('nsr');
            } elseif($event->DESCRIPTION_SHORT == 'SinBRADY') {
                $eventCounts->increment('sinbrady');
            } elseif($event->DESCRIPTION_SHORT == 'SinTACHY') {
                $eventCounts->increment('sintachy');
            } elseif($event->DESCRIPTION_SHORT == 'NSR+IVCD'){
                $eventCounts->increment('nsr_ivcd');
            } elseif($event->DESCRIPTION_SHORT == 'SinBrady+IVCD') {
                $eventCounts->increment('sinbrady_ivcd');
            } elseif($event->DESCRIPTION_SHORT == 'SinTachy+IVCD') {
                $eventCounts->increment('sintachy_ivcd');
            } elseif($event->DESCRIPTION_SHORT == 'PAUSE') {
                $eventCounts->increment('pause');
            } elseif($event->DESCRIPTION_SHORT == 'SVC') {
                $eventCounts->increment('svc');
            } elseif($event->DESCRIPTION_SHORT == 'JuncTACHY') {
                $eventCounts->increment('junctachy');
            } elseif($event->DESCRIPTION_SHORT == '1degr.AVBlock+NSR') {
                $eventCounts->increment('first_deg_avblock_nsr');
            } elseif($event->DESCRIPTION_SHORT == '1degr.AVBlock+SinTACHY') {
                $eventCounts->increment('first_deg_avblock_sintachy');
            } elseif($event->DESCRIPTION_SHORT == '1degr.AVBlock+SinBRADY') {
                $eventCounts->increment('first_deg_avblock_sinbrady');
            } elseif($event->DESCRIPTION_SHORT == 'Mobitz I') {
                $eventCounts->increment('mobitz_i');
            } elseif($event->DESCRIPTION_SHORT == 'Mobitz II') {
                $eventCounts->increment('mobitz_ii');
            } elseif($event->DESCRIPTION_SHORT == 'PAC') {
                $eventCounts->increment('pac');
            } elseif($event->DESCRIPTION_SHORT == 'SVTA') {
                $eventCounts->increment('svta');
            } elseif($event->DESCRIPTION_SHORT == 'AFib slow') {
                $eventCounts->increment('afib_slow');
            } elseif($event->DESCRIPTION_SHORT == 'AFib normal') {
                $eventCounts->increment('afib_normal');
            } elseif($event->DESCRIPTION_SHORT == 'AFib rapid') {
                $eventCounts->increment('afib_rapid');
            } elseif($event->DESCRIPTION_SHORT == 'PVC'){
                $eventCounts->increment('pvc');
            } elseif($event->DESCRIPTION_SHORT == 'VCoup') {
                $eventCounts->increment('vcoup');
            } elseif($event->DESCRIPTION_SHORT == 'VTrip') {
                $eventCounts->increment('vtrip');
            } elseif($event->DESCRIPTION_SHORT == 'VBig') {
                $eventCounts->increment('vbig');
            } elseif($event->DESCRIPTION_SHORT == 'VTrig') {
                $eventCounts->increment('vtrig');
            } elseif($event->DESCRIPTION_SHORT == 'IVR') {
                $eventCounts->increment('ivr');
            } elseif($event->DESCRIPTION_SHORT == 'VT') {
                $eventCounts->increment('vt');
            } elseif($event->DESCRIPTION_SHORT == 'Slow VT') {
                $eventCounts->increment('slow_vt');
            } elseif($event->DESCRIPTION_SHORT == 'VF') {
                $eventCounts->increment('vf');
            }
        }
       return $eventCounts->save();
    }

}