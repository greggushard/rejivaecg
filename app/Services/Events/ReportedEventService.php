<?php
namespace App\Services\Events;

use App\Models\EcgEventsCount;

/**
 * Class ReportedEventService
 * @package App\Services\Events
 */
class ReportedEventService
{

    /**
     * @param $event_type
     * @param $recording_session
     */
    public function incrementPatientEvents($recording_session, $event_type)
    {
        $eventsCount = EcgEventsCount::where('recording_session_id', $recording_session->id)->get()->first();

        if($event_type == 1) {
            $eventsCount->increment('light_headed');
        } elseif($event_type == 2) {
            $eventsCount->increment('shortness_of_breath');
        } elseif($event_type == 3) {
            $eventsCount->increment('fatigue');
        } elseif($event_type == 4) {
            $eventsCount->increment('chest_pain');
        } elseif($event_type == 5) {
            $eventsCount->increment('heart_palpitation');
        } elseif($event_type == 6) {
            $eventsCount->increment('audio_submission');
        } elseif($event_type == 9) {
            $eventsCount->increment('fall_detected');
        }
        $eventsCount->save();
    }
}