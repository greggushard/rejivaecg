<?php
namespace App\Services\Clips;

use Auth;
use Carbon\Carbon;
use App\Models\Clip;
use App\Models\Recording;
use App\Jobs\RenderReportGraph;

/**
 * Class ClippingService
 * @package App\Services
 */
class ClippingService
{

    /**
     * @var Clip
     */
    protected $model;

    /**
     * ClippingService constructor.
     */
    public function __construct()
    {
        $this->model = new Clip();
    }

    /**
     * Store a new clip in the database
     * @param $recording_id
     * @return Clip
     */
    public function storeNewClip($recording_id, $patient_id)
    {
        $this->model->recording_id = $recording_id;
        $this->model->clipped_at = Carbon::now();
        $this->model->patient_id = Recording::with('patient')->find($recording_id)->patient->id;
        $this->model->clipped_by = Auth::guard('api')->user()->id;
        $this->model->save();
        dispatch(new RenderReportGraph($this->model->recording));
        return $this->model;
    }

    /**
     * Get a clip by a recording id
     * @param $recording_id
     * @return mixed
     */
    public function getClipsByPatient($patient_id)
    {
        $clips = $this->model->with('recording', 'recording.events', 'recording.remeasurements', 'recording.patient', 'recording.recording_session')
                             ->where('patient_id', $patient_id)
                             ->get();
        return $clips;
    }

}