<?php
namespace App\Services\Networks;

use App\Models\Network;
use Config;

/**
 * Class CreateNetworkService
 * @package App\Services\Networks
 */
class CreateNetworkService {

    /**
     * @var Network
     */
    protected $model;

    /**
     * CreateNetworkService constructor.
     * @param Network $model
     */
    public function __construct(Network $model)
    {

        $this->model = $model;

    }

    /**
     * Create a new network entry in the database
     * @param $data
     */
    public function storeNetwork($network)
    {

        return Network::create($network);

    }



}