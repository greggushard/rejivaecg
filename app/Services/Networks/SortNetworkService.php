<?php

namespace App\Services\Networks;

use App\Models\Network;
use App\Transformers\NetworkTransformer;

/**
 * Class SortNetworkService
 * @package App\Services\Networks
 */
class SortNetworkService
{

    /**
     * @var Network
     */
    protected $model;

    /**
     * @var NetworkTransformer
     */
    protected $transformer;

    /**
     * SortNetworkService constructor.
     * @param Network $network
     */
    public function __construct(Network $network, NetworkTransformer $transformer)
    {

        $this->model = $network;
        $this->transformer = $transformer;

    }

    /**
     * Return a list of networks
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getNetworks()
    {
        $networks =  $this->model->toUserRole()->with(['practices', 'users'])->orderBy('id', 'asc')->get();
        return $networks;
    }

    /**
     * Sort users by a column
     * @param $request
     * @return mixed
     */
    public function sortNetworks($sort)
    {
        list($sortCol, $sortDir) = explode('|', $sort);
        $query = $this->model->toUserRole()->with(['practices', 'users'])->orderBy($sortCol, $sortDir);
        $networks = $query->paginate(10);
        $this->transformer->transformOutput($networks->getCollection());
        return $networks;
    }


    /**
     * Search for a user by name, email or company
     * @param $request
     * @return mixed
     */
    public function filterNetworks($request)
    {
        $query = $this->model->toUserRole()->with(['practices', 'users'])->where(function($q) use($request){
            $value = "%{$request->filter}%";
            $q->where('name', 'like', $value)
                ->orWhere('city', 'like', $value)
                ->orWhere('address', 'like', $value);
        });

        $networks = $query->paginate(10);
        $this->transformer->transformOutput($networks->getCollection());
        return $networks;
    }

    /**
     * Get patients without a device
     * @param $id
     * @return mixed
     */
    public function getAvailablePatients($id)
    {
        $network =  $this->model->with(['patients' => function($query) use($id) {
                            $query->availableForRecordingSession();
                    }])->where('id', $id)->get()->first();

        $patients = $network->patients;

        return $patients;
    }

    /**
     * Get devices without a patient
     *
     * @param $id
     */
    public function getAvailableDevices($id)
    {
        $network = $this->model->with(['devices' => function($query) use($id) {
            $query->whereDoesntHave('recording_sessions');
        }])->find($id);

        return $network->devices;
    }


}