<?php
namespace App\Services\Networks;

use App\Models\Network;

/**
 * Class UpdateNetworkService
 * @package App\Services\Networks
 */
class UpdateNetworkService {

    /**
     * @var Network
     */
    protected $model;

    /**
     * CreateNetworkService constructor.
     * @param Network $model
     */
    public function __construct(Network $model)
    {

        $this->model = $model;

    }

    /**
     * Create a new network entry in the database
     * @param $data
     */
    public function updateNetwork($network)
    {

        if(isset($network->province))
             $network->province = $this->mapProvince($network->province);

        $network = $this->model->update($network);
        return $network;

    }

    /**
     * Map a province
     * @param $id
     * @return mixed
     */
    private function mapProvince($id)
    {

        $provinces = collect(Config::get('available_locations'));
        return $provinces->get($id);

    }
}