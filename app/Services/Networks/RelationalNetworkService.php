<?php

namespace App\Services\Networks;

use App\Models\User;
use App\Models\Practice;
use App\Models\Network;

class RelationalNetworkService
{

    /**
     * List practices with their users for a network
     * @param Network $network_id
     */
    public function getPracticesInNetwork($network_id)
    {

        $practices = Practice::whereHas('network', function($q) use ($network_id){
            $q->where('network_id', $network_id);
        })->get();

        return $practices;

    }

    /**
     * Return a list of members for a network
     *
     * @param Network $network_id
     */
    public function getStaffInNetwork($network_id)
    {
        $users = User::with('roles')->whereHas('networks', function($q) use ($network_id) {
            $q->where('network_id', $network_id);
        })->get();

        $users = $users->transform(function($user, $key){

                $role = $user->roles->first();

                if($role->level == 1) {
                    $role = '<span class="label label-primary pull-right">Administrator</span>';
                } elseif($role->level == 2) {
                    $role = '<span class="label label-default pull-right">User</span>';
                } elseif($role->level == 3) {
                    $role = '<span class="label label-success pull-right">Network Administrator</span>';
                } elseif($role->level == 4) {
                    $role = '<span class="label label-warning pull-right">Cardiac Technician</span>';
                }
                elseif($role->level == 5) {
                    $role = '<span class="label label-danger pull-right">Interpreting Cardiologist</span>';
                }

                return [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'role' => $role
                ];

        });

        return $users;

    }
}