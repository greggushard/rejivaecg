<?php
/**
 * Created by PhpStorm.
 * User: greggushard
 * Date: 10/24/16
 * Time: 9:52 AM
 */

namespace App\Services\Recordings;

use App\Models\Recording;
use App\Jobs\RenderMoneboGraph;
use JpGraph\JpGraph;

/**
 * Class JPGraphService
 * @package App\Services\Recordings
 */
class JPGraphService
{

    /**
     * @var JpGraph
     */
    protected $recording;

    /**
     * JPGraphService constructor.
     * @param JpGraph $grapher
     */
    public function __construct(Recording $recording)
    {

        $this->recording = $recording;

    }

    /**
     * Generate an ECG Graph given a recording hash
     *
     * @param $recording_hash
     */
    public function generateEcgGraph()
    {

        $base64 = dispatch(new RenderMoneboGraph($this->recording));
        return $base64;

    }

}