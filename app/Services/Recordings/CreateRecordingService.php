<?php
namespace App\Services\Recordings;

use Carbon\Carbon;
use Monolog\Logger;
use App\Models\Recording;
use App\Models\RecordingSession;
use Monolog\Handler\StreamHandler;
use Illuminate\Support\Facades\Storage;

/**
 * Class CreateRecordingService
 * @package App\Services\Recordings
 */
class CreateRecordingService
{

    /**
     * CreateRecordingService constructor.
     */
    public function __construct()
    {
        // Initialize Nate's recording function
        $this->recording = new Recording();
    }

    /**
     * Store a new recording in the database
     *
     * @param $recordingSession
     * @param $recording_id
     * @return mixed
     */
    public function createNewRecording($recordingSession, $additional_data, $missing_strips_count)
    {
        // Create the Redis entry
        $recording_id = md5('session-'.$recordingSession->first()->id.'-recording'.time());
        // Save the recording to the db
        $recording = (new Recording());
        $recording->patient()->associate($recordingSession->first()->patient->id);
        $recording->recording_session()->associate($recordingSession->first()->id);
        $recording->timestamp = $additional_data['timestamp'];
        $recording->recording_hash = $recording_id;
        $recording->recordings_missed = $missing_strips_count;
        $recording->phone_id = $additional_data['phone_id'];
        $recording->battery_level = $additional_data['battery_level'];
        $recording->breathing_rate = $additional_data['breathing_rate'];
        $recording->heart_rate = $additional_data['heart_rate'];
        $recording->body_position = $additional_data['body_position'];
        $recording->save();
        return $recording;
    }

    /**
     * Generate the Raw ECG File
     * @param $data
     * @return string
     */
    public function generateEcgFile($data)
    {
        $id = rand(1, 935993);
        $saveLocation = '/app/raw_ecg/' . $id. '.raw';
        Storage::disk('local')->put($saveLocation, $data);
        return $saveLocation;
    }

    /**
     * Process Raw data into a readable format
     * Adapted from EC2 Function in App\Models\Recording
     *
     * @param $data
     * @return mixed
     */
    public function processRawData($recording_hash, $data)
    {
        $rawArray = explode(',', $data);
        // Build return array
        $calculatedArray = [];
        $calculatedString = '';
        // Iterate raw array values
        foreach($rawArray as $deviceValue) {
            $realValue = (int)$deviceValue;
            $ecgValue = ((($realValue) * env('ECG_MULTIPLIER')) / env('ECG_RAW_MAX')) - env('ECG_OFFSET');
            array_push($calculatedArray, $ecgValue);
            $calculatedString .= $ecgValue.',';
        }
        \Storage::disk('local')->put('raw_graph_file/' . $recording_hash . '.gph', $calculatedString);
        # Respond to array
        return $calculatedString;
    }

    /**
     * Map the existing events in the db to a valid recording ID
     *
     * @param $recording_hash
     */
    public function mapExistingReportedEvents($recording)
    {
        $recording = Recording::with('patient')->where('recording_hash', $recording)->get()->first();
        $start_time = $recording->timestamp;
        $end_time = $recording->timestamp->addSeconds(30);

        $events = \DB::table('reported_events')
                    ->select('*')
                    ->whereRaw('patient_id = ' . $recording->patient->id . ' AND timestamp >= "' . $start_time . '" AND timestamp <= "' . $end_time . '"')
                    ->orderBy('id', 'DESC')->get();
        return $events;
    }

    /**
     * Process Raw data into a readable format
     * Adapted from EC2 Function in App\Models\Recording
     *
     * @param $data
     * @return mixed
     */
    public function normalizeFileForEvents($recording_hash, $data)
    {
        $rawdata_string = $data;

        $rawdata_array = explode(",", $rawdata_string);
        $rawdata_max = max($rawdata_array);

        $gphdata_array = [];
        $chunked_data = [];

        foreach($rawdata_array as $rawdata_val) {
            $gphdata_val = ($rawdata_val)/($rawdata_max);
            array_push($gphdata_array,$gphdata_val);
            if($gphdata_val < 1)
                array_push($chunked_data, substr($gphdata_val, 1, 6));
            else
                array_push($chunked_data, $gphdata_val);
        }

       Storage::disk('local')->put('ecg_normalized_shifted/'. $recording_hash . '.gph', implode(",", $chunked_data));
       return Storage::disk('local')->put('ecg_normalized/'. $recording_hash . '.gph', implode(",", $gphdata_array));
    }

    /**
     * Store a normalized RAW File
     *
     * @param $data
     * @return string
     */
    public function saveRawFile($recording_id, $data)
    {
        $saveLocation = '/app/raw_ecg/' . $recording_id . '.raw';
        Storage::disk('local')->put($saveLocation, $data);
        return $recording_id;
    }

    /**
     * Store a normalized GPH File
     *
     * @param $data
     * @return string
     */
    public function saveRawGraphFile($recording_id, $data)
    {
        $normalized = $this->processRawData($recording_id, $data);
        $saveLocation = 'raw_graph_file/' . $recording_id . '.gph';
        Storage::disk('local')->put($saveLocation, $normalized);
        return $recording_id;
    }


    /**
     * Generate the Monebo Output
     *
     * @param $graph_id
     * @return array
     */
    public function generateNormalizedResults($graph_id, $sample_rate = 250)
    {
        $output = $this->recording->runMoneboEcgAlogrithm($graph_id, $sample_rate);
        // Once the algorithm is run we will generate the event_recording keys so that
        // users can filter recordings by their corresponding events
        return $output;
    }

    /**
     * Filter the Patients' Reported Events
     *
     * @param $recordings
     * @param $events
     */
    public function filterReportedEvents($recordings, $events, $patient_id)
    {
        ini_set('memory_limit','3048M');
        $log = new Logger('ReportedMap');
        $log->pushHandler(new StreamHandler(storage_path().'/logs/ReportedMap.log', Logger::INFO));
        $addRecordings = [];
        $rawString = '';

        $len = count($recordings);
        $event_sql = implode(',', $events);

        foreach($recordings as $index => $recording) {
            if ($index == 0) {
                $rawString .= 'patient_id = "' . $patient_id . '" AND ';
                $rawString .= 'event_id IN (' . $event_sql . ') AND ';
                $rawString .= 'timestamp >= "' . $recording->timestamp . '" AND timestamp <= "' . $recording->timestamp->addSeconds(8) . '" OR';
            } else if ($index == $len - 1) {
                $rawString .= ' patient_id = "' . $patient_id . '" AND ';
                $rawString .= ' event_id IN (' . $event_sql . ') AND ';
                $rawString .= ' timestamp >= "' . $recording->timestamp . '" AND timestamp <= "' . $recording->timestamp->addSeconds(8) . '"';
            } else {
                $rawString .= ' patient_id = "' . $patient_id . '" AND ';
                $rawString .= ' event_id IN (' . $event_sql . ') AND ';
                $rawString .= ' timestamp >= "' . $recording->timestamp . '" AND timestamp <= "' . $recording->timestamp->addSeconds(8) . '" OR';
            }
        }

        $reported_events = \DB::table('reported_events')
                        ->select('*')
                        ->whereRaw($rawString)
                        ->orderBy('id', 'DESC')->get();

        foreach($recordings as $recording) {
            $eventTrue = $reported_events->filter(function($event) use ($recording, $addRecordings){
                                        if  (   $event->timestamp >= $recording->timestamp
                                                && $event->timestamp <= $recording->timestamp->addSeconds(8)
                                            )
                                        {
                                            return true;
                                        }
                        });
            if($eventTrue->count() > 0)
                array_push($addRecordings, $recording);
        }
        ini_set('memory_limit','128M');
        return array_reverse($addRecordings);
    }

    /**
     * Walk through the array of monebo events, and associate them with the recording
     * jesus christ...
     *
     * @param $recording_id
     */
    public function filterMoneboEvents($recordings, $events)
    {
        $log = new Logger('EventMap');
        $log->pushHandler(new StreamHandler(storage_path().'/logs/EventMap.log', Logger::INFO));
        $addEvents = [];
        $mappedRecordings = [];
        foreach($recordings as $recording) {
            $log->addInfo("Getting events file for " . $recording->recording_hash);
            if(Storage::disk('local')->exists('ecg_monebo_output/'. $recording->recording_hash . '.json')) {
                $file = json_decode(Storage::disk('local')->get('ecg_monebo_output/' . $recording->recording_hash . '.json'));
                foreach($file->OUTPUT->EVENT as $event)
                {
                    // Bad Events
                    if($event->DESCRIPTION_FULL == "ARTIFACT") {
                        array_push($addEvents, "400");
                    } elseif($event->DESCRIPTION_FULL == "Noise") {
                        array_push($addEvents, "401");
                    } elseif($event->DESCRIPTION_FULL == "UNCLASSIFIED BEATS") {
                        array_push($addEvents, "402");
                    }

                    // Arrythmia Events
                    elseif($event->DESCRIPTION_FULL == "PAUSE") {
                        $log->addNotice("Pause Detected");
                        array_push($addEvents, "300");
                    } elseif($event->DESCRIPTION_FULL == "PREMATURE SUPRAVENTRICULAR CONTRACTION") {
                        array_push($addEvents, "301");
                    } elseif($event->DESCRIPTION_FULL == "JUNCTIONAL TACHYCARDIA") {
                        array_push($addEvents, "302");
                    } elseif($event->DESCRIPTION_FULL == "FIRST DEGREE HEART BLOCK + SINUS RHYTHM") {
                        array_push($addEvents, "303");
                    } elseif($event->DESCRIPTION_FULL == "FIRST DEGREE HEART BLOCK + SINUS TACHYCARDIA") {
                        array_push($addEvents, "304");
                    } elseif($event->DESCRIPTION_FULL == "FIRST DEGREE HEART BLOCK + SINUS BRADYCARDIA") {
                        array_push($addEvents, "305");
                    } elseif($event->DESCRIPTION_FULL == "SECOND DEGREE HEART BLOCK TYPE I") {
                        array_push($addEvents, "306");
                    } elseif($event->DESCRIPTION_FULL == "SECOND DEGREE HEART BLOCK TYPE II") {
                        array_push($addEvents, "307");
                    } elseif($event->DESCRIPTION_FULL == "PREMATURE ATRIAL CONTRACTION") {
                        array_push($addEvents, "308");
                    } elseif($event->DESCRIPTION_FULL == "ATRIAL/SUPRAVENTRICULAR TACHYCARDIA") {
                        array_push($addEvents, "309");
                    } elseif($event->DESCRIPTION_FULL == "ATRIAL FIBRILLATION/FLUTTER SVR") {
                        array_push($addEvents, "310");
                    } elseif($event->DESCRIPTION_FULL == "ATRIAL FIBRILLATION/FLUTTER CVR") {
                        array_push($addEvents, "311");
                    } elseif($event->DESCRIPTION_FULL == "ATRIAL FIBRILLATION/FLUTTER RVR") {
                        array_push($addEvents, "312");
                    } elseif($event->DESCRIPTION_FULL == "PREMATURE VENTRICULAR CONTRACTION") {
                        array_push($addEvents, "313");
                    } elseif($event->DESCRIPTION_FULL == "VENTRICULAR COUPLET Detected") {
                        array_push($addEvents, "314");
                    } elseif($event->DESCRIPTION_FULL == "VENTRICULAR TRIPLET") {
                        array_push($addEvents, "315");
                    } elseif($event->DESCRIPTION_FULL == "VENTRICULAR BIGEMINY") {
                        array_push($addEvents, "316");
                    } elseif($event->DESCRIPTION_FULL == "VENTRICULAR TRIGEMINY") {
                        array_push($addEvents, "317");
                    } elseif($event->DESCRIPTION_FULL == "IDIOVENTRICULAR RHYTHM") {
                        array_push($addEvents, "318");
                    } elseif($event->DESCRIPTION_FULL == "VENTRICULAR TACHYCARDIA") {
                        array_push($addEvents, "319");
                    } elseif($event->DESCRIPTION_FULL == "SLOW VENTRICULAR TACHYCARDIA Detected") {
                        array_push($addEvents, "320");
                    } elseif($event->DESCRIPTION_FULL == "VENTRICULAR FLUTTER") {
                        array_push($addEvents, "321");
                    }

                    // Good Events
                    elseif($event->DESCRIPTION_SHORT == "SinBrady+IVCD") {
                        array_push($addEvents, "204");
                    } elseif($event->DESCRIPTION_SHORT == "SinBRADY") {
                        array_push($addEvents, "201");
                    } elseif($event->DESCRIPTION_SHORT == "SinTACHY") {
                        array_push($addEvents, "202");
                    } elseif($event->DESCRIPTION_SHORT == "NSR+IVCD") {
                        array_push($addEvents, "203");
                    } elseif($event->DESCRIPTION_SHORT == "NSR") {
                        array_push($addEvents, "200");
                    } elseif($event->DESCRIPTION_SHORT == "SinTachy+IVCD") {
                        array_push($addEvents, "205");
                    }

                    // Find the intersecting values
                    $compared = array_intersect($events, $addEvents);
                    // If the two arrays both have one or more of the events, push them into the mapped recordings
                    if(!empty($compared))
                        array_push($mappedRecordings, $recording);
                    $addEvents = [];
                }
            }
        }
        // Return the mapped recordings
        return array_reverse($mappedRecordings);
    }

    /**
     * Get the devices associated with a recording session
     * @param $request
     * @return bool
     */
    public function getRecordingSessionDevice($request)
    {
        $recordingSession = RecordingSession::with('patient')->whereHas('devices', function($q) use($request){
                                $q->where('devices.device_id', (int)$request['device_id']);
                            })->with(['recordings' => function($q){
                                $q->limit(1)->orderBy('id', 'DESC');
                            }])->where('date_end', '>', Carbon::now())->get();

        if(!$recordingSession->count())
            return false;

        return $recordingSession;
    }

}