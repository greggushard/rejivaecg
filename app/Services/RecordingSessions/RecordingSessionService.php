<?php
namespace App\Services\RecordingSessions;

use Log;
use Auth;
use Carbon\Carbon;
use App\Models\RecordingSession;
use App\Transformers\RecordingSessionTransformer;

/**
 * Class RecordingSessionService
 * @package App\Services\RecordingSessions
 */
class RecordingSessionService {

    /**
     * @var RecordingSession
     */
    protected $model;

    /**
     * @var RecordingSessionTransformer
     */
    protected $transformer;

    /**
     * RecordingSessionService constructor.
     * @param RecordingSession $recordingSession
     */
    public function __construct(RecordingSession $recordingSession, RecordingSessionTransformer $transformer)
    {
        $this->model = $recordingSession;
        $this->transformer = $transformer;
    }

    /**
     * Authenticate a recording session given a sessions' PIN
     * @param $session_pin
     * @return array|bool
     */
    public function authenticateRecordingSession($session_pin)
    {
        $recordingSession = $this->model->with('devices')
                                        ->where('session_pin', $session_pin)
                                        ->get()
                                        ->first();
        if($recordingSession)
            $oauthResponse = $this->getNetworkAdminCredentials($recordingSession->assigned_by);
        else
            return false;

        return ['credentials' => $oauthResponse, 'devices' => $recordingSession->devices->transform(function($device){
                        return $device->radio_id;
                    })];
    }

    /**
     * Return a list of recording sessions
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getRecordingSessions()
    {
        $query =  $this->model->with(['patient.users', 'devices', 'recordings'])->orderBy('id', 'asc');
        $recordingSessions = $query->paginate(10);
        $this->transformer->transformOutput($recordingSessions->getCollection());
        return $recordingSessions;
    }

    /**
     * Sort the list of recording sessions by their start_date, end_date or their last recording session
     * @param $sort
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function sortRecordingSessions($sort)
    {
        list($sortCol, $sortDir) = explode('|', $sort);
        if($sortCol == 'last_recording') {
            # Order by last recording timestamp
            $query = $this->model->with(['patient', 'devices', 'recordings' => function($q) use($sortDir) {
                            $q->orderBy('recordings.timestamp', $sortDir);
                        }]);
        } elseif($sortCol == 'devices') {
            # Order by device count
            $query = $this->model->with(['patient', 'devices', 'recordings'])->withCount('devices')->orderBy('devices_count', $sortDir);
        } else {
            # Order by field in the RecordingSessions table
            $query =  $this->model->with(['patient', 'devices', 'recordings'])
                                    ->orderBy($sortCol, $sortDir);
        }
        $recordingSessions = $query->paginate(10);
        $this->transformer->transformOutput($recordingSessions->getCollection());
        return $recordingSessions;
    }

    /**
     * Filter sessions by a variety of different relations
     * @param $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function filterRecordingSessions($request)
    {
        $value = "%{$request->filter}%";
        $query = $this->model->with('devices', 'patient', 'patient.users', 'recordings')
                             # Filter by Device Radio ID
                             ->whereHas('devices', function($q) use ($value) {
                                    $q->where('devices.radio_id', 'like', $value);
                             })
                             # Filter by Patients First or Last Name
                             ->orWhereHas('patient', function($q) use ($value) {
                                    $q->where('patients.name_first', 'like', $value);
                                    $q->orWhere('patients.name_last', 'like', $value);
                             })
                             # Filter by a users' name
                             ->orWhereHas('patient.users', function($q) use($value) {
                                    $q->where('users.name_first', 'like', $value);
                                    $q->orWhere('users.name_last', 'like', $value);
                             });
        $recordingSessions = $query->paginate(10);
        $this->transformer->transformOutput($recordingSessions->getCollection());
        return $recordingSessions;
    }

    /**
     * Instantiate a new recording session
     * @param $recordingSession
     */
    public function initiateNewRecordingSession($data)
    {
        $recordingSession = new RecordingSession();
        $recordingSession->assigned_by = \Auth::guard('api')->user()->id;
        $recordingSession->date_start = Carbon::parse($data['start_date']);
        $recordingSession->date_end = Carbon::parse($data['end_date']);
        $recordingSession->patient()->associate($data['patient']);
        $recordingSession->session_pin = $data['patient'] . Carbon::now()->format('mm') . Carbon::now()->format('y');
        $recordingSession->save();
        $recordingSession->devices()->sync($data['devices']);
        $assignUsers = [];
        foreach($data['users'] as $user){
            if(is_array($user)) {
                foreach($user as $u) {
                    array_push($assignUsers, $u);
                }
            } else {
                array_push($assignUsers, $user);
            }
        }
        \App\Models\Patient::find($data['patient'])->users()->sync($assignUsers);
        return $recordingSession->save();
    }

    /**
     * Update a recording session
     * @param $recordingSession
     */
    public function updateRecordingSession($id, $data)
    {
        $recordingSession = $this->model->find($id);
        $recordingSession->date_start = Carbon::parse($data['start_date']);
        $recordingSession->date_end = Carbon::parse($data['end_date']);
        $recordingSession->patient()->associate($data['patient']);
        $recordingSession->devices()->sync($data['devices']);
        $assignUsers = [];
        foreach($data['users'] as $user){
            if(is_array($user)) {
                foreach($user as $u) {
                    array_push($assignUsers, $u);
                }
            } else {
                array_push($assignUsers, $user);
            }
        }
        \App\Models\Patient::find($data['patient'])->users()->sync($assignUsers);
        return $recordingSession->save();
    }

    /**
     * Archive a recording session
     * @param $id
     * @return mixed
     */
    public function archiveRecordingSession($id)
    {
        $recordingSession = $this->model->find($id);
        $recordingSession->date_end = Carbon::now()->toDateString();
        $recordingSession->is_archived = Carbon::now();
        $recordingSession->devices()->detach($recordingSession->devices->each(function($device){
                                                return $device->id;
                                            }));
        $recordingSession->save();
        return $recordingSession;
    }

    /**
     * Return the oAuth credentials for the assigning user
     * @param $recordingSession
     * @return mixed
     */
    private function getNetworkAdminCredentials($user_id)
    {
        $user = \App\Models\User::find($user_id);
        return $user->createToken('CAI LiveStream Personal Access Client', ['*'])->accessToken;
    }

}