<?php
namespace App\Services\Notes;

use App\Models\Note;
use App\Models\NoteCategory;
use App\Models\Recording;

/**
 * Class CreateNoteService
 * @package App\Services\Notes\CreateNoteService
 */
class CreateNoteService
{

    /**
     * @var Note
     */
    protected $model;

    /**
     * CreateNoteService constructor.
     * @param Note $model
     */
    public function __construct(Note $model)
    {
        $this->model = $model;
    }

    /**
     * @param $note
     * @return Note
     */
    public function createNewNote($recording_id, $data)
    {
        $note = new Note();
        $note->content = $data['content'];
        $note->user()->associate(\Auth::guard('api')->user()->id);
        $note->note_category()->associate(NoteCategory::find($data['note_category']));
        Recording::find($recording_id)->notes()->save($note);
        return $note;
    }

    /**
     * @param $note
     * @return bool
     */
    public function updateNote($data)
    {
        $note = $this->model->find($data['id']);
        return $note->update($data);
    }
}