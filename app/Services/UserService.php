<?php

namespace App\Services;

use App\Models\Location;
use App\Models\User;
use App\Transformers\UserTransformer;

/**
 * Class UserService
 * @package App\Services
 */
class UserService
{

    /**
     * @var User
     */
    protected $model;

    /**
     * @var UserTransformer
     */
    protected $transformer;

    /**
     * UserService constructor.
     * @param User $user
     */
    public function __construct(User $user, UserTransformer $transformer)
    {

        $this->model = $user;
        $this->transformer = $transformer;

    }

    /**
     * Create a user in the database
     * @param $userData
     */
    public function createUser($userData)
    {
        // Generate a random password to be emailed to the user
        $password = str_random(8);
        $userData['password'] = bcrypt($password);
        // Create the user in the database
        $user = $this->model->create($userData);
        // Return the result
        $user = $this->attachRoles($user, $userData['roles']);
        $user->networks()->attach($userData['network']);
        $user->save();
        if(isset($userData['locations'])) {
            $this->attachLocations($user, $userData['locations']);
        }
        return ['user' => $user, 'clean_pw' => $password];
    }

    /**
     * Edit a user in the database
     * @param User $userData, $editRolesNetworks
     * @return User
     */
    public function editUser(User $user, $userData,$editRolesNetworks = true)
    {
        if($editRolesNetworks){
            $user->detachAllRoles();
        }

        $user->update($userData);

        if($editRolesNetworks) {
            $user = $this->attachRoles($user, $userData['roles']);
            if(count($user->networks))
                $user->networks()->detach($user->networks->first()->id);
            $user->networks()->attach($userData['network']);
        }

        $user->save();

        if(isset($userData['locations'])) {
            $this->attachLocations($user, $userData['locations']);
        }

        return $user;
    }

    /**
     * A user updates their own account
     *
     * @param User $user
     * @param $data
     * @return User
     */
    public function updateUserAccount(User $user, $data)
    {
        if($data->has('name_first'))
            $user->name_first = $data['name_first'];
        if($data->has('name_last'))
            $user->name_last = $data['name_last'];
        if($data->has('username'))
            $user->username = $data['username'];
        if($data->has('password'))
            $user->password = bcrypt($data['password']);
        $user->save();
        return $user;
    }

    /**
     * Get the users
     * @return User
     */
    public function getUsers()
    {
        $query =  $this->model->with('locations', 'roles', 'networks')->orderBy('id', 'asc');
        $users = $query->paginate(10);
        $this->transformer->transformOutput($users->getCollection());
        return $users;
    }

    /**
     * Sort users by a column
     * @param $request
     * @return mixed
     */
    public function sortUsers($sort)
    {
        list($sortCol, $sortDir) = explode('|', $sort);
        $query = $this->model->with('locations', 'roles', 'networks')->orderBy($sortCol, $sortDir);
        $users = $query->paginate(10);
        $this->transformer->transformOutput($users->getCollection());
        return $users;
    }

    /**
     * Search for a user by name, email or company
     * @param $request
     * @return mixed
     */
    public function filterUsers($request)
    {
        $query = $this->model->with('locations', 'roles', 'networks')->where(function($q) use($request){
                        $value = "%{$request->filter}%";
                        $q->where('name_first', 'like', $value)
                            ->orWhere('name_last', 'like', $value)
                            ->orWhere('username', 'like', $value);

                    });

        $users = $query->paginate(10);
        $this->transformer->transformOutput($users->getCollection());
        return $users;
    }

    /**
     * Assign user to roles based on user input
     * @param User $user
     * @param $roles
     * @return User
     */
    public function attachRoles(User $user, $roles)
    {
        if(is_array($roles)) {
            foreach($roles as $role) {
                $user->attachRole($role);
            }
        } else {
            $user->attachRole($roles);
        }
        return $user;
    }

    /**
     * Attach users' locations for physicians
     * @param User $user
     * @param $locations
     */
    protected function attachLocations(User $user, $locations)
    {
            foreach($locations as $locationData) {
                if($locationData['location_id'])
                    $location = Location::find($locationData['location_id']);
                else
                    $location = new Location();

                $location->user_id = $user->id;
                $location->street = $locationData['location_street'];
                $location->street_2 = $locationData['location_street_2'];
                $location->city = $locationData['location_city'];
                $location->province = $locationData['location_province']['id'];
                $location->postal_code = $locationData['location_postal_code'];
                $location->phone = $locationData['location_phone'];
                $location->fax = $locationData['location_fax'];
                $location->save();
            }
    }

}