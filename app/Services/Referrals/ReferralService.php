<?php
/**
 * Created by PhpStorm.
 * User: home
 * Date: 12/22/16
 * Time: 1:35 PM
 */

namespace App\Services\Referrals;

use App\Mail\ReferralMail;
use Carbon\Carbon;
use App\Models\Note;
use App\Models\Referral;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

/**
 * Class ReferralService
 * @package App\Services\Referrals
 */
class ReferralService
{

    /**
     * @var Referral
     */
    protected $model;

    /**
     * ReferralService constructor.
     * @param Referral $model
     */
    public function __construct(Referral $model)
    {
        $this->model = $model;
    }

    /**
     * Get a collection of referrals for a user
     *
     * @return mixed
     */
    public function getUserReferrals()
    {
        $referrals = $this->model->get();
        return $referrals;
    }

    /**
     * Create a new referral in the database
     *
     * @param $data
     * @return Referral
     */
    public function storeNewReferral($data)
    {
        $referral = new Referral();
        $referral->user_id = $data['physician_id'];
        $referral->recording_id = $data['recording_id'];
        $referral->cleared_at = null;
        $file_name = $data['recording_id'] . '-' . time() . '.svg';
        Storage::disk('local')->put('ecg_graphs_svg/' . $file_name, $data['rendered_image']);
        $referral->image_file = $file_name;
        $referral->created_by = \Auth::guard('api')->user()->id;
        $referral->priority = $data['priority'];
        $referral->save();
        if($data['note']) {
            $this->createReferralNote($referral, $data['note'], $data['category_id']);
        }
        $this->sendEmailNotification($referral);
        return $referral;
    }

    /**
     * Update an existing referral in the database
     *
     * @param $data
     * @param $id
     */
    public function updateExistingReferral($data, $id)
    {
        $referral = $this->model->find($id);
        $referral->note = $data['note'];
        $referral->priority = $data['priority'];
        $referral->save();
        return $referral;
    }

    /**
     * Clear an existing referral as completed
     *
     * @param $id
     * @return mixed
     */
    public function clearExistingReferral($id)
    {
        $referral = $this->model->find($id);
        $referral->cleared_at = Carbon::now();
        $referral->save();
        return $referral;
    }

    /**
     * Create and associate a new note with a referral
     *
     * @param $referral
     * @param $note
     * @return mixed
     */
    protected function createReferralNote($referral, $note_content, $category)
    {
        $note = new Note();
        $note->content = $note_content;
        $note->note_category_id = $category;
        $note->user_id = \Auth::guard('api')->user()->id;
        return $referral->notes()->save($note);
    }

    /**
     * Send a referral notification via email
     *
     * @param $referral
     */
    protected function sendEmailNotification($referral)
    {
        Mail::to($referral->user->email)->send(new ReferralMail($referral->user, $referral));
    }

}