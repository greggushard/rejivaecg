<?php

namespace App\Services;

use Storage;
use App\Models\Patient;
use App\Transformers\NoteTransformer;
use App\Transformers\PatientTransformer;
use App\Transformers\ReportedEventTransformer;
use App\Services\Recordings\CreateRecordingService;

/**
 * Class PatientService
 * @package App\Services
 */
class PatientService
{

    /**
     * @var Patient
     */
    protected $model;

    /**
     * @var PatientTransformer
     */
    protected $transformer;

    /**
     * PatientService constructor.
     * @param Patient $patient
     */
    public function __construct(Patient $patient, PatientTransformer $transformer)
    {
        $this->model = $patient;
        $this->transformer = $transformer;
    }

    /**
     * Create a patient in the database
     * @param $patientData
     */
    public function createPatient($patientData)
    {
        // Create the patient in the database
        $patient = $this->model->create($patientData);
        return $patient;
    }

    /**
     * Return a list of available events to the front-end for disabling
     * specific events by id
     * @param $recordings
     */
    public function getAvailableEvents($events_count)
    {
            $addEvents = [];
            if ($events_count->artifact > 0) {
                array_push($addEvents, ["400" => $events_count->artifact]);
            }
            if ($events_count->noise > 0) {
                array_push($addEvents, ["401" => $events_count->noise]);
            }
            if ($events_count->unclassified_rhythm > 0) {
                array_push($addEvents, ["402" => $events_count->unclassified_rhythm]);
            }
            // Good Events
            if ($events_count->sinbrady_ivcd > 0) {
                array_push($addEvents, ["204" => $events_count->sinbrady_ivcd]);
            } 
            if ($events_count->sinbrady > 0) {
                array_push($addEvents, ["201" => $events_count->sinbrady]);
            } 
            if ($events_count->sintachy > 0) {
                array_push($addEvents, ["202" => $events_count->sintachy]);
            } 
            if ($events_count->nsr_ivcd > 0) {
                array_push($addEvents, ["203" => $events_count->nsr_ivcd]);
            } 
            if ($events_count->nsr > 0) {
                array_push($addEvents, ["200" => $events_count->nsr]);
            } 
            if ($events_count->sintachy_ivcd > 0) {
                array_push($addEvents, ["205" => $events_count->sintachy_ivcd]);
            }
            // Arrythmia Events
            if ($events_count->pause > 0) {
                array_push($addEvents, ["300" => $events_count->pause]);
            }
            if ($events_count->svc > 0) {
                array_push($addEvents, ["301" => $events_count->svc]);
            }
            if ($events_count->junctachy > 0) {
                array_push($addEvents, ["302" => $events_count->junctachy]);
            }
            if ($events_count->first_deg_avblock_nsr > 0) {
                array_push($addEvents, ["303" => $events_count->first_deg_avblock_nsr]);
            }
            if ($events_count->first_deg_avblock_sintachy > 0) {
                array_push($addEvents, ["304" => $events_count->first_deg_avblock_sintachy]);
            }
            if ($events_count->first_deg_avblack_sinbrady > 0) {
                array_push($addEvents, ["305" => $events_count->first_deg_avblack_sinbrady]);
            }
            if ($events_count->mobitz_i > 0) {
                array_push($addEvents, ["306" => $events_count->mobitz_i]);
            }
            if ($events_count->mobitz_ii > 0) {
                array_push($addEvents, ["307" => $events_count->mobitz_ii]);
            }
            if ($events_count->pac > 0) {
                array_push($addEvents, ["308" => $events_count->pac]);
            }
            if ($events_count->svta > 0) {
                array_push($addEvents, ["309" => $events_count->svta]);
            }
            if ($events_count->afib_slow > 0) {
                array_push($addEvents, ["310" => $events_count->afib_slow]);
            }
            if ($events_count->afib_noraml > 0) {
                array_push($addEvents, ["311" => $events_count->afib_noraml]);
            }
            if ($events_count->afib_rapid > 0) {
                array_push($addEvents, ["312" => $events_count->afib_rapid]);
            }
            if ($events_count->pvc > 0) {
                array_push($addEvents, ["313" => $events_count->pvc]);
            }
            if ($events_count->vcoup > 0) {
                array_push($addEvents, ["314" => $events_count->vcoup]);
            }
            if ($events_count->vtrip > 0) {
                array_push($addEvents, ["315" => $events_count->vtrip]);
            }
            if ($events_count->vbig > 0) {
                array_push($addEvents, ["316" => $events_count->vbig]);
            }
            if ($events_count->vtrig > 0) {
                array_push($addEvents, ["317" => $events_count->vtrig]);
            }
            if ($events_count->ivr > 0) {
                array_push($addEvents, ["318" => $events_count->ivr]);
            }
            if ($events_count->vt > 0) {
                array_push($addEvents, ["319" => $events_count->vt]);
            }
            if ($events_count->slow_vt > 0) {
                array_push($addEvents, ["320" => $events_count->slow_vt]);
            }
            if ($events_count->vf > 0) {
                array_push($addEvents, ["321" => $events_count->vf]);
            }
            if($events_count->light_headed > 0) {
                array_push($addEvents, ["1" => $events_count->light_headed]);
            }
            if($events_count->shortness_of_breath > 0) {
                array_push($addEvents, ["2" => $events_count->shortness_of_breath]);
            }
            if($events_count->fatigue > 0) {
                array_push($addEvents, ["3" => $events_count->fatigue]);
            }
            if($events_count->chest_pain > 0) {
                array_push($addEvents, ["4" => $events_count->chest_pain]);
            }
            if($events_count->heart_palpitation >0) {
                array_push($addEvents, ["5" => $events_count->heart_palpitation]);
            }
            if($events_count->audio_submission > 0) {
                array_push($addEvents, ["6" => $events_count->audio_submission]);
            }
            if($events_count->fall_detected > 0) {
                array_push($addEvents, ["9" => $events_count->fall_detected]);
            }

        return $addEvents;
    }

    /**
     * Grab the patient recordings
     * @param bool $offset
     * @param $session_id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function getPatientRecordings($offset = false, $session_id, $id)
    {
        if ($offset) {
            $patient = Patient::with(['recordings' => function ($q) use ($session_id, $offset) {
                $q->whereHas('recording_session', function ($q) use ($session_id) {
                    $q->where('recording_sessions.id', $session_id);
                })->orderBy('id', 'desc');
                $q->with('recording_session.events_count');
                $q->offset($offset)->limit(30);
            }])->find($id);
            $events_count = $patient->recordings->first()->recording_session->events_count;
            $availableEvents = $this->getAvailableEvents($events_count);
            return [
                        'recordings' => $patient->recordings->unique(),
                        'available_events' => $availableEvents
            ];
        } else {
            $patient = Patient::with(['recordings' => function ($q) use ($session_id) {
                $q->whereHas('recording_session', function ($q) use ($session_id) {
                    $q->where('recording_sessions.id', $session_id);
                })->orderBy('id', 'desc');
                $q->with('recording_session.events_count');
                $q->limit(16);
            }])->find($id);

            if ($patient->recordings->count() < 1) {
                return [
                    'recordings' => 'No recordings submitted',
                    'available_events' => null,
                    'last_recording' => null
                ];
            } else {
                if (Storage::disk('local')->get('raw_graph_file/' . $patient->recordings->first()->recording_hash . '.gph')) {
                    $data = Storage::disk('local')->get('raw_graph_file/' . $patient->recordings->first()->recording_hash . '.gph');;
                } else {
                    $data = [];
                }
                $recording_data = explode(',', $data);
                $value = 0;
                foreach ($recording_data as $k => $v) {
                    $recording_data[$k] = [$value, floatval($v)];
                    $value += 4;
                }
                $events_count = $patient->recordings->first()->recording_session->events_count;
                $availableEvents = $this->getAvailableEvents($events_count);
                $recordings = $patient->recordings->take(16);
            }
        }

        return [
            'recordings' => $recordings,
            'available_events' => $availableEvents,
            'last_recording' =>
                [
                    'id' => $patient->recordings->first()->id,
                    'notes' => (new NoteTransformer())->transformNotes($patient->recordings->first()->notes),
                    'events' => $patient->recordings->first()->events,
                    'is_clipped' => $patient->recordings->first()->clip,
                    'reported_events' => (new ReportedEventTransformer())->transformOutput(
                        (new CreateRecordingService())->mapExistingReportedEvents($patient->recordings->first()->recording_hash), $patient->recordings->first()),
                    'ecgData' => $recording_data,
                    'recording_remeasurements' => $patient->recordings->first()->remeasurements,
                    'alerts' => $patient->recordings->first()->alerts,
                    'body_position' => $patient->recordings->first()->body_position,
                    'breathing_rate' => $patient->recordings->first()->breathing_rate
                ]
        ];
    }

    /**
     * Edit a patient in the database
     * @param Patient $patientData
     * @return Patient
     */
    public function editPatient(Patient $patient, $patientData)
    {
        $patient->update($patientData);
        $patient->save();
        return $patient;
    }

    /**
     * Get the patients
     * @return Patient
     */
    public function getPatients()
    {
        $query =  $this->model->select('id', 'name_first', 'name_last', 'created_at', 'updated_at')
                            ->with('recording_sessions')
                            ->orderBy('id', 'asc');
        $patients = $query->paginate(10);
        $this->transformer->transformOutput($patients->getCollection());
        return $patients;
    }

    /**
     * Return only the patients that have been assigned to a user
     * @return mixed
     */
    public function getAssignedPatients()
    {
        $user = \Auth::guard('api')->user();
        $query =  $this->model->select('id', 'name_first', 'name_last', 'created_at', 'updated_at')
                                ->whereHas('users', function($builder) use($user) {
                                    $builder->where('user_id', $user->id);
                                })
                            ->with('recording_sessions')
                            ->orderBy('id', 'asc');
        $patients = $query->paginate(10);
        $this->transformer->transformOutput($patients->getCollection());
        return $patients;
    }

    /**
     * Sort patients by a column
     * @param $request
     * @return mixed
     */
    public function sortPatients($sort)
    {
        list($sortCol, $sortDir) = explode('|', $sort);
        $query = $this->model->select('id', 'name_first', 'name_last', 'created_at', 'updated_at')
                             ->with('recording_sessions')
                             ->orderBy($sortCol, $sortDir);
        $patients = $query->paginate(10);
        $this->transformer->transformOutput($patients->getCollection());
        return $patients;
    }


    /**
     * Search for a patient by name
     * @param $request
     * @return mixed
     */
    public function filterPatients($request)
    {
        $query = $this->model->where(function($q) use($request){
                        $value = "%{$request->filter}%";
                          $q->where('name_first', 'like', $value)
                            ->orWhere('name_last', 'like', $value);
                    })->select('id', 'name_first', 'name_last', 'created_at', 'updated_at')
                      ->with('recording_sessions');
        $patients = $query->paginate(10);
        $this->transformer->transformOutput($patients->getCollection());
        return $patients;
    }

}