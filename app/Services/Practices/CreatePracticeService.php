<?php
namespace App\Services\Practices;

use App\Models\Network;
use App\Models\Practice;

/**
 * Class CreatePracticeService
 * @package App\Services\Practice
 */
class CreatePracticeService {

    /**
     * @var Practice
     */
    protected $model;

    /**
     * CreatePracticeService constructor.
     * @param Practice $model
     */
    public function __construct(Practice $model)
    {

        $this->model = $model;

    }

    /**
     * Create a new practice entry in the database
     * @param $data
     */
    public function storePractice($practice)
    {

        # Something funky, the int's for the options are being sent as strings
        # Casing as ints until front-end can modify it

        $add_users = [];

        if($practice['users']) {
            foreach($practice['users'] as $user) {
                array_push($add_users, (int) $user);
            }
        }
        $network_id = (int) $practice['network_id'];
        $practice = $this->model->create($practice);
        $practice->network()->associate($network_id);
        $practice->users()->attach($add_users);
        $practice->save();
        return $practice;

    }



}