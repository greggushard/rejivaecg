<?php

namespace App\Services\Practices;

use App\Models\Practice;

/**
 * Class UpdatePracticeService
 * @package App\Services\Practices
 */
class UpdatePracticeService
{

    /**
     * @var Practice
     */
    protected $model;

    /**
     * UpdatePracticeService constructor.
     * @param Practice $model
     */
    public function __construct(Practice $model)
    {

        $this->model = $model;

    }

    /**
     * Update a practice in the database
     *
     * @param $practice
     * @return bool
     */
    public function updatePractice($practice)
    {

        $add_users = [];

        if($practice['users']) {
            foreach($practice['users'] as $user) {
                array_push($add_users, (int) $user);
            }
        }
        $network_id = (int) $practice['network_id'];
        $this->model->update($practice);
        $this->model->network()->associate($network_id);
        $this->model->users()->sync($add_users);
        $practice = $this->model->save();
        return $practice;

    }

}