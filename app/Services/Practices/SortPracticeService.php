<?php
namespace App\Services\Practices;

use App\Models\Practice;
use App\Transformers\PracticeTransformer;

class SortPracticeService
{

    /**
     * @var Practice
     */
    protected $model;

    protected $transformer;

    /**
     * SortPracticeService constructor.
     * @param Practice $model
     */
    public function __construct(Practice $model, PracticeTransformer $transformer)
    {

        $this->model = $model;
        $this->transformer = $transformer;

    }

    /**
     * Return a list of networks
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getPractices()
    {

        $query =  $this->model->with('users', 'network')->orderBy('id', 'asc');
        $practices = $query->paginate(10);
        return $practices;

    }

    public function sortPractices($sort)
    {

        list($sortCol, $sortDir) = explode('|', $sort);
        $query = $this->model->with('users', 'network')->orderBy($sortCol, $sortDir);
        $practices = $query->paginate(10);
        $this->transformer->transformOutput($practices->getCollection());
        return $practices;

    }

    public function filterPractices($request)
    {

        $query = $this->model->with('users', 'network')->where(function($q) use($request){
            $value = "%{$request->filter}%";
            $q->where('name', 'like', $value);
        });
        $practices = $query->paginate(10);
        $this->transformer->transformOutput($practices->getCollection());
        return $practices;

    }
}