<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Redis;
use App\Models\Recording;

/**
 * Class RedisService
 * @package App\Services
 */
class RedisService
{

    /**
     * RedisService constructor.
     * @param User $user
     */
    public function __construct(User $user, $rawData)
    {
        $md5 = md5($user->id, time());
        Redis::set($md5, $rawData);
    }


}