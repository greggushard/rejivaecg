<?php

namespace App\Services\Devices;

use App\Models\Device;

/**
 * Class CreateOrUpdateDeviceService
 * @package App\Services\Devices
 */
class CreateOrUpdateDeviceService
{

    /**
     * @var Device
     */
    protected $model;

    /**
     * CreateOrUpdateDeviceService constructor.
     * @param Device $model
     */
    public function __construct(Device $model)
    {
        $this->model = $model;
    }

    /**
     * @param $device
     * @return mixed
     */
    public function createDevice($data)
    {
        $data['device_id'] = hexdec(substr($data['radio_id'], 4));
        $device = $this->model->create($data);
        $device->firmware()->associate(1);
        $device->networks()->attach($data['network_id']);
        $device->save();
        return $device;
    }

    /**
     * @param $device
     * @return mixed
     */
    public function updateDevice($id, $data)
    {
        $device = $this->model->whereHas('networks', function($q) use ($data) {
                        $q->where('networks.id', $data['network_id']);
                    })->where('id', $id)->get()->first();
        $device->radio_id = $data['radio_id'];
        $device->device_id = hexdec(substr($data['radio_id'], 4));
        $device->save();
        return $device;
    }
}