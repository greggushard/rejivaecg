<?php
namespace App\Services\Devices;

use App\Models\Device;
use App\Transformers\DeviceTransformer;

class SortDeviceService
{

    /**
     * @var Practice
     */
    protected $model;

    protected $transformer;

    /**
     * SortPracticeService constructor.
     * @param Practice $model
     */
    public function __construct(Device $model, DeviceTransformer $transformer)
    {
        $this->model = $model;
        $this->transformer = $transformer;
    }

    /**
     * Return a list of networks
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getDevices()
    {
        $query =  $this->model->with('active_recording_session.recordings')->where('enabled', 1)->orderBy('id', 'asc');
        $devices = $query->paginate(10);
        $this->transformer->transformOutput($devices->getCollection());
        return $devices;
    }

    /**
     * Sort devices
     * @param $sort
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function sortDevices($sort)
    {
        list($sortCol, $sortDir) = explode('|', $sort);
        $query = $this->model->with('active_recording_session')->where('enabled', 1)->orderBy($sortCol, $sortDir);
        $devices = $query->paginate(10);
        $this->transformer->transformOutput($devices->getCollection());
        return $devices;
    }

    /**
     * Search for devices with the radio id
     * @param $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function filterDevices($request)
    {
        $query = $this->model->with('active_recording_session')->where('enabled', 1)->where(function($q) use($request){
            $value = "%{$request->filter}%";
            $q->where('radio_id', 'like', $value);
            $q->orWhereHas('recording_sessions', function($q) use ($value) {
                $q->whereHas('patient', function($q) use($value){
                    $q->where('patients.name_first', 'like', $value);
                    $q->orWhere('patients.name_last', 'like', $value);
                });
            });
        });
        $devices = $query->paginate(10);
        $this->transformer->transformOutput($devices->getCollection());
        return $devices;
    }
}