<?php

namespace App\Services\Alerts;

use Carbon\Carbon;
use App\Models\Alert;
use App\Jobs\RenderReportGraph;

class AlertService
{

    /**
     * Store a new alert in the database
     *
     * @param $alert
     * @return bool
     */
    public function storeNewAlert($data)
    {
        $alert = new Alert();
        $alert->alert_type = $data['type'];
        $alert->recording_id = $data['recording_id'];
        $alert->patient_id = $data['patient_id'];
        $alert->event_id = $data['event_id'];
        $alert->significant_event = $data['significant_event'];
        $alert->save();
        dispatch(new RenderReportGraph($alert->recording));
        return $alert;
    }


    /**
     * Update an existing alert in the database
     *
     * @param $id
     * @param $alert
     * @return mixed
     */
    public function updateExistingAlert($id, $type)
    {
        $alert = Alert::findOrFail($id);
        if($type == 'flag') {
            $alert->flagged_at = Carbon::now();
            $alert->flagged_by = \Auth::guard('api')->user()->id;
            // Render the alerted graph for a report
            dispatch(new RenderReportGraph($alert->recording));
        }
        if($type == 'clear') {
            $alert->reviewed_at = Carbon::now();
            $alert->reviewed_by = \Auth::guard('api')->user()->id;
        }
        $alert->save();
        return $alert;
    }

}