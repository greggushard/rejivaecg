<?php
namespace App\Services\Firmware;

use App\Models\Device;
use App\Models\Firmware;

/**
 * Class FirmwareService
 * @package App\Services\Firmware
 */
class FirmwareService
{

    /**
     * @var mixed
     */
    protected $latest_firmware;

    /**
     * FirmwareService constructor.
     */
    public function __construct()
    {
        $this->latest_firmware = Firmware::all()->last();
    }

    /**
     * Get the current firmware for a device
     * @param $device_id
     * @return mixed
     */
    public function getCurrentFirmware($device_id)
    {
        $device = Device::with('firmware')->where('device_id', $device_id)->get()->first();

        return [
            'device_id' => $device_id,
            'current_firmware' => $device->firmware->firmware_version,
            'latest_firmware' => [
                'firmware_version' => $this->latest_firmware->firmware_version,
                'hardware_version' => $this->latest_firmware->hardware_version,
                'url'              => $this->latest_firmware->path
            ]
        ];
    }

    /**
     * Set a new firmware version for a device
     * @param $device_id
     * @param $firmware_version
     * @return mixed
     */
    public function upgradeToNewFirmware($device_id, $new_firmware)
    {
        $device = Device::with('firmware')->where('device_id', $device_id)->get()->first();
        $device->firmware_id = Firmware::where('firmware_version', $new_firmware)->get()->first()->id;
        $device->save();
        return [
            'device_id' => $device->device_id,
            'status' => 'Device upgraded to firmware version: ' . $new_firmware,
            'current_firmware' => $new_firmware,
        ];
    }

}