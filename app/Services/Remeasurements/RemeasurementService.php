<?php

namespace App\Services\Remeasurements;

use App\Models\Remeasurement;

/**
 * Class RemeasurementService
 * @package App\Services\Remeasurements
 */
class RemeasurementService
{

    /**
     * @var Remeasurement
     */
    protected $model;

    /**
     * RemeasurementService constructor.
     * @param Remeasurement $model
     */
    public function __construct(Remeasurement $model)
    {
        $this->model = $model;
    }

    /**
     * Create a reamsurement
     * @param $data
     * @return Remeasurement
     */
    public function createRemeasurement($data)
    {
        $remeasurement = new Remeasurement();
        $remeasurement->measurement_start   = $data['measurement_start'];
        $remeasurement->measurement_end     = $data['measurement_end'];
        $remeasurement->measurement_type    = $data['measurement_type'];
        $remeasurement->measurement_length  = $data['measurement_length'];
        $remeasurement->measurement_points  = json_encode($data['measurement_points']);
        $remeasurement->recording_id        = $data['recording_id'];
        $remeasurement->user_id             = \Auth::guard('api')->user()->id;
        $remeasurement->save();
        return $remeasurement;
    }

    /**
     * Update a remeasurement
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateRemeasurement($id, $data)
    {
        $remeasurement = Remeasurement::find($id);
        $remeasurement->measurement_start   = $data['measurement_start'];
        $remeasurement->measurement_end     = $data['measurement_end'];
        $remeasurement->measurement_type    = $data['measurement_type'];
        $remeasurement->measurement_length  = $data['measurement_length'];
        $remeasurement->measurement_points  = json_encode($data['measurement_points']);
        $remeasurement->updated_by          = \Auth::guard('api')->user()->id;
        $remeasurement->save();
        return $remeasurement;
    }
}