<?php

namespace App\Services;

use App\Models\Notification;
use App\Models\User;
use App\Models\Network;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use GeniusTS\Roles\Models\Role;

/**
 * Class NotificationService
 * @package App\Services
 */
class NotificationService
{

    /**
     * @var Notification
     */
    protected $model;

    /**
     * NotificationService constructor.
     * @param Notification $notification
     */
    public function __construct(Notification $notification)
    {
        $this->model = $notification;
    }


    /**
     * Get unread notifications for a user
     * @param User $user
     * @return mixed
     */
    public function getNewNotificationsForUser($user_id)
    {
        $user = User::with('unread_notifications')->find($user_id);
        return $user->unread_notifications;
    }

    /**
     * Get the latest read notifications for a user
     * @param $user_id
     * @return mixed
     */
    public function getLatestNotificationsForUser($user_id)
    {
        $user = User::with('read_notifications')->find($user_id);
        return $user->read_notifications;
    }

    /**
     * Store a notification
     * @param $type
     * @param $content
     * @return static
     */
    public function storeNotification($type, $content)
    {
        return $this->model->create(['type' => $type, 'content' => $content]);
    }

    /**
     * Assign a notification for a specific role
     * @param Notification $notification
     * @param Role $role
     * @return mixed
     */
    public function assignNotificationForRole(Notification $notification, $role_id)
    {
        $users = User::whereHas('roles', function($query) use($role_id){
                    $query->where('roles.id', $role_id);
                })->get();

        foreach($users as $user) {
            $user->notifications()->attach($notification->id);
        }

        return $users;
    }

    /**
     * Assign a notification to a user
     *
     * @param Notification $notification
     * @param User $user
     * @return User
     */
    public function assignNotificationForUser(Notification $notification, User $user)
    {
        $user->notifications()->attach($notification->id);
        return $user;
    }

    /**
     * Assign notifications to users in a network
     *
     * @param Notification $notification
     * @param Network $network
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function assignNotificationsToUsersInNetwork(Notification $notification, $users)
    {
        $users = $users->get();
        foreach($users as $user) {
            $user->notifications()->attach($notification->id);
        }
        return $users;
    }

    /**
     * Mark a notification as viewed for a user
     * @param $id
     * @param $user_id
     * @return mixed
     */
    public function markNotificationAsReadForUser($id, $user_id)
    {
        $user = User::with('notifications')->find($user_id);
        if(!is_array($id))
            $user->notifications()->sync([$id], ['viewed_at' => Carbon::now()], true);
        else
            $user->notifications()->sync($id, ['viewed_at' => Carbon::now()], true);

        return $user->notifications;
    }

}