<?php
namespace App\Services\Metrics;

use App\Models\ActivityLog;
use Carbon\Carbon;
use App\Models\Recording;
use App\Models\EcgEventsCount;

class MetricsService
{

    /**
     * Get a count of network recordings by month
     * @param $network_id
     */
    public function getNetworkRecordingsMetric($network_id)
    {
        $today = Carbon::now();
        $yesterday = Carbon::now()->subHours(24);
        $twoDaysAgo = Carbon::now()->subHours(48);
        $threeDaysAgo = Carbon::now()->subHours(72);
        $fourDaysAgo = Carbon::now()->subHours(96);
        $fiveDaysAgo = Carbon::now()->subHours(120);
        $sixDaysAgo = Carbon::now()->subHours(144);
        $sevenDaysAgo = Carbon::now()->subHours(168);

        $recordings_today = Recording::whereHas('patient', function($q) use($network_id) {
                                   $q->whereHas('network', function($q) use($network_id) {
                                       $q->where('networks.id', $network_id);
                                   });
                             })->whereBetween('created_at', [$today->format('Y-m-d 00:00:00'), $today->format('Y-m-d 23:59:59')])->count();

        $recordings_yesterday = Recording::whereHas('patient', function($q) use($network_id){
                                    $q->whereHas('network', function($q) use($network_id){
                                        $q->where('networks.id', $network_id);
                                    });
                                })->whereBetween('created_at', [$yesterday->format('Y-m-d 00:00:00'), $yesterday->format('Y-m-d 23:59:59')])->count();

        $recordings_twoDaysAgo = Recording::whereHas('patient', function($q) use($network_id){
                                    $q->whereHas('network', function($q) use($network_id){
                                        $q->where('networks.id', $network_id);
                                    });
                                })->whereBetween('created_at', [$twoDaysAgo->format('Y-m-d 00:00:00'), $twoDaysAgo->format('Y-m-d 23:59:59')])->count();

        $recordings_threeDaysAgo = Recording::whereHas('patient', function($q) use($network_id){
                                        $q->whereHas('network', function($q) use($network_id){
                                            $q->where('networks.id', $network_id);
                                        });
                                    })->whereBetween('created_at', [$threeDaysAgo->format('Y-m-d 00:00:00'), $threeDaysAgo->format('Y-m-d 23:59:59')])->count();

        $recordings_fourDaysAgo = Recording::whereHas('patient', function($q) use($network_id){
                                        $q->whereHas('network', function($q) use($network_id){
                                            $q->where('networks.id', $network_id);
                                        });
                                  })->whereBetween('created_at', [$fourDaysAgo->format('Y-m-d 00:00:00'), $fourDaysAgo->format('Y-m-d 23:59:59')])->count();

        $recordings_fiveDaysAgo = Recording::whereHas('patient', function($q) use($network_id){
                                        $q->whereHas('network', function($q) use($network_id){
                                            $q->where('networks.id', $network_id);
                                        });
                                    })->whereBetween('created_at', [$fiveDaysAgo->format('Y-m-d 00:00:00'), $fiveDaysAgo->format('Y-m-d 23:59:59')])->count();

        $recordings_sixDaysAgo = Recording::whereHas('patient', function($q) use($network_id){
                                    $q->whereHas('network', function($q) use($network_id){
                                        $q->where('networks.id', $network_id);
                                    });
                                })->whereBetween('created_at', [$sixDaysAgo->format('Y-m-d 00:00:00'), $sixDaysAgo->format('Y-m-d 23:59:59')])->count();

        $recording_sevenDaysAgo = Recording::whereHas('patient', function($q) use($network_id){
                                    $q->whereHas('network', function($q) use($network_id){
                                        $q->where('networks.id', $network_id);
                                    });
                                })->whereBetween('created_at', [$sevenDaysAgo->format('Y-m-d 00:00:00'), $sevenDaysAgo->format('Y-m-d 23:59:59')])->count();

        return [
                    'today'         => $recordings_today,
                    'yesterday'     => $recordings_yesterday,
                    'twoDaysAgo'    => $recordings_twoDaysAgo,
                    'threeDaysAgo'  => $recordings_threeDaysAgo,
                    'fourDaysAgo'   => $recordings_fourDaysAgo,
                    'fiveDaysAgo'   => $recordings_fiveDaysAgo,
                    'sixDaysAgo'    => $recordings_sixDaysAgo,
                    'sevenDaysAgo'  => $recording_sevenDaysAgo
        ];
    }

    /**
     * Get a count of reports ran in the last few days
     * @param $network_id
     */
    public function getNetworkReportsMetric($network_id)
    {
        $arythmias = EcgEventsCount::whereHas('recording_session.patient.network', function($q) use($network_id) {
                                $q->where('networks.id', $network_id);
                            })->get();

        $light_headed = 0;
        $fatigue = 0;
        $chest_pain = 0;
        $heart_palpitation = 0;
        $fall_detected = 0;

        foreach($arythmias as $ary) {
            $light_headed = $light_headed + $ary->light_headed;
            $fatigue = $fatigue + $ary->fatigue;
            $chest_pain = $chest_pain + $ary->chest_pain;
            $heart_palpitation = $heart_palpitation + $ary->heart_palpitation;
            $fall_detected = $fall_detected + $ary->fall_detected;
        }

        return [
                    'light_headed'      => $light_headed,
                    'fatigue'           => $fatigue,
                    'chest_pain'        => $chest_pain,
                    'heart_palpitation' => $heart_palpitation,
                    'fall_detected'     => $fall_detected
        ];
    }

    /**
     * Get a log of user activity within a network
     * @param $network_id
     */
    public function getNetworkUserActivityMetric($network_id)
    {
        $arythmias = EcgEventsCount::whereHas('recording_session.patient.network', function($q) use($network_id) {
            $q->where('networks.id', $network_id);
        })->get();

        $afib_rapid = 0;
        $pause = 0;
        $vt = 0;
        $vf = 0;
        $mobitz_i = 0;
        $mobitz_ii = 0;
        $afib_slow = 0;
        $afib_normal = 0;
        $svta = 0;

        foreach($arythmias as $ary) {
            $afib_rapid = $afib_rapid + $ary->afib_rapid;
            $pause = $pause + $ary->pause;
            $vt = $vt + $ary->vt;
            $vf = $vf + $ary->vf;
            $mobitz_i = $mobitz_i + $ary->mobitz_i;
            $mobitz_ii = $mobitz_ii + $ary->mobitz_ii;
            $afib_slow = $afib_slow + $ary->afib_slow;
            $afib_normal = $afib_normal + $ary->afib_normal ;
            $svta = $svta + $ary->svta ;
        }

        return [
            'afib_rapid'    => $afib_rapid,
            'pause'         => $pause,
            'vt'            => $vt,
            'vf'            => $vf,
            'mobitz_i'      => $mobitz_i,
            'mobitz_ii'     => $mobitz_ii,
            'afib_slow'     => $afib_slow,
            'afirb_normal'  => $afib_normal,
            'svta'          => $svta
        ];
    }

    /**
     * Get the activity log of users in a network
     * @param $network_id
     * @return mixed
     */
    public function getActivityLog($network_id)
    {
        return ActivityLog::whereHas('user.networks', function($q) use($network_id){
            $q->where('networks.id', $network_id);
        })->orderBy('created_at', 'desc')->get();
    }

}