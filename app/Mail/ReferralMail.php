<?php

namespace App\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class WelcomeMail
 * @package App\Mail
 */
class ReferralMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var
     */
    public $user;

    /**
     * @var
     */
    public $referral;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $referral)
    {
        $this->user = $user;
        $this->referral = $referral;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->referral->priority == 3)
            $priority = "Urgent";
        elseif($this->referral->priority == 2)
            $priority = "Medium";
        elseif($this->referral->priority == 1)
            $priority = "Low";

        return $this->from('noreply@rijuven.com')
                    ->subject('New Referral - ' . ucwords($priority) . " - " . Carbon::now()->format('m/d/Y'))
                    ->priority($this->referral->priority)
                    ->view('emails.referrals.new');
    }
}
