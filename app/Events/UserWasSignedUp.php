<?php

namespace App\Events;

use App\Mail\WelcomeMail;
use App\Models\Notification;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use App\Models\User;
use Mail;
use App\Services\NotificationService;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Mockery\Matcher\Not;

class UserWasSignedUp implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;

    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, $pw)
    {
        $this->user = $user;
        $this->user->clean_pw = $pw;

        Mail::to($user->email)->send(new WelcomeMail($this->user));

        $service = new NotificationService(new Notification());
        $notification = $service->storeNotification('user_register', $this->user->email . ' registered.');
        $service->assignNotificationForRole($notification, 1);
        event(new GeneratePublicAdminNotification($notification));
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['user-activity'];
    }
}