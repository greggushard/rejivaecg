<?php

namespace App\Events;

use App\Models\Notification;
use App\Models\Patient;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Class GeneratePatientNotification
 * @package App\Events
 */
class GeneratePatientNotification implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;

    public $notification;

    public $patient;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Patient $patient, Notification $notification)
    {
        $this->notification = $notification;
        $this->patient = $patient;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array=
     */
    public function broadcastOn()
    {
        $channel = md5('patient-channel-'.$this->patient->id);
        return [$channel];
    }
}
