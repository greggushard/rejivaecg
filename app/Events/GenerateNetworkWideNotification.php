<?php

namespace App\Events;

use App\Models\Notification;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Class GeneratePublicAdminNotification
 * @package App\Events
 */
class GenerateNetworkWideNotification implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;

    public $notification;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array=
     */
    public function broadcastOn()
    {
        $network_id = $this->notification->users()->first()->networks()->first()->id;
        $channel = md5('network-channel-'.$network_id);
        return [$channel];
    }
}
