<?php

namespace App\Models;

use Carbon\Carbon;
use App\Traits\NoteableTrait;
use GuzzleHttp\Client;
use Symfony\Component\Process\Process;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Recording
 * @package App\Models
 */
class Recording extends Model
{

    use SoftDeletes, NoteableTrait;

    /**
     * @var array
     */
    protected $fillable = [
        'recording_hash',
        'recordings_missed',
    ];

    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'timestamp'];

    /**
     * A recording belongs to one patient
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo('App\Models\Patient');
    }

    /**
     * A recording may have many alerts, including red or yellow based on the events
     * found within the recording
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function alerts()
    {
        return $this->hasOne('App\Models\Alert');
    }

    /**
     * A recording has one entry of events
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasOne('App\Models\Event');
    }

    /**
     * A recording belongs to one recording session
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recording_session()
    {
        return $this->belongsTo('App\Models\RecordingSession');
    }

    /**
     * A recording is saved by many users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    /**
     * A recording may have many remeasurements
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function remeasurements()
    {
        return $this->hasMany('App\Models\Remeasurement');
    }

    /**
     * A recording may have one clip
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function clip()
    {
        return $this->hasOne('App\Models\Clip');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function interpretation()
    {
        return $this->hasOne('App\Models\Interpretation');
    }

    /*
    |------------------------------------------------------------
    |MONEBO ALGORITHM FUNCTIONS
    |------------------------------------------------------------
    */

    /**
     * @param $recording_id
     * @return array
     */
    public function runMoneboEcgAlogrithm($recording_id, $sampleRate = null)
    {
        $input_file = storage_path('app/ecg_normalized_shifted/' . $recording_id . '.gph');
        $output_dir = storage_path('app/ecg_monebo_output/');

        if(!$sampleRate) {
            $cmd = '/var/cardiosleeve/moneboprogram  --output ' . $output_dir . ' ' . $input_file;
        } else {
            $cmd = '/var/cardiosleeve/moneboprogram --sample-rate ' . $sampleRate . ' --output ' . $output_dir . ' ' . $input_file;
        }

        try {
            $process = new Process($cmd);
            $process->run();
        } catch (Exception $e) {
            # Monebo algo failed:
            return [
                'event_time'        => "0:00:00.000",
                'sample_number'     => 1,
                'description_full'  => 'Not Available',
                'description_short' => 'Not Available',
                'confidence_index'  => '0%',
            ];
        }

        if ( !$process->isSuccessful() ) {
            # Monebo algo failed:
            return [
                'event_time'        => "0:00:00.000",
                'sample_number'     => 1,
                'description_full'  => 'Not Available',
                'description_short' => 'Not Available',
                'confidence_index'  => '0%',
            ];
        }

        $monebo_json_output = Storage::disk('local')->get('ecg_monebo_output/' . $recording_id . '.json');
        $return = $this->transformRawMoneboEcgResults($monebo_json_output);

        return $return;
    }


    /**
     * @param $strJsonResults
     * @return array
     */
    public function transformRawMoneboEcgResults($strJsonResults)
    {
        $objResults         = json_decode($strJsonResults);
        $results            = $objResults->response->ecg_results;
        $strEvents          = json_encode($results->events);
        $aryEvents          = json_decode($results->events);
        $aryFilteredEvents  = $this->filterMoneboEcgEventResults($aryEvents);

        $aryReturn = [
            'algorithm_version'         => $results->algorithm_version,
            'bucket'                    => $results->bucket,
            'noise'                     => $results->noise,
            'complete_confidence_index' => $results->complete_confidence_index,
            'number_of_events'          => $results->number_of_events,
            'events_raw_json_data'      => $strEvents,
            'events_filtered_json_data' => $aryFilteredEvents,
            'events' => json_encode($aryFilteredEvents)
        ];

        return $aryReturn;
    }


    /**
     * @param array $aryEvents
     * @return array
     */
    public function filterMoneboEcgEventResults(array $aryEvents)
    {
        $aryBadEvents       = config('monebo_ecg.bad_events');
        $aryNormalEvents    = config('monebo_ecg.normal_events');

        // Mark noise events
        $aryInitialFlaggedEvents    = array_map(function($objEvent) use ($aryBadEvents) {
            if ( in_array($objEvent->DESCRIPTION_SHORT, $aryBadEvents) || in_array($objEvent->DESCRIPTION_FULL, $aryBadEvents) ) {
                $objEvent->NOISE_EVENT = true;
            } else {
                $objEvent->NOISE_EVENT = false;
            }
            return $objEvent;
        }, $aryEvents);

        // Mark abnormal events
        $aryFinalFlaggedEvents      = array_map(function($objEvent) use ($aryNormalEvents) {
            if ( $objEvent->NOISE_EVENT == false && in_array($objEvent->DESCRIPTION_SHORT, $aryNormalEvents) ) {
                $objEvent->ABNORMAL_EVENT = false;
            } else {
                $objEvent->ABNORMAL_EVENT = true;
            }
            return $objEvent;
        }, $aryInitialFlaggedEvents);

        // Mark duration
        foreach ($aryFinalFlaggedEvents as $objEvent) {
            $objEventTime   = new Carbon($objEvent->EVENT_TIME);
            $intEventNumber = (int)$objEvent->EVENT_NUMBER;
            $fltMark        = (float)($objEventTime->second.'.'.$objEventTime->micro);

            if ($intEventNumber === 1) {
                $objEvent->MARK     = $fltMark;
                continue;
            }

            $intPreviousEventIndex  = $intEventNumber - 2;
            $fltPreviousMark        = $aryFinalFlaggedEvents[$intPreviousEventIndex]->MARK;
            $fltDuration            = $fltMark - $fltPreviousMark;
            $objEvent->MARK         = $fltMark;
        }

        $aryReturn = $aryFinalFlaggedEvents;

        return $aryReturn;
    }


    /**
     * @param array $aryFilteredEvents
     * @return null
     */
    public function findMostSignificantEvent(array $aryFilteredEvents)
    {

        $aryNonNoiseEvents  = [];
        $aryAbnormalEvents  = [];
        $aryNormalEvents    = [];
        $objReturnEvent     = null;

        // Remove noise events
        foreach ($aryFilteredEvents as $objFilteredEvent) {
            if ($objFilteredEvent->NOISE_EVENT === false) {
                array_push($aryNonNoiseEvents, $objFilteredEvent);
            } else {
                unset(
                    $objFilteredEvent->ABNORMAL_EVENT,
                    $objFilteredEvent->MARK,
                    $objFilteredEvent->DURATION
                );
            }
            unset($objFilteredEvent->NOISE_EVENT);
        }

        // Single out abnormal events
        foreach ($aryNonNoiseEvents as $objNonNoiseEvent) {
            if ($objNonNoiseEvent->ABNORMAL_EVENT == true) {
                array_push($aryAbnormalEvents, $objNonNoiseEvent);
            } else {
                array_push($aryNormalEvents, $objNonNoiseEvent);
            }
            unset($objNonNoiseEvent->ABNORMAL_EVENT);
        }

        // Find normal event with greatest duration
        if ($aryNormalEvents > 0) {
            $fltLongestDuration = 0.0;
            foreach ($aryNormalEvents as $objNormalEvent) {
                if ($objNormalEvent->DURATION > $fltLongestDuration) {
                    $fltLongestDuration = $objNormalEvent->DURATION;
                    $objReturnEvent     = $objNormalEvent;
                }
                unset(
                    $objNormalEvent->MARK,
                    $objNormalEvent->DURATION
                );
            }
        }

        // Find abnormal event with greatest duration
        if ($aryAbnormalEvents > 0) {// This takes priority over a normal event
            $fltLongestDuration = 0.0;
            foreach ($aryAbnormalEvents as $objAbnormalEvent) {
                if ($objAbnormalEvent->DURATION > $fltLongestDuration) {
                    $fltLongestDuration = $objAbnormalEvent->DURATION;
                    $objReturnEvent     = $objAbnormalEvent;
                }
                unset(
                    $objAbnormalEvent->MARK,
                    $objAbnormalEvent->DURATION
                );
            }
        }

        return $objReturnEvent;

    }

    /**
     * Get a normalized ECG Array from Redis
     *
     * @return array
     */
    public function getNormalizedEcgFileAsArray()
    {
        $recording_hash = $this->recording_hash;
        return (array)Storage::disk('local')->get('raw_graph_file/'. $recording_hash .'.gph');
    }

    /**
     * Scope the number of recordings in a recording session
     * @return mixed
     */
    public function scopeWithRecordingsCount($q)
    {

    }

}
