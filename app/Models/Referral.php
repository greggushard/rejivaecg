<?php

namespace App\Models;

use App\Traits\ReferralTrait;
use App\Traits\NoteableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Referral
 * @package App
 */
class Referral extends Model
{

    /**
     * Traits
     */
    use SoftDeletes, ReferralTrait, NoteableTrait;

    /**
     * @var array
     */
    protected $fillable = ['cleared_at', 'priority', 'rendered_image'];

    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'cleared_at', 'deleted_at'];

    /**
     * A referral belongs to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * A referral has a creator belonging to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
    }

    /**
     * A referral has an updater belonging to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updater()
    {
        return $this->belongsTo('App\Models\User', 'updated_by', 'id');
    }

    /**
     * A referral belongs to a recording
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recording()
    {
        return $this->belongsTo('App\Models\Recording');
    }

}
