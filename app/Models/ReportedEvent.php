<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReportedEvent
 * @package App\Models
 */
class ReportedEvent extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'timestamp',
        'breathing_rate',
        'battery_level',
        'body_position',
        'phone_id',
        'heart_rate'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'timestamp'
    ];


    /**
     * A reported event will belong to one patient
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo('App\Models\Patient');
    }

    /**
     * A reported event will be recorded on only one device
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function device()
    {
        return $this->belongsTo('App\Models\Device');
    }

}
