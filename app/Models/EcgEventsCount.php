<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class EcgEventsCount
 * @package App\Models
 */
class EcgEventsCount extends Model
{

    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
          'recording_session_id',
          'noise',
          'unclassified_rhythm',
          'unclassified_beats',
          'nsr',
          'sinbrady',
          'sintachy',
          'nsr_ivcd',
          'sinbrady_ivcd',
          'sintachy_ivcd',
          'pause',
          'svc',
          'junctachy',
          '1deg_avblock_nsr',
          '1deg_avblock_sintachy',
          '1deg_avblack_sinbrady',
          'mobitz_i',
          'mobitz_ii',
          'pac',
          'svta',
          'afib_slow',
          'afib_normal',
          'afrib_rapid',
          'pvc',
          'vcoup',
          'vtrig',
          'vbig',
          'vtrip',
          'ivr',
          'vt',
          'slow_vt',
          'vf',
          'light_headed',
          'shortness_of_breath',
          'fatigue',
          'chest_pain',
          'heart_palpitation',
          'audio_submission',
          'fall_detected',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * An event counter has one recording session
     */
    public function recording_session()
    {
       return $this->belongsTo('App\Models\RecordingSession');
    }

}
