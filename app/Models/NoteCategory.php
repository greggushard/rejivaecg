<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class NoteCategory
 * @package App
 */
class NoteCategory extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['description'];

    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * A note category has many notes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany('App\Models\Note');
    }
}
