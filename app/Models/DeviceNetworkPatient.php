<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceNetworkPatient extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'device_network_patient';

    /**
     * @var array
     */
    protected $fillable = ['device_network_id', 'patient_id', 'temporary_code', 'date_start', 'date_end'];

    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'date_start', 'date_end'];


}
