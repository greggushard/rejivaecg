<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class Event
 * @package App\Models
 */
class Event extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['event_payload'];

    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * An event payload may have many types of alerts
     * based on the events found in the payload
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function alerts()
    {
        return $this->hasMany('App\Models\Alert');
    }

    /**
     * An event belongs to one recording ID
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recording()
    {
        return $this->belongsTo('App\Models\Recording');
    }
}
