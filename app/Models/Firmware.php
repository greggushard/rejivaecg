<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Firmware
 * @package App\Models
 */
class Firmware extends Model
{


    protected $fillable = ['device_name', 'hardware_version', 'status', 'path'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * A firmware version has many devices
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    protected function device()
    {
        return $this->hasMany('App\Models\Device');
    }
}
