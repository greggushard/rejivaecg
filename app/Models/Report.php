<?php

namespace App\Models;

use App\Traits\NoteableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Report
 * @package App\Models
 */
class Report extends Model
{

    use SoftDeletes, NoteableTrait;

    /**
     * @var array
     */
    protected $fillable = [
        'file_slug',
        'report_type'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * A report belongs to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'created_by');
    }

    /**
     * A report belongs to a patient
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo('App\Models\Patient');
    }

    /**
     * A report belongs to a recording session
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recording_session()
    {
        return $this->belongsTo('App\Models\RecordingSession');
    }
}
