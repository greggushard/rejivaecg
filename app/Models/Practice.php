<?php

namespace App\Models;

use App\Scopes\PracticeLevelScope;
use Illuminate\Database\Eloquent\Model;

class Practice extends Model
{
    protected $fillable = ['name'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {

        parent::boot();

        if(\Auth::guard('api')->user()) {
            if(\Auth::guard('api')->user()->hasRole('network-admin') && !\Auth::guard('api')->user()->hasRole('admin')) {
                static::addGlobalScope(new PracticeLevelScope());
            }
        }

    }


    /**
     * A practice has one Network
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function network()
    {

        return $this->belongsTo('App\Models\Network');

    }

    /**
     * A practice may have many users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {

        return $this->belongsToMany('App\Models\User')
                    ->withTimestamps();

    }

    /**
     * A Practice may have many patients
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function patients()
    {

        return $this->belongsToMany('App\Models\Patient')
                    ->withTimestamps();

    }

}
