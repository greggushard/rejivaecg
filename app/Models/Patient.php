<?php

namespace App\Models;

use Config;
use Carbon\Carbon;
use App\Traits\PatientTrait;
use App\Traits\NoteableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Patient extends Model
{

    use SoftDeletes, PatientTrait, NoteableTrait;

    /**
     * @var array
     */
    protected $fillable = [
        'name_first',
        'name_title',
        'name_last',
        'name_middle',
        'dob',
        'gender',
        'health_card_number',
        'health_card_expiration',
        'address_street1',
        'address_city',
        'address_province',
        'address_postal_code',
        'address_country',
        'phone1',
        'phone1_type',
        'phone2',
        'phone2_type',
        'email',
        'height',
        'weight',
        'patient_history',
        'network_id',
    ];

    /**
     * Casted Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'dob',
        'health_card_expiration'
    ];

    /**
     * A patient can be assigned by one user
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function assignor()
    {
        return $this->hasOne('App\Models\User', 'assigned_by', 'id');
    }

    /**
     * Transform the province attribute to a readable province
     * @param $val
     */
    public function getProvinceAttribute($val)
    {
        $provinces = collect(Config::get('available_locations'));
        return $provinces->where('id', $val)->first();
    }

    /**
     * A patient belongs to one network
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function network()
    {
        return $this->belongsTo('App\Models\Network');
    }

    /**
     * A patient has many recordings
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recordings()
    {
        return $this->hasMany('App\Models\Recording');
    }

    /**
     * A patient has many recording sessions
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recording_sessions()
    {
        return $this->hasMany('App\Models\RecordingSession');
    }

    /**
     * A patient may have many alerts
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function alerts()
    {
        return $this->hasMany('App\Models\Alert');
    }

    /**
     * Return a list of patients who do not have an open recording session
     * @param $query
     * @return mixed
     */
    public function scopeAvailableForRecordingSession($query)
    {
        return $query->whereDoesntHave('recording_sessions', function($q) {
            $q->where('date_end', '>', Carbon::now());
        });
    }

    /**
     * Get the active recording session in the database
     * @param $query
     * @return mixed
     */
    public function scopeActiveRecordingSession($query)
    {
        return $query->whereHas('recording_sessions', function($q) {
            $q->where('date_end', '>', Carbon::now());
        })->with('recording_sessions');
    }

    /**
     * A patient may be assigned to many users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User')
                    ->withPivot('location_id')
                    ->withPivot('user_type')
                    ->withTimestamps();
    }

    /**
     * A patient belongs to many devices
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function devices()
    {
        return $this->belongsToMany('App\Models\Device')->withTimestamps();
    }

}
