<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timezone extends Model
{

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'timezone_key',
        'human',
        'utc'
    ];

}
