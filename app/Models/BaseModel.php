<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{

    protected $dates =
                        [
                            'created_at',
                            'updated_at',
                            'deleted_at'
                        ];

    /**
     * @param $value
     * @return Carbon
     */
    public function convertToUsersTimezone($value)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $value)->setTimezone('America/New_York');
    }
}