<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

/**
 * Class Notification
 * @package App\Models
 */
class Notification extends BaseModel
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'type', 'content'
    ];

    /**
     * Boot method
     */
    protected static function boot()
    {

        if(auth()->user()) {
            static::addGlobalScope('user', function(Builder $builder) {
                $builder->whereHas('users', function($q) {
                    $q->where('users.id', auth()->user()->id);
                });
            });
        } elseif(Auth::guard('api')->user()) {
            static::addGlobalScope('user', function(Builder $builder) {
                $builder->whereHas('users', function($q) {
                    $q->where('users.id', \Auth::guard('api')->user()->id);
                });
            });
        }

    }

    /**
     * A notification belongs to a user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    /*
  START COMMON ACCESSORS / MUTATORS
 */
    public function getUpdatedAtAttribute($value)
    {
        return $this->convertDateTimeToCarbonInUsersTZ($value);
    }

    public function getCreatedAtAttribute($value)
    {
        return $this->convertDateTimeToCarbonInUsersTZ($value);
    }
    /*
     END COMMON ACCESSORS / MUTATORS
    */


    /**
     * @param $value
     * @return mixed
     */
    public function convertDateTimeToCarbonInUsersTZ($value)
    {
        if (auth()->check() && !empty($value) && $value > 0 && $value != '0000-00-00 00:00:00') {
            return Carbon::createFromFormat('Y-m-d H:i:s', $value)
                        ->setTimezone('America/New_York')->diffForHumans();
        }
        return $value;
    }


    /**
     * Resetting dates to 00:00:00 time in user's TZ
     * There should be a better way to do this.
     *
     * @param $value
     * @return Carbon
     */
    public function convertDateToCarbonInUsersTZ($value) {
        if (auth()->check() && !empty($value) && $value > 0 && $value != '0000-00-00') {

            # yyyy-mm-dd
            if (str_contains($value, '-')) {
                $dateParts = explode('-', $value);
                $year = $dateParts[0];
                $month = $dateParts[1];
                $day = $dateParts[2];
            }
            # mm/dd/yyyy
            elseif(str_contains($value, '/')) {
                $dateParts = explode('/', $value);
                $year = $dateParts[2];
                $month = $dateParts[0];
                $day = $dateParts[1];
            }

            return Carbon::create((int)$year, (int)$month, (int)$day, 0, 0, 0, 'America/New_York');

        }

        return Carbon::parse($value);
    }

}
