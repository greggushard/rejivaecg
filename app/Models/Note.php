<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Note
 * @package App\Models
 */
class Note extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['content', 'marked_at'];

    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Polymorphic note relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function noteable()
    {
        return $this->morphTo();
    }

    /**
     * A note belongs to one user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * A note belongs to one note category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function note_category()
    {
        return $this->belongsTo('App\Models\NoteCategory');
    }

}
