<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Interpretation
 * @package App\Models
 */
class Interpretation extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'content'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * An interpretation belongs to one recording session
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recording_session()
    {
        return $this->belongsTo('App\Models\RecordingSession');
    }

    /**
     * An interpreation belongs to one recording
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recording()
    {
        return $this->belongsTo('App\Models\Recording');
    }

    /**
     * An interpretation belongs to one user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
    }

}
