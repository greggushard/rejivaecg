<?php

namespace App\Models;

use Carbon\Carbon;
use App\Scopes\NetworkAdminScope;
use Illuminate\Database\Eloquent\Model;
use App\Scopes\DeviceScope;

/**
 * Class Device
 * @package App\Models
 */
class Device extends Model
{

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable =
                            [
                                'device_id',
                                'radio_id',
                                'enabled'
                            ];


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        if(\Auth::guard('api')->user()) {
            if(\Auth::guard('api')->user()->hasRole('network-admin') && !\Auth::guard('api')->user()->hasRole('admin')) {
                static::addGlobalScope(new DeviceScope);
            }
        }
    }

    /**
     * A device belongs to one network
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function networks()
    {
        return $this->belongsToMany('App\Models\Network')->withTimestamps();
    }

    /**
     * A device has many recording sessions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recording_sessions()
    {
        return $this->belongsToMany('App\Models\RecordingSession')->withTimestamps();
    }

    /**
     * Return the active recording session for a device
     *
     * @return mixed
     */
    public function active_recording_session()
    {
        return $this->belongsToMany('App\Models\RecordingSession')
                    ->where('recording_sessions.date_end', '>=', Carbon::now())
                    ->withTimestamps();
    }

    /**
     * Return the active devices in a network
     *
     * @param $query
     * @return mixed
     */
    public function scopeActiveDevices($query)
    {
        return $query->whereHas('recording_sessions', function($q){
            $q->where('recording_sessions.date_end', '>', Carbon::now());
            $q->with('patient');
        });
    }

    /**
     * A device belongs to one version of firmware
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function firmware()
    {
        return $this->belongsTo('App\Models\Firmware');
    }

}
