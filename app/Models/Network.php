<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Network
 * @package App\Models
 */
class Network extends Model
{

    /**
     * Use the soft deleting trait
     */
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'address',
        'address_2',
        'city',
        'province',
        'postal_code',
        'phone',
        'fax',
        'details'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Scope Networks to a user
     *
     * @param $query
     * @return mixed
     */
    public function scopeToUserRole($query)
    {
        if(!Auth::guard('api')->user()->hasRole('admin')) {
            $query = $query->whereHas('users', function($q){
                $q->where('users.id', Auth::guard('api')->user()->id);
            });
        }

        return $query;
    }

    /**
     * A network may have many practices
     *
     * @return mixed
     */
    public function practices()
    {
        return $this->hasMany('App\Models\Practice');
    }

    /**
     * A network may belong to many users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User')
                    ->wherePivot('deleted_at', null)
                    ->withTimestamps();
    }

    /**
     * A Network has many patients
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function patients()
    {
        return $this->hasMany('App\Models\Patient');
    }

    /**
     * A Network belongs to many devices
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function devices()
    {
        return $this->belongsToMany('App\Models\Device');
    }

    /**
     * Get the active devices in a network
     *
     * @return mixed
     */
    public function activeDevices()
    {
        return $this->belongsToMany('App\Models\Device')->whereHas('recording_session', function($q){
            $q->where('date_end', null);
        });
    }


}
