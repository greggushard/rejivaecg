<?php

namespace App\Models;

use App\Traits\AlertTrait;
use App\Traits\NoteableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Alert
 * @package App\Models
 */
class Alert extends Model
{

    use NoteableTrait, SoftDeletes, AlertTrait;

    /**
     * @var array
     */
    protected $fillable = [
        'alert_type',
        'significant_event',
        'recording_id',
        'event_id',
        'flagged_at',
        'flagged_by',
        'reviewed_at',
        'reviewed_by',
        'patient_id'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'flagged_at',
        'reviewed_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * An alert can be flagged by one user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function flagger()
    {
        return $this->belongsTo('App\Models\User', 'flagged_by', 'id');
    }

    /**
     * An alert can be reviewed by one user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reviewer()
    {
        return $this->belongsTo('App\Models\User', 'reviewed_by', 'id');
    }

    /**
     * An alert has one recording
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function recording()
    {
        return $this->belongsTo('App\Models\Recording');
    }

    /**
     * An alert has one patient
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function patient()
    {
        return $this->belongsTo('App\Models\Patient');
    }

    /**
     * An alert has one event payload
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }
}
