<?php

namespace App\Models;

use App\Http\Requests\StoreUserRequest;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GeniusTS\Roles\Traits\HasRoleAndPermission;
use Illuminate\Database\Eloquent\Builder;
use GeniusTS\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
use App\Scopes\NetworkAdminScope;
/**
 * Class User
 * @package App\Models
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoleAndPermission, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_first', 'name_last', 'username', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        if(\Auth::guard('api')->user()) {
            if(\Auth::guard('api')->user()->hasRole('network-admin') && !\Auth::guard('api')->user()->hasRole('admin')) {
                static::addGlobalScope(new NetworkAdminScope());
            }
        }
    }

    /**
     * Transform the province attribute to a readable province
     * @param $val
     */
    public function getProvinceAttribute($val)
    {
        $provinces = collect(Config::get('available_locations'));
        return $provinces->where('id', $val)->first();
    }

    /**
     * A user may belong to one or many networks
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function networks()
    {
        return $this->belongsToMany('App\Models\Network');
    }

    /**
     * A user may belong to one or many practices
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function practices()
    {
        return $this->belongsToMany('App\Models\Practice');
    }

    /**
     *
     */
    public function patients()
    {
       return $this->belongsToMany('App\Models\Patient');
    }

    /**
     * A user may have many locations indicated by an address
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function locations()
    {
        return $this->hasMany('App\Models\Location');
    }

    /**
     * A user belongs to many notifications
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function notifications()
    {
        return $this->belongsToMany('App\Models\Notification')
            ->withPivot('viewed_at')
            ->withTimestamps()
            ->orderBy('created_at', 'desc');
    }

    /**
     * A user belongs to many new notifications
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function unread_notifications($limit = 10)
    {
        return $this->belongsToMany('\App\Models\Notification')
            ->withTimestamps()
            ->withPivot('viewed_at')
            ->wherePivot('viewed_at', null)
            ->orderBy('created_at', 'desc')
            ->limit($limit);
    }

    /**
     * A user belongs to many read notifications
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function read_notifications($limit = 10)
    {
        return $this->belongsToMany('App\Models\Notification')
            ->withTimestamps()
            ->withPivot('viewed_at')
            ->wherePivot('viewed_at', '!=', null)
            ->orderBy('created_at', 'desc')
            ->limit($limit);
    }

    /**
     * A user can save many recordings for later analysis and review
     *
     * @return mixed
     */
    public function recordings()
    {
        return $this->belongsToMany('App\Models\Recording')
                    ->withTimestamps()
                    ->withPivot('notes');
    }

}