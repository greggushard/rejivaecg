<?php

namespace App\Models;

use Carbon\Carbon;
use App\Scopes\RecordingSessionScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecordingSession extends Model
{

    use SoftDeletes;
    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'date_start', 'date_end', 'is_archived'];

    /**
     * @var array
     */
    protected $fillable = ['session_pin', 'indications', 'date_start', 'date_end', 'assigned_by'];

    /**
     * Apply query scope
     */
   protected static function boot()
   {

        parent::boot();

        if(\Auth::guard('api')->user()) {
            if(!\Auth::guard('api')->user()->hasRole('admin')) {
                static::addGlobalScope(new RecordingSessionScope());
            }
        }
    }

    /**
     * A recording session belongs to one recording
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo('App\Models\Patient');
    }

    /**
     * A recording session belongs to many devices
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function devices()
    {
        return $this->belongsToMany('App\Models\Device')->withTimestamps();
    }

    /**
     * A recording session belongs to one user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assignor()
    {
        return $this->belongsTo('App\Models\User', 'assigned_by', 'id');
    }

    /**
     * A recording session has tons of recordings
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recordings()
    {
        return $this->hasMany('App\Models\Recording');
    }

    /**
     * A recording session has one events count entry
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function events_count()
    {
        return $this->hasOne('App\Models\EcgEventsCount');
    }

    /**
     * Return a count of the recordings
     * @return mixed
     */
    public function recordingsCount()
    {
        return $this->recordings()->count();
    }

    /**
     * The last recording a session has recorded
     * @return mixed
     */
    public function last_recording()
    {
        return $this->hasOne('App\Models\Recording')
                    ->orderBy('id', 'desc');
    }

    /**
     * A recording session has many interpretations
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function interpretations()
    {
        return $this->hasMany('App\Models\Interpretation');
    }

    /**
     * Return recordings in a specific date range
     * @param $date_start
     * @param $date_end
     */
    public function scopeActiveSession($query)
    {
        return $query->where('date_end', '>', Carbon::now());
    }

}
