<?php

namespace App\Models;

use Config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Location
 * @package App
 */
class Location extends Model
{

    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'street',
        'street_2',
        'city',
        'province',
        'postal_code',
        'phone'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * A location belongs to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Transform the province attribute to a readable province
     * @param $val
     */
    public function getProvinceAttribute($val)
    {
        $provinces = collect(Config::get('available_locations'));
        return $provinces->where('id', $val)->first();
    }

}
