<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Remeasurement
 * @package App\Models
 */
class Remeasurement extends Model
{
    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * @var array
     */
    protected $fillable = [
        'measurement_start',
        'measurement_end',
        'measurement_length',
        'measurement_points',
        'measurement_type'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recording()
    {
        return $this->belongsTo('App\Models\Recording');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
