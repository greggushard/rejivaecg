<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Clip
 * @package App
 */
class Clip extends Model
{

    use SoftDeletes;

    /**
     * @var array
     */
    protected $dates = [
        'clipped_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * A clip belongs to a user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clipper()
    {
        return $this->belongsTo('App\Models\User', 'clipped_by', 'id');
    }

    /**
     * A clip belongs to a recording
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recording()
    {
        return $this->belongsTo('App\Models\Recording');
    }

}
