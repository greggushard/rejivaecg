<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Aws\S3\S3Client;

class Image extends BaseModel  {

    use SoftDeletes;

    protected $fillable = ['imageable_type', 'imageable_id', 'base64_data'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    /**
     * Get all of the owning imageable models.
     */
    public function imageable()
    {

        return $this->morphTo();

    }

}
