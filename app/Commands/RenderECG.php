<?php

namespace App\Console\Commands;

# TODO: Better handling for font location
if (\App::environment('local') && !defined('TTF_DIR'))
{
    define('TTF_DIR', storage_path() . '/fonts/truetype/');
}

use Illuminate\Console\Command;
use JpGraph;
use Monolog\Logger;
use Aws\S3\S3Client;
use App\Models\Image;
use Monolog\Handler\StreamHandler;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class RenderECG extends Command implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $log;
    protected $test;

    const ECG_SAMPLE_RATE = 500;
    const AUS_SAMPLE_RATE = 3000;


    /**
     * Create a new command instance.
     */
    public function __construct($test)
    {
        $this->test->id = 5593;

    }


    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $start = time();

        JpGraph\JpGraph::load();
        JpGraph\JpGraph::module('line');


        $this->log = new Logger('name');
        $this->log->pushHandler(new StreamHandler(storage_path().'/logs/RenderECG.log', Logger::INFO));

        $this->log->addInfo('*** START: '.__CLASS__.' ****');
        $this->log->addInfo('ID: ' . $this->test->id);

        $data = $this->test->getNormalizedEcgFileAsArray();

        $this->log->addInfo('getNormalizedEcgFileAsArray: '.count($data).' data points');
        $this->log->addInfo('getNormalizedEcgFileAsArray: '.min($data).' minimum');
        $this->log->addInfo('getNormalizedEcgFileAsArray: '.max($data).' maximum');

        $graphs = [];

        $file = $this->test->id . '-ecg-full.png';
        $path = storage_path() .'/app/ecg_graphs';

//        $graphs[] = ['file_location' => $this->drawEcgGraph($data, $path.'/'.$file), 'type' => 'full', 'name' => $file];


        $sec_per_row = 16;
        $ecg_array_size = count($data);
        $samples_per_row = $sec_per_row * self::ECG_SAMPLE_RATE;
        $num_of_rows = ceil($ecg_array_size/$samples_per_row);
        $msPerRow = $samples_per_row*(1000/self::ECG_SAMPLE_RATE);

//         $ecg_array_size = count($data);
//         $ecg_row_size = 8000;
//         $num_of_rows = ceil($ecg_array_size/$ecg_row_size);

        $this->log->addInfo("ecg_array_size = ".$ecg_array_size);
        $this->log->addInfo("samples_per_row = ".$samples_per_row);
        $this->log->addInfo("num_of_rows    = ".$num_of_rows);
        $this->log->addInfo("msPerRow    = ".$msPerRow);


        for ($x = 0; $x < $num_of_rows; $x++) {

            $array_segment_bgn = $x * $samples_per_row;
            $ecg_nrm_array_segments_array[$x] = array_slice($data, $array_segment_bgn, $samples_per_row);

            $this->log->addInfo('ecg_nrm_array_segments_array: '.count($ecg_nrm_array_segments_array[$x]).' data points');
            $this->log->addInfo("array_slice(".count($data).",".$array_segment_bgn.",".$samples_per_row.")");

            $file_name = $this->test->id . '-ecg-part-'.($x+1).'.png';
            $path = storage_path() .'/app/ecg_graphs';

            $graphs[] = [
                'file_location' => $this->drawEcgGraph($ecg_nrm_array_segments_array[$x],$path.'/'.$file_name,($msPerRow*$x)),
                'file_name' => $file_name
            ];

        }


        foreach($graphs as $file_info) {

            $this->log->addInfo('Saving graphs: ' . $file_info['file_name']);


        }

        $this->log->addInfo('*** END: '.__CLASS__.' ['.(time()-$start).' seconds] ****');
    }


    function drawEcgGraph ($datay, $full_path, $msStart = 0)
    {

        $this->log->addInfo('-- start: ' . __METHOD__ . '-- ');
        $this->log->addInfo('-- type: '  . $full_path);
        $this->log->addInfo("count(datay) = ".count($datay));
        $this->log->addInfo("msStart = ".$msStart);

        $xTextLabelInterval=5;
        $xGraphWidth=(count($datay)/self::ECG_SAMPLE_RATE)*125;

        $xSegmentsPerSecond=5;
        $xSamplesPerSegment=self::ECG_SAMPLE_RATE/$xSegmentsPerSecond;


        $graph = new \Graph($xGraphWidth, 400);
        $graph->SetGridDepth(DEPTH_BACK);
        $graph->img->SetMargin(55,40,0,30);

        $graph->img->SetAntiAliasing();
        $graph->SetScale("linlin",0.3,1.1,0,count($datay));

        $graph->xaxis->SetFont(FF_COURIER,FS_BOLD,16);
        $graph->yaxis->SetFont(FF_COURIER,FS_BOLD,14);
        $graph->xaxis->SetTextLabelInterval($xTextLabelInterval);

        $xTickPositions = array();
        $xTickLabels = array();
        $xTickPositions[0] = 0;
        $xTickLabels[0] = ($msStart/1000)."s";
        for($i=1; $i < (count($datay)/100) ; ++$i ) {
            $xTickPositions[$i] = $i*$xSamplesPerSegment;
            $xTickLabels[$i] = ((($i*200)+$msStart)/1000)."s";
        }

        $this->log->addInfo("TICK POSITIONS:" . json_encode($xTickPositions));
        $this->log->addInfo("TICK LABELS:" . json_encode($xTickLabels));

        $graph->xaxis->SetMajTickPositions($xTickPositions,$xTickLabels);

        $yTickPositions = array(
            0 => 0.3,
            1 => 0.4,
            2 => 0.5,
            3 => 0.6,
            4 => 0.7,
            5 => 0.8,
            6 => 0.9,
            7 => 1.0,
            8 => 1.1,
        );

        $yTickLabels = array(
            0 => "",
            1 => "0.4",
            2 => "0.5",
            3 => "0.6",
            4 => "0.7",
            5 => "0.8",
            6 => "0.9",
            7 => "1.0",
            8 => "",
        );

        $graph->yaxis->SetMajTickPositions($yTickPositions,$yTickLabels);

        $graph->yaxis->scale->ticks->Set(0.1);

        $p1 = new \LinePlot($datay);

        $graph->xscale->SetAutoTicks(false);
        $graph->xgrid->Show(true,true);

        $p1->SetCenter();
        $graph->Add($p1);
        $p1->SetWeight(2);
        $p1->SetColor('#076C16');
        $p1->SetStyle("solid");

        $gdImgHandler = $graph->Stroke(_IMG_HANDLER);

        $graph->img->Stream($full_path);
        $this->log->addInfo('-- save: ' . $full_path);

        $this->log->addInfo('-- end: ' . __METHOD__ . '-- ');

        return $full_path;

    }

}
