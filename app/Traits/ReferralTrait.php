<?php
/**
 * Created by PhpStorm.
 * User: home
 * Date: 12/22/16
 * Time: 1:52 PM
 */

namespace App\Traits;

use App\Scopes\ReferralScope;

trait ReferralTrait
{
    /**
     * Boot the PatientTrait for a model.
     *
     * @return void
     */
    public static function bootReferralTrait()
    {
        static::addGlobalScope(new ReferralScope());
    }

}