<?php namespace App\Traits;

/**
 * Class NoteableTrait
 * @package App\Traits
 */
trait NoteableTrait {

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function notes()
    {
        return $this->morphMany('App\Models\Note', 'noteable');
    }

}