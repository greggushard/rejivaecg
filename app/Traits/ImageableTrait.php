<?php namespace App\Traits;

/**
 * Class ImageableTrait
 * @package App\Traits
 */
trait ImageableTrait {


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

}