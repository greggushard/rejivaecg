<?php namespace App\Traits;

use App\Scopes\AlertScope;

/**
 * Class AlertTrait
 * @package App\Traits
 */
trait AlertTrait {

    /**
     * Boot the PatientTrait for a model.
     *
     * @return void
     */
    public static function bootAlertTrait()
    {
        static::addGlobalScope(new AlertScope);
    }

}