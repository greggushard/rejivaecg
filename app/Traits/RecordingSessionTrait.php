<?php
/**
 * Created by PhpStorm.
 * User: greggushard
 * Date: 2/27/17
 * Time: 9:38 AM
 */

namespace App\Traits;

use App\Scopes\RecordingSessionScope;

trait RecordingSessionTrait
{

    public static function bootRecordingSessionScope()
    {
        static::addGlobalScope(new RecordingSessionScope());
    }

}