<?php namespace App\Traits;

use App\Scopes\PatientScope;

/**
 * Class PatientTrait
 * @package App\Traits
 */
trait PatientTrait {

    /**
     * Boot the PatientTrait for a model.
     *
     * @return void
     */
    public static function bootPatientTrait()
    {
        static::addGlobalScope(new PatientScope);
    }

}