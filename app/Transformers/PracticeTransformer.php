<?php

namespace App\Transformers;

/**
 * Class PracticeTransformer
 * @package App\Transformers
 */
class PracticeTransformer
{

    /**
     * @param $practices
     * @return mixed
     */
    public function transformOutput($practices)
    {

        return
            $practices->transform(function($practice, $key) {
                return [
                    'id' => $practice->id,
                    'name' => $practice->name,
                    'network' => $practice->network,
                    'users' => $practice->users->count(),
                    'created_at' => $practice->created_at->format('d/m/Y h:i:s')
                ];
            });
    }
}