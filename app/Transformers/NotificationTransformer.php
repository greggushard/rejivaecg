<?php

namespace App\Transformers;


class NotificationTransformer
{

    /**
     * Transform a collection of notifications
     * @param $posts
     * @return mixed
     */
    public function transformOutput($notifications)
    {
        return
                $notifications->transform(function($item, $key){

                        switch($item->type){
                            case 'user_register':
                                $icon = 'fa fa-user';
                                break;
                            default:
                                $icon = 'fa fa-alert';
                                break;
                        }

                        return [
                            'id'            =>  $item->id,
                            'icon'          => $icon,
                            'content'       =>  $item->content,
                            'created_at'    =>  $item->created_at->diffForHumans(),
                        ];
                });
    }

}