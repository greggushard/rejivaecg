<?php
namespace App\Transformers;

use App\Jobs\RenderReportGraph;
use Carbon\Carbon;
use App\Models\Recording;
use App\Jobs\RenderMoneboGraph;
use Illuminate\Support\Facades\Storage;

class PatientTransformer
{
    /**
     * Transform a collection of patients
     * @param $patients
     * @return mixed
     */
    public function transformOutput($patients)
    {
        return
            $patients->transform(function($item, $key) {
                if(count($item->recording_sessions) == 0) {
                    $active_recording_session = null;
                    $last_body_position = "Not recorded";
                    $last_data_submitted = "No recordings";
                } elseif(count($item->recording_sessions->last()->recordings) == 0) {
                    $active_recording_session = null;
                    $last_body_position = "Not recorded";
                    $last_data_submitted = "No recordings";
                }
                else {
                    $active_recording_session = $item->recording_sessions->last()->id;
                    $last_body_position = $item->recording_sessions->last()->recordings->last()->body_position;
                    $last_data_submitted = $item->recording_sessions->last()->recordings->last()->created_at->setTimezone('America/New_York')->format('m/d/Y h:i:s a');
                }

                return [
                    'id'                                =>  $item->id,
                    'name_first'                        =>  $item->name_first,
                    'name_last'                         =>  $item->name_last,
                    'active_recording_session'          =>  $active_recording_session,
                    'last_body_position'                =>  $last_body_position,
                    'last_data_submitted'               =>  $last_data_submitted
                ];
            });
    }

    /**
     * Transform patient data for filling out a form
     * @param $patient
     * @return array
     */
    public function transformPatientForForm($patient)
    {
        return [
            'id'                    => $patient->id,
            'name_first'            => ($patient->name_first != null ? $patient->name_first : ''),
            'name_last'             => ($patient->name_last != null ? $patient->name_last : ''),
            'name_middle'           => ($patient->name_middle != null ? $patient->name_middle : ''),
            'name_title'            => ($patient->name_title != null ? $patient->name_title : ''),
            'dob'                   => ($patient->dob != null ? $patient->dob : ''),
            'gender'                => ($patient->gender != null ? $patient->gender : ''),
            'health_card_number'    => ($patient->health_card_number != null ? $patient->health_card_number : ''),
            'health_card_expiration'=> ($patient->health_card_expiration != null ? $patient->health_card_expiration : ''),
            'address_street1'       => ($patient->address_street1 != null ? $patient->address_street1 : ''),
            'address_city'          => ($patient->address_city != null ? $patient->address_city : ''),
            'address_province'      => ($patient->address_province != null ? $patient->address_province : ''),
            'address_postal_code'   => ($patient->address_postal_code != null ? $patient->address_postal_code : ''),
            'address_country'       => ($patient->address_country != null ? $patient->address_country : ''),
            'phone1'                => ($patient->phone1 != null ? $patient->phone1 : ''),
            'phone1_type'           => ($patient->phone1_type != null ? $patient->phone1_type : ''),
            'phone2'                => ($patient->phone2 != null ? $patient->phone2 : ''),
            'phone2_type'           => ($patient->phone2_type != null ? $patient->phone2_type : ''),
            'email'                 => ($patient->email != null ? $patient->email : ''),
            'height'                => ($patient->height != null ? $patient->height : ''),
            'weight'                => ($patient->weight != null ? $patient->weight : ''),
            'patient_history'       => ($patient->patient_history != null ? $patient->patient_history : ''),
            'network_id'            => ($patient->network_id != null ? $patient->network_id : ''),
        ];
    }

    /**
     * Transform a single patient
     * @param $patient
     * @return array
     */
    public function transformPatientData($patient)
    {
        $today = Carbon::now();
        if($patient->health_card_expiration != null ) {
            if($today > $patient->health_card_expiration) {
                $health_card_expiration = '<span class="label label-danger">Expired ' . $patient->health_card_expiration->format('m/d/y') . '</span>';
            } else {
                $health_card_expiration = '<span class="label label-default">Expires ' . $patient->health_card_expiration->format('m/d/y') . '</span>';
            }
        } else {
            $health_card_expiration = null;
        }

        if(count($patient->recordings) > 0) {
            if(Storage::disk('local')->exists('ecg_graphs_png/'.$patient->recordings->last()->recording_hash .'.png')) {
                $last_graph = Storage::disk('local')->get('ecg_graphs_png/'.$patient->recordings->last()->recording_hash . '.png');
            } else {
                $last_graph = Storage::disk('local')->get('placeholders/data-missing.png');
            }
        } else {
            $last_graph = null;
        }

        return [
            'name'                   => $patient->name_first . ' ' . $patient->name_middle . ' ' . $patient->name_last,
            'dob'                    => ($patient->dob != null ? $patient->dob->format('m/d/y') : ''),
            'age'                    => ($patient->dob != null ? $patient->dob->diffInYears($today) : ''),
            'created_at'             => $patient->created_at->format('m/d/y'),
            'weight'                 => ($patient->weight != null ? $patient->weight . ' kg.' : '<i>Not available</i>'),
            'height'                 => ($patient->height != null ? $patient->height . ' cm.' : '<i>Not available</i>'),
            'patient_history'        => ($patient->patient_history != null ? $patient->patient_history : '<i>Not available</i>'),
            'health_card_number'     => ($patient->health_card_number != null ? $patient->health_card_number : '<i>Not available</i>'),
            'health_card_expiration' => ($health_card_expiration != null ? $health_card_expiration : '<i>Not avilable</i>'),
            'address'                => $patient->address_street1 . '<br />' . $patient->address_province . ' ' . $patient->address_postal_code . ' ' . $patient->address_country,
            'phone_primary'          => ($patient->phone1 != null ? $patient->phone1 : ''),
            'phone_secondary'        => ($patient->phone2 != null ? $patient->phone2 : ''),
            'sessions'               => $patient->recording_sessions->transform(function($session) use($today, $last_graph) {
                if($session->date_end > $today) {
                    return [
                        'session_id'         => $session->id,
                        'is_active'          => true,
                        'date_start'         => $session->date_start->format('m/d/y'),
                        'date_end'           => $session->date_end->format('m/d/y'),
                        'num_of_days'        => $session->date_end->diff($session->date_start),
                        'assigned_by'        => $session->assignor->name,
                        'recordings_counted' => $session->recordingsCount(),
                        'last_recording'     => [
                            'created_at'     => $session->recordings->last()->created_at->diffForHumans(),
                            'small_graph'    => 'data:image/png;charset=utf-8;base64,'.base64_encode($last_graph),
                        ],
                        'devices'            => $session->devices->transform(function($device){
                            return [
                                'radio_id' => $device->radio_id
                            ];
                        })
                    ];
                }
                return [
                    'session_id'         => $session->id,
                    'is_active'          => false,
                    'date_start'         => $session->date_start->format('m/d/y'),
                    'date_end'           => $session->date_end->format('m/d/y'),
                    'assigned_by'        => $session->assignor->name,
                    'recordings_counted' => $session->recordingsCount(),
                    'last_recording'     => $session->recordings->last(),
                    'devices'            => $session->devices->transform(function($device){
                        return [
                            'radio_id' => $device->radio_id
                        ];
                    }),
                ];
            })
        ];
    }
}