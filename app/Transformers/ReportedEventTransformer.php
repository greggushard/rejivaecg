<?php

namespace App\Transformers;

use Config;
/**
 * Class ReportedEventTransformer
 * @package App\Transformers
 */
class ReportedEventTransformer
{
    /**
     * @param $reportedEvents
     * @return mixed
     */
    public function transformOutput($reportedEvents, $recording)
    {
        return
            $reportedEvents->transform(function($event, $key) use($recording) {
                return [
                    'id' => $event->id,
                    'reported' => Config::get('monebo_ecg.patient_events.' . $event->event_id .'.description'),
                    'x' => $this->convertToMs($event, $recording),
                    'y' => -0.6,
                    'height' => 18,
                    'width' => 18,
                    'marker' => [
                        'symbol' => 'url(http://dev.rejiva.com/img/'.Config::get('monebo_ecg.patient_events.' . $event->event_id .'.icon').'.png)'
                    ]
                ];
            });
    }

    /**
     * Convert the Patient Reported Event X Axis value
     * @param $event
     */
    protected function convertToMs($event, $recording)
    {
        $start_time = strtotime($recording->timestamp);
        $reported_at = strtotime($event->timestamp);
        $xPoint = ((int)$reported_at - (int)$start_time)*1000;
        return $xPoint;
    }
}