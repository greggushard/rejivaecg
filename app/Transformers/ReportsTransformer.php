<?php

namespace App\Transformers;

use Config;
use Carbon\Carbon;
use App\Models\Location;
use App\Models\RecordingSession;
use Illuminate\Support\Facades\Storage;

/**
 * Class ReportsTransformer
 * @package App\Transformers
 */
class ReportsTransformer
{

    /**
     * Transform the data for a transmission report
     * @param RecordingSession $recordingSession
     * @return array
     */
    public function transformForTransmissionReport(RecordingSession $recordingSession)
    {
        $sessionMeasurements = $this->presentTransmissionReport($recordingSession);
        // Transform the "strips"
        $strips = $recordingSession->recordings->transform(function($recording){
            $decodedEvents = json_decode($recording->events->event_payload);
            $hrAverMeasurements = [];
            $prAverMeasurements = [];
            $qrsAverMeasurements = [];
            $qtAverMeasurements = [];
            if(count($decodedEvents) > 0) {
                foreach($decodedEvents->events_filtered_json_data as $event) {
                    if(isset($event->MEASUREMENTS)) {
                        array_push($hrAverMeasurements, $event->MEASUREMENTS->HR_MAX);
                        array_push($hrAverMeasurements, $event->MEASUREMENTS->HR_MIN);
                        array_push($prAverMeasurements, $event->MEASUREMENTS->PR_AVER);
                        array_push($qrsAverMeasurements, $event->MEASUREMENTS->QRS_AVER);
                        array_push($qtAverMeasurements, $event->MEASUREMENTS->QT_AVER);
                    }
                }
            }
            if(count($recording->remeasurements) > 0) {
                foreach($recording->remeasurements as $remeasurement) {
                    if($remeasurement->type == 'pr') {
                        array_push($prAverMeasurements, $remeasurement->measurement_length*1000);
                    } elseif($remeasurement->type == 'qrs') {
                        array_push($qrsAverMeasurements, $remeasurement->measurement_length);
                    } elseif($remeasurement->type == 'qt') {
                        array_push($qtAverMeasurements, $remeasurement->measurement_length);
                    } elseif($remeasurement->type == 'rr') {
                        array_push($hrAverMeasurements, round(60/$remeasurement->measurement_length*1000));
                    }
                }
            }

            return [
                'recording_id'  => $recording->id,
                'clipped_at'    => $recording->clip->clipped_at->format('m/d/y h:i:s a'),
                'clipped_by'    => $recording->clip->clipper->name_first . ' ' . $recording->clip->clipper->name_last,
                'battery_level' => $recording->battery_level,
                'timestamp'     => $recording->timestamp->toDateTimeString(),
                'heart_rate'    => $recording->heart_rate,
                'recording_hash'=> $recording->recording_hash,
                'body_position' => $recording->body_position,
                'events'        => $decodedEvents->events_filtered_json_data,
                'reported_events' => $this->getReportedEventsForRecording($recording),
                'measurements'  => [
                                'rr' => [
                                    'max' => max($hrAverMeasurements),
                                    'min' => min($hrAverMeasurements),
                                ],
                                'pr' => [
                                    'max' => max($prAverMeasurements),
                                    'min' => min($prAverMeasurements),
                                ],
                                'qrs'=> [
                                    'max' => max($qrsAverMeasurements),
                                    'min' => min($qrsAverMeasurements)
                                ],
                                'qt'=> [
                                    'max' => max($qtAverMeasurements),
                                    'min' => min($qtAverMeasurements)
                                ]
                ],
                'notes'         => [
                    'recording_notes' => $recording->notes->transform(function ($note, $key) {
                        $role = $note->user->roles->first();
                        if($role->slug == 'admin') {
                            $role_text = '<span class="label label-primary">Administrator</span>';
                        } elseif($role->slug == 'physician') {
                            $role_text = '<span class="label label-default">Physician</span>';
                        } elseif($role->slug == 'network-admin') {
                            $role_text = '<span class="label label-success">Network Administrator</span>';
                        } elseif($role->slug == 'interpreting-cardiologist') {
                            $role_text = '<span class="label label-danger">Interpreting Cardiologist</span>';
                        } elseif($role->slug == 'cardiac-tech') {
                            $role_text = '<span class="label label-warning">Cardiac Technician</span>';
                        } else {
                            $role_text = 'No Role Selected';
                        }
                        return [
                            'id'            => $note->id,
                            'created_at'    => $note->created_at->format('m/d/y h:i:s a'),
                            'user'          => $note->user->name,
                            'user_badge'    => $role_text,
                            'category'      => $note->note_category->description,
                            'content'       => $note->content,
                        ];
                    })
                ],
                'recording_img' => base64_encode(Storage::get('ecg_graphs_reports/' . $recording->recording_hash . '.png')),
                'remeasurements'=> $recording->remeasurements
            ];
        });
        // Return the cover sheet data and the strips for the transmission report
        return [
            'cover_sheet_data' => $this->generateCoverSheetData($recordingSession),
            'present_transmission' => $sessionMeasurements,
            'strips' => $strips
        ];

    }

    /**
     * Transform the data for a recording session report
     * @param RecordingSession $recordingSession
     * @return array
     */
    public function transformForDisclosureReport(RecordingSession $recordingSession)
    {
        $missed_recordings_cap = 120;

        $strips = $recordingSession->recordings->transform(function($recording){
            return [
                'recordings_missed' => $recording->recordings_missed,
                'recording_hash'    => $recording->recording_hash,
                'timestamp'         => $recording->timestamp->format('m/d/y h:i:s a'),
                'rawTimeStamp'      => $recording->timestamp
            ];
        });
        $graphs = [];
        $missed = 0;
        foreach($strips as $strip) {
            if(Storage::disk('local')->exists('ecg_graphs_small/'.$strip['recording_hash'].'_part0.png')){
                array_push($graphs, [
                    'timestamp'               => $strip['timestamp'],
                    'base64'                  => "data:image/png;charset=utf-8;base64,".base64_encode(Storage::disk('local')->get('ecg_graphs_small/'.$strip['recording_hash'].'_part0.png')),
                    'recordings_missed_after' => $strip['recordings_missed']
                ]);
            }
            else {
                array_push($graphs, [
                    'timestamp'               => $strip['timestamp'],
                    'base64'                  => "data:image/png;charset=utf-8;base64,".base64_encode(Storage::disk('local')->get('placeholders/data-missing.png')),
                    'recordings_missed_after' => $strip['recordings_missed']
                ]);
            }
            $missed+=$strip['recordings_missed'];
            if($strip['recordings_missed']>0){
                for($i =1;$i<=min($strip['recordings_missed'], $missed_recordings_cap);$i++){
                    $time = $strip['rawTimeStamp']->addSeconds(8*$i);
                    $warning =  $strip['recordings_missed'];
                    if($i==$missed_recordings_cap) {
                        $warning = 'There are still a further '.($strip['recordings_missed']-100) . ' missing strips';
                    }
                    array_push($graphs, [
                        'timestamp'               => $time->format('m/d/y h:i:s a'),
                        'base64'                  => "data:image/png;charset=utf-8;base64,".base64_encode(Storage::disk('local')->get('placeholders/data-missing.png')),
                        'recordings_missed_after' => $warning
                    ]);
                }
            }
        }

        return [
            'cover_sheet_data' => $this->generateCoverSheetData($recordingSession),
            'strips' => $graphs
        ];
    }

    /**
     * Transform for Holter Summary Report
     * @param $recordingSession
     * @return array
     */
    public function transformForHolterReport($recordingSession)
    {
        $overallMeasurements = $this->presentTransmissionReport($recordingSession);

        $rapidHrMeasurements = $this->minMaxRr($recordingSession->recordings->where('alerts.significant_event', 'Rapid AFib'));
        $thirdDegreeHrMeasurements = $this->minMaxRr($recordingSession->recordings->where('alerts.significant_event', 'Third Degree Block'));
        $vTachMeasurements = $this->minMaxRr($recordingSession->recordings->where('alerts.significant_event', 'VTach'));
        $slowHrMeasurements = $this->minMaxRr($recordingSession->recordings->where('alerts.significant_event', 'Slow AFib'));
        $svtaHrMeasurements = $this->minMaxRr($recordingSession->recordings->where('alerts.significant_event', 'SVTA'));

        $rapidPrMinMax = $this->getMinMax($recordingSession->recordings->where('alerts.significant_event', 'Rapid AFib'), 'pr');
        $rapidQrsMinMax = $this->getMinMax($recordingSession->recordings->where('alerts.significant_event', 'Rapid AFib'), 'qrs');
        $rapidQtMinMax = $this->getMinMax($recordingSession->recordings->where('alerts.significant_event', 'Rapid AFib'), 'qt');

        $thirdDegreePrMinMax = $this->getMinMax($recordingSession->recordings->where('alerts.significant_event', 'Third Degree Block'), 'pr');
        $thirdDegreeQrsMinMax = $this->getMinMax($recordingSession->recordings->where('alerts.significant_event', 'Third Degree Block'), 'qrs');
        $thirdDegreeQtMinMax = $this->getMinMax($recordingSession->recordings->where('alerts.significant_event', 'Third Degree Block'), 'qt');

        $vTachPrMinMax = $this->getMinMax($recordingSession->recordings->where('alerts.significant_event', 'VTach'), 'pr');
        $vTachQrsMinMax = $this->getMinMax($recordingSession->recordings->where('alerts.significant_event', 'VTach'), 'qrs');
        $vTachQtMinMax = $this->getMinMax($recordingSession->recordings->where('alerts.significant_event', 'VTach'), 'qt');

        $slowPrMinMax = $this->getMinMax($recordingSession->recordings->where('alerts.significant_event', 'Slow AFib'), 'pr');
        $slowQrsMinMax = $this->getMinMax($recordingSession->recordings->where('alerts.significant_event', 'Slow AFib'), 'qrs');
        $slowQtMinMax = $this->getMinMax($recordingSession->recordings->where('alerts.significant_event', 'Slow AFib'), 'qt');

        $svtaPrMinMax = $this->getMinMax($recordingSession->recordings->where('alerts.significant_event', 'SVTA'), 'pr');
        $svtaQrsMinMax = $this->getMinMax($recordingSession->recordings->where('alerts.significant_event', 'SVTA'), 'qrs');
        $svtaQtMinMax = $this->getMinMax($recordingSession->recordings->where('alerts.significant_event', 'SVTA'), 'qt');

        $rapidAfib = $recordingSession->recordings->where('alerts.significant_event', 'Rapid AFib')->transform(function ($item) {
            $measurements = $this->chunkRemeasurements($item);
            return [
                'img' => base64_encode(Storage::get('ecg_graphs_reports/' . $item->recording_hash . '.png')),
                'pr' => $measurements['pr'] . ' ms',
                'qrs' => $measurements['qrs'] . ' ms',
                'qt' => $measurements['qt'] . ' ms',
                'rr' => $measurements['rr'] . ' bpm',
                'br' => $item->breathing_rate,
            ];
        })->toArray();

        $thirdDegreeBlock = $recordingSession->recordings->where('alerts.significant_event', 'Third Degree Block')->transform(function ($item) {
            $measurements = $this->chunkRemeasurements($item);
            return [
                'img' => base64_encode(Storage::get('ecg_graphs_reports/' . $item->recording_hash . '.png')),
                'pr' => $measurements['pr'] . ' ms',
                'qrs' => $measurements['qrs'] . ' ms',
                'qt' => $measurements['qt'] . ' ms',
                'rr' => $measurements['rr'] . ' bpm',
                'br' => $item->breathing_rate,
            ];
        })->toArray();

        $vTach = $recordingSession->recordings->where('alerts.significant_event', 'VTach')->transform(function ($item) {
            $measurements = $this->chunkRemeasurements($item);
            return [
                'img' => base64_encode(Storage::get('ecg_graphs_reports/' . $item->recording_hash . '.png')),
                'pr' => $measurements['pr'] . ' ms',
                'qrs' => $measurements['qrs'] . ' ms',
                'qt' => $measurements['qt'] . ' ms',
                'rr' => $measurements['rr'] . ' bpm',
                'br' => $item->breathing_rate,
            ];
        })->toArray();

        $slowAfib = $recordingSession->recordings->where('alerts.significant_event', 'Slow AFib')->transform(function ($item) {
            $measurements = $this->chunkRemeasurements($item);
            return [
                'img' => base64_encode(Storage::get('ecg_graphs_reports/' . $item->recording_hash . '.png')),
                'pr' => $measurements['pr'] . ' ms',
                'qrs' => $measurements['qrs'] . ' ms',
                'qt' => $measurements['qt'] . ' ms',
                'rr' => $measurements['rr'] . ' bpm',
                'br' => $item->breathing_rate,
            ];
        })->toArray();

        $svta = $recordingSession->recordings->where('alerts.significant_event', 'SVTA')->transform(function ($item) {
            $measurements = $this->chunkRemeasurements($item);
            return [
                'img' => base64_encode(Storage::get('ecg_graphs_reports/' . $item->recording_hash . '.png')),
                'pr' => $measurements['pr'] . ' ms',
                'qrs' => $measurements['qrs'] . ' ms',
                'qt' => $measurements['qt'] . ' ms',
                'rr' => $measurements['rr'] . ' bpm',
                'br' => $item->breathing_rate,
            ];
        })->toArray();

        $rapidAfibStrips = [];
        $thirdDegreeBlockStrips = [];
        $vTachStrips = [];
        $slowAfibStrips = [];
        $svtaStrips = [];

        foreach ($rapidAfib as $strip) {
            array_push($rapidAfibStrips, $strip);
        }
        foreach ($thirdDegreeBlock as $strip) {
            array_push($thirdDegreeBlockStrips, $strip);
        }
        foreach ($vTach as $strip) {
            array_push($vTachStrips, $strip);
        }
        foreach ($slowAfib as $strip) {
            array_push($slowAfibStrips, $strip);
        }
        foreach ($svta as $strip) {
            array_push($svtaStrips, $strip);
        }

        return [
            'cover_sheet_data' => $this->generateCoverSheetData($recordingSession),
            'holter_summary' => [
                    'patient_event_totals' => [
                        'chest_pain'            => $recordingSession->events_count->chest_pain,
                        'shortness_of_breath'   => $recordingSession->events_count->shortness_of_breath,
                        'fatigue'               => $recordingSession->events_count->fatigue,
                        'heart_palpitation'     => $recordingSession->events_count->heart_palpitation
                    ],
                    'signficiant_event_totals' => [
                        'rapid_atrial_fibrillation' => $recordingSession->recordings->where('alerts.significant_event', 'Rapid AFib')->count(),
                        'third_degree_block'        => $recordingSession->recordings->where('alerts.significant_event', 'Third Degree Block')->count(),
                        'ventricular_tachycardia'   => $recordingSession->recordings->where('alerts.significant_event', 'VTach')->count(),
                        'atrial_fibrillation'       => $recordingSession->recordings->where('alerts.significant_event', 'Slow AFib')->count(),
                        'svta'                      => $recordingSession->recordings->where('alerts.significant_event', 'SVTA')->count()
                    ],
                    'hr_variability' => [
                        'min' => $overallMeasurements['rr']['min'] . ' bpm',
                        'max' => $overallMeasurements['rr']['max'] . ' bpm',
                    ],
                    'significant_event_summary' => [
                            'rapid_atrial_fibrillation' => [
                                'total'             => $recordingSession->recordings->where('alerts.significant_event', 'Rapid AFib')->count(),
                                'longest'           => 0,
                                'min_hr'            => $rapidHrMeasurements['min'] . ' bpm',
                                'max_hr'            => $rapidHrMeasurements['max'] . ' bpm',
                                'max_pr'            => $rapidPrMinMax['max'] . ' ms',
                                'min_pr'            => $rapidPrMinMax['min'] . ' ms',
                                'max_qrs'           => $rapidQrsMinMax['max'] . ' ms',
                                'min_qrs'           => $rapidQrsMinMax['min'] . ' ms',
                                'max_qt'            => $rapidQtMinMax['max'] . ' ms',
                                'min_qt'            => $rapidQtMinMax['min'] . ' ms',
                                'additional_data'   => [
                                    'strips' => $rapidAfibStrips
                                ]
                            ],
                            'third_degree_block' => [
                                'total'             => $recordingSession->recordings->where('alerts.significant_event', 'Third Degree Block')->count(),
                                'longest'           => 0,
                                'min_hr'            => $thirdDegreeHrMeasurements['min'] . ' bpm',
                                'max_hr'            => $thirdDegreeHrMeasurements['max'] . ' bpm',
                                'max_pr'            => $thirdDegreePrMinMax['max'] . ' ms',
                                'min_pr'            => $thirdDegreePrMinMax['min'] . ' ms',
                                'max_qrs'           => $thirdDegreeQrsMinMax['max'] . ' ms',
                                'min_qrs'           => $thirdDegreeQrsMinMax['min'] . ' ms',
                                'max_qt'            => $thirdDegreeQtMinMax['max'] . ' ms',
                                'min_qt'            => $thirdDegreeQtMinMax['min'] . ' ms',
                                'additional_data'   => [
                                    'strips' => $thirdDegreeBlockStrips
                                ]
                            ],
                            'ventricular_tachycardia' => [
                                'total'             => $recordingSession->recordings->where('alerts.significant_event', 'VTach')->count(),
                                'longest'           => 0,
                                'min_hr'            => $vTachMeasurements['min'] . ' bpm',
                                'max_hr'            => $vTachMeasurements['max'] . ' bpm',
                                'max_pr'            => $vTachPrMinMax['max'] . ' ms',
                                'min_pr'            => $vTachPrMinMax['min'] . ' ms',
                                'max_qrs'           => $vTachQrsMinMax['max'] . ' ms',
                                'min_qrs'           => $vTachQrsMinMax['min'] . ' ms',
                                'max_qt'            => $vTachQtMinMax['max'] . ' ms',
                                'min_qt'            => $vTachQtMinMax['min'] . ' ms',
                                'additional_data'   => [
                                    'strips' => $vTachStrips
                                ]
                        ],
                        'atrial_fibrillation' => [
                            'total'             => $recordingSession->recordings->where('alerts.significant_event', 'Slow AFib')->count(),
                            'longest'           => 0,
                            'min_hr'            => $slowHrMeasurements['min'] . ' bpm',
                            'max_hr'            => $slowHrMeasurements['max'] . ' bpm',
                            'max_pr'            => $slowPrMinMax['max'] . ' ms',
                            'min_pr'            => $slowPrMinMax['min'] . ' ms',
                            'max_qrs'           => $slowQrsMinMax['max'] . ' ms',
                            'min_qrs'           => $slowQrsMinMax['min'] . ' ms',
                            'max_qt'            => $slowQtMinMax['max'] . ' ms',
                            'min_qt'            => $slowQtMinMax['min'] . ' ms',
                            'additional_data'   => [
                                'strips' => $slowAfibStrips
                            ]
                        ],
                        'svta' => [
                            'total'             => $recordingSession->recordings->where('alerts.significant_event', 'SVTA')->count(),
                            'longest'           => 0,
                            'min_hr'            => $svtaHrMeasurements['min'] . ' bpm',
                            'max_hr'            => $svtaHrMeasurements['max'] . ' bpm',
                            'max_pr'            => $svtaPrMinMax['max'] . ' ms',
                            'min_pr'            => $svtaPrMinMax['min'] . ' ms',
                            'max_qrs'           => $svtaQrsMinMax['max'] . ' ms',
                            'min_qrs'           => $svtaQrsMinMax['min'] . ' ms',
                            'max_qt'            => $svtaQtMinMax['max'] . ' ms',
                            'min_qt'            => $svtaQtMinMax['min'] . ' ms',
                            'additional_data'   => [
                                'strips' => $svtaStrips
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Generate the cover sheet data for a report
     * @param $recordingSession
     * @return array
     */
    protected function generateCoverSheetData($recordingSession)
    {
        $referrer_data =    $recordingSession->patient
                                         ->users()
                                         ->wherePivot('user_type', 'referring-physician')
                                         ->withPivot('location_id')
                                         ->get()
                                         ->first();
        $referring_physician_location = Location::find($referrer_data->pivot->location_id);
        $referring_physician = $referrer_data;

        $interpreter_data = $recordingSession->patient
                                             ->users()
                                             ->wherePivot('user_type', 'interpreting-cardiologist')
                                             ->withPivot('location_id')
                                             ->get()
                                             ->first();
        $interpreting_physician_location = Location::find($interpreter_data->pivot->location_id);
        $interpeting_physician = $interpreter_data;

        $copy_to_data = $recordingSession->patient
                                         ->users()
                                         ->wherePivot('user_type', 'copy-to')
                                         ->withPivot('location_id')
                                         ->get()
                                         ->first();
        $copy_to_location = Location::find($copy_to_data->pivot->location_id);
        $copy_to_physician = $copy_to_data;

        if(is_null($referring_physician)) {
            $referring_physician = new \StdClass;
            $referring_physician->name_first = 'Unassigned';
            $referring_physician->name_last = 'Unassigned';
            $referring_physician->email = 'Unassigned';
            $referring_physician->phone = 'Unassigned';
        }

        if(is_null($interpeting_physician)) {
            $interpeting_physician = new \StdClass;
            $interpeting_physician->name_first = 'Unassigned';
            $interpeting_physician->name_last = 'Unassigned';
            $interpeting_physician->email = 'Unassigned';
            $interpeting_physician->phone = 'Unassigned';
        }

        if(is_null($copy_to_physician)) {
            $copy_to_physician = new \StdClass;
            $copy_to_physician->name_first = 'Unassigned';
            $copy_to_physician->name_last = 'Unassigned';
            $copy_to_physician->email = 'Unassigned';
            $copy_to_physician->phone = 'Unassigned';
        }

        if(is_null($referring_physician_location)) {
            $referring_physician_location = new \StdClass;
            $referring_physician_location->street = 'Unassigned';
            $referring_physician_location->street_2 = 'Unassigned';
            $referring_physician_location->city = 'Unassigned';
            $referring_physician_location->province->name = 'Unassigned';
            $referring_physician_location->postal_code = 'Unassigned';
            $referring_physician_location->phone = 'Unassigned';
            $referring_physician_location->fax = 'Unassigned';
        }

        if(is_null($interpreting_physician_location)) {
            $interpreting_physician_location = new \StdClass;
            $interpreting_physician_location->street = 'Unassigned';
            $interpreting_physician_location->street_2 = 'Unassigned';
            $interpreting_physician_location->city = 'Unassigned';
            $interpreting_physician_location->province->name = 'Unassigned';
            $interpreting_physician_location->postal_code = 'Unassigned';
            $interpreting_physician_location->phone = 'Unassigned';
            $interpreting_physician_location->fax = 'Unassigned';
        }

        if(is_null($copy_to_location)) {
            $copy_to_location = new \StdClass;
            $copy_to_location->street = 'Unassigned';
            $copy_to_location->street_2 = 'Unassigned';
            $copy_to_location->city = 'Unassigned';
            $copy_to_location->province->name = 'Unassigned';
            $copy_to_location->postal_code = 'Unassigned';
            $copy_to_location->phone = 'Unassigned';
            $copy_to_location->fax = 'Unassigned';
        }

        if($recordingSession->date_end > Carbon::now()) {
            $report_type = 'Preliminary';
        } else {
            $report_type = 'Final';
        }

        return [
            'generated' => Carbon::now()->setTimezone('America/New_York')->format('m/d/y \a\t h:i a'),
            'report_type' => $report_type,
            'patient'   =>
                [
                    'id'                 => $recordingSession->patient->id,
                    'session_id'         => $recordingSession->id,
                    'name'               => $recordingSession->patient->name_first . ' ' . $recordingSession->patient->name_middle . ' ' .$recordingSession->patient->name_last,
                    'dob'                => $recordingSession->patient->dob->format('m/d/y'),
                    'health_card_number' => $recordingSession->patient->health_card_number,
                    'age'                => $recordingSession->patient->dob->diffInYears(Carbon::now()),
                    'gender'             => $recordingSession->patient->gender,
                    'duration'           => $recordingSession->patient->created_at->diffInDays(Carbon::now()) . ' days'
                ],
            'network'   =>
                [
                    'name'          => $recordingSession->patient->network->name,
                    'address'       => $recordingSession->patient->network->address . ' ' . $recordingSession->patient->network->address_2,
                    'city'          => $recordingSession->patient->network->city,
                    'province'      => $recordingSession->patient->network->province,
                    'postal_code'   => $recordingSession->patient->network->postal_code,
                    'country'       => $recordingSession->patient->network->country,
                    'phone'         => $recordingSession->patient->network->phone,
                    'email'         => $recordingSession->patient->network->users->first()->email,
                    'fax'           => $recordingSession->patient->network->fax,
                ],
            'referring_physician' =>
                [
                    'name'          => $referring_physician->name_first . ' ' . $referring_physician->name_last,
                    'street_address'=> $referring_physician_location->street . ' ' . $referring_physician_location->street_2,
                    'city'          => $referring_physician_location->city,
                    'province'      => $referring_physician_location->province['name'],
                    'postal_code'   => $referring_physician_location->postal_code,
                    'email'         => $referring_physician->email,
                    'phone'         => $referring_physician_location->phone,
                    'fax'           => $referring_physician_location->fax,
                ],
            'interpreting_physician' =>
                [
                    'name'          => $interpeting_physician->name_first . ' ' . $interpeting_physician->name_last,
                    'street_address'=> $interpreting_physician_location->street . ' ' . $interpreting_physician_location->street_2,
                    'city'          => $interpreting_physician_location->city,
                    'province'      => $interpreting_physician_location->province['name'],
                    'postal_code'   => $interpreting_physician_location->postal_code,
                    'email'         => $interpeting_physician->email,
                    'phone'         => $interpreting_physician_location->phone,
                    'fax'           => $interpreting_physician_location->fax,
                ],
            'copy_to_physician' =>
                [
                    'name'          => $copy_to_physician->name_first . ' ' . $copy_to_physician->name_last,
                    'street_address'=> $copy_to_location->street . ' ' . $copy_to_location->street_2,
                    'city'          => $copy_to_location->city,
                    'province'      => $copy_to_location->province['name'],
                    'postal_code'   => $copy_to_location->postal_code,
                    'email'         => $copy_to_physician->email,
                    'phone'         => $copy_to_location->phone,
                    'fax'           => $copy_to_location->fax,
                ],
            'monitoring_data' =>
                [
                    'start_date'    => $recordingSession->date_start->format('m/d/y'),
                    'end_date'      => $recordingSession->date_end->format('m/d/y'),
                    'return_date'   => $recordingSession->date_end->format('m/d/y'),
                    'devices'       => $recordingSession->devices->transform(function($device){
                        return [
                            'radio_id' => $device->radio_id
                        ];
                    }),
                    'indications'       => '',
                    'interpretations'   => $recordingSession->interpretations
                ],
        ];
    }

    /**
     * Get the min and max RR from an individual recording
     * @param $recordings
     * @return array
     */
    protected function minMaxRr($recordings)
    {
        $hrAverMeasurements = [];
        foreach($recordings as $recording) {
            $decodedEvents = json_decode($recording->events->event_payload);
            if(count($decodedEvents) > 0) {
                foreach($decodedEvents->events_filtered_json_data as $event) {
                    if(isset($event->MEASUREMENTS)) {
                        array_push($hrAverMeasurements, $event->MEASUREMENTS->HR_MAX);
                        array_push($hrAverMeasurements, $event->MEASUREMENTS->HR_MIN);
                    }
                }
            }
            if(count($recording->remeasurements) > 0) {
                foreach($recording->remeasurements as $remeasurement) {
                    if($remeasurement->type == 'rr') {
                        array_push($hrAverMeasurements, round(60/$remeasurement->measurement_length*1000));
                    }
                }
            }
        }

        if(empty($hrAverMeasurements)) {
            $hrAverMeasurements = [
                0 => 'N/A'
            ];
        }

        return [
                    'min' => min($hrAverMeasurements),
                    'max' => max($hrAverMeasurements)
        ];
    }

    /**
     * @param $recordings
     * @param $measurement
     * @return array
     */
    protected function getMinMax($recordings, $measurement)
    {
        $array = [];
        foreach($recordings as $recording) {
            $decodedEvents = json_decode($recording->events->event_payload);
            if(count($decodedEvents) > 0) {
                foreach($decodedEvents->events_filtered_json_data as $event) {
                    if(isset($event->MEASUREMENTS)) {
                        if($measurement == 'qrs')
                            array_push($array, $event->MEASUREMENTS->QRS_AVER);
                        if($measurement == 'pr')
                            array_push($array, $event->MEASUREMENTS->PR_AVER);
                        if($measurement == 'qt')
                            array_push($array, $event->MEASUREMENTS->QT_AVER);
                    }
                }
            }
            if(count($recording->remeasurements) > 0) {
                foreach($recording->remeasurements as $remeasurement) {
                    if($remeasurement->type == $measurement) {
                        array_push($array, round(60/$remeasurement->measurement_length*1000));
                    }
                }
            }
        }
        if(empty($array)) {
            $array = [
                0 => 'N/A'
            ];
        }
        return [
            'min' => min($array),
            'max' => max($array)
        ];
    }

    /**
     * Present the transmission report form
     * @param $recordingSession
     * @return array
     */
    private function presentTransmissionReport($recordingSession)
    {
        $hrAverMeasurements = [];
        $prAverMeasurements = [];
        $qrsAverMeasurements = [];
        $qtAverMeasurements = [];

        foreach($recordingSession->recordings as $recording) {
            $decodedEvents = json_decode($recording->events->event_payload);
            if(count($decodedEvents) > 0) {
                foreach($decodedEvents->events_filtered_json_data as $event) {
                    if(isset($event->MEASUREMENTS)) {
                        array_push($hrAverMeasurements, $event->MEASUREMENTS->HR_MAX);
                        array_push($hrAverMeasurements, $event->MEASUREMENTS->HR_MIN);
                        array_push($prAverMeasurements, $event->MEASUREMENTS->PR_AVER);
                        array_push($qrsAverMeasurements, $event->MEASUREMENTS->QRS_AVER);
                        array_push($qtAverMeasurements, $event->MEASUREMENTS->QT_AVER);
                    }
                }
            }
            if(count($recording->remeasurements) > 0) {
                foreach($recording->remeasurements as $remeasurement) {
                    if($remeasurement->type == 'pr') {
                        array_push($prAverMeasurements, $remeasurement->measurement_length*1000);
                    } elseif($remeasurement->type == 'qrs') {
                        array_push($qrsAverMeasurements, $remeasurement->measurement_length*1000);
                    } elseif($remeasurement->type == 'qt') {
                        array_push($qtAverMeasurements, $remeasurement->measurement_length);
                    } elseif($remeasurement->type == 'rr') {
                        array_push($hrAverMeasurements, round(60/$remeasurement->measurement_length*1000));
                    }
                }
            }
        }

        if(empty($hrAverMeasurements)) {
            $hrAverMeasurements = [
                0 => 'N/A'
            ];
        }
        if(empty($prAverMeasurements)) {
            $prAverMeasurements = [
                0 => 'N/A'
            ];
        }
        if(empty($qrsAverMeasurements)) {
            $qrsAverMeasurements = [
                0 => 'N/A'
            ];
        }
        if(empty($qtAverMeasurements)) {
            $qtAverMeasurements = [
                0 => 'N/A'
            ];
        }

        return [
            'rr' => [
                'min' => min($hrAverMeasurements),
                'max' => max($hrAverMeasurements)
            ],
            'pr' => [
                'min' => min($prAverMeasurements),
                'max' => max($prAverMeasurements)
            ],
            'qrs' => [
                'min' => min($qrsAverMeasurements),
                'max' => max($qrsAverMeasurements)
            ],
            'qt' => [
                'min' => min($qtAverMeasurements),
                'max' => max($qtAverMeasurements)
            ]
        ];
    }

    /**
     * Chunk measurements for a recording
     * @param $recording
     * @return array
     */
    protected function chunkRemeasurements($recording)
    {
        $rrMeasurement = '';
        $qrsMeasurement = '';
        $prMeasurement = '';
        $qtMeasurement = '';
        $decodedEvents = json_decode($recording->events->event_payload);
        if(count($decodedEvents) > 0) {
            foreach($decodedEvents->events_filtered_json_data as $event) {
                if(isset($event->MEASUREMENTS)) {
                    $rrMeasurement = $event->MEASUREMENTS->HR_AVER;
                    $qrsMeasurement = $event->MEASUREMENTS->QRS_AVER;
                    $prMeasurement = $event->MEASUREMENTS->PR_AVER;
                    $qtMeasurement = $event->MEASUREMENTS->QT_AVER;
                }
            }
        }
        if(count($recording->remeasurements) > 0) {
            foreach($recording->remeasurements as $remeasurement) {
                if($remeasurement->type == 'pr') {
                    $prMeasurement = $remeasurement->measurement_length*1000;
                } elseif($remeasurement->type == 'qrs') {
                    $qrsMeasurement = $remeasurement->measurement_length*1000;
                } elseif($remeasurement->type == 'qt') {
                    $qtMeasurement = $remeasurement->measurement_length;
                } elseif($remeasurement->type == 'rr') {
                    $rrMeasurement = (60/$remeasurement->measurement_length*1000);
                }
            }
        }
        return [
                'rr'    => $rrMeasurement,
                'pr'    => $prMeasurement,
                'qrs'   => $qrsMeasurement,
                'qt'    => $qtMeasurement
        ];
    }

    /**
     * Get reported events for a recording
     * @param $recording
     * @return mixed
     */
    private function getReportedEventsForRecording($recording)
    {
        $rawString = 'patient_id = "' . $recording->patient->id . '" AND ';
        $rawString .= 'timestamp >= "' . $recording->timestamp . '" AND timestamp <= "' . $recording->timestamp->addSeconds(30) . '"';

        $reported_events =  \DB::table('reported_events')
            ->select('*')
            ->whereRaw($rawString)
            ->orderBy('id', 'DESC')->get();
        $events = [];
        $patient_events = collect(Config::get('monebo_ecg.patient_events'));
        if(count($reported_events) > 0) {
            foreach($reported_events as $event) {
                array_push($events, $patient_events->where('id', $event->event_id)->first());
            }
        }
        return $events;
    }

}