<?php
namespace App\Transformers;

use Carbon\Carbon;
use App\Models\RecordingSession;

/**
 * Class RecordingSessionTransformer
 * @package App\Transformers
 */
class RecordingSessionTransformer
{

    /**
     * Transform the output of Recording Sessions
     *
     * @param $recordingSessions
     */
    public function transformOutput($recordingSessions, $sort = false)
    {
        if($recordingSessions->count() > 0) {
            $recordingSessions->transform(function($session) use($sort) {
                if($session->recordings->count() > 0)
                    $last_recording = $session->recordings->last()->created_at->setTimezone('America/New_York')->format('m/d/Y h:i:s a');
                else
                    $last_recording = "No recordings yet...";

                if($session->devices->count() > 0 && is_null($session->is_archived)) {
                    $devices = $session->devices->transform(function($device) {
                        return $device->radio_id;
                    });
                }
                elseif($session->devices->count() == 0 && !is_null($session->is_archived)) {
                    $devices = "This session was archived " . $session->is_archived->setTimezone('America/New_York')->format('m/d/y h:i:s a');
                }
                else {
                    $devices = "No devices have been assigned to this session.";
                }

                if(isset($session->patient->users) && $session->patient->users->count() > 0) {
                    $users = $session->patient->users->transform(function($user){
                        if(isset($user->roles) && $user->roles->count() >0)
                            return ['name' => $user->name_first . ' ' . $user->name_last, 'role' => $user->roles->first()->slug];
                        else
                            return ['name' => 'No User', 'role' => 'No Role Assigned'];
                    });
                } else {
                    $users = "No analysts have been assigned to this session.";
                }

                if(isset($session->patient)) {
                    $patient_name = $session->patient->name_first . ' ' . $session->patient->name_last;
                    $patient_id = $session->patient_id;
                } else {
                    $patient_name = '';
                    $patient_id = '';
                }
                $array = [
                    'id'            => ($session->id != null ? $session->id : ''),
                    'patient_id'    => $patient_id,
                    'patient'       => $patient_name,
                    'session_pin'   => ($session->session_pin != null ? $session->session_pin: ''),
                    'devices'       => $devices,
                    'start_date'    => ($session->date_start != null ? $session->date_start->format('M. d, Y') : ''),
                    'end_date'      => ($session->date_end != null ? $session->date_end->format('M. d, Y') : ''),
                    'analysts'      => $users,
                    'last_recording' => $last_recording
                ];
                if($sort) {
                    if($sort == 'asc') {

                    } elseif($sort == 'desc') {

                    }
                }
                return $array;
            });
        } else {
            return null;
        }
    }

    /**
     * Return a recording session for editing
     * @param RecordingSession $recordingSession
     * @return array
     */
    public function transformRecordingSession(RecordingSession $recordingSession)
    {
        return [
            'id'            => $recordingSession->id,
            'patient_id'    => $recordingSession->patient_id,
            'session_pin'   => $recordingSession->session_pin,
            'assigned_by'   => $recordingSession->assignor->username,
            'indications'   => $recordingSession->indications,
            'date_start'    => $recordingSession->date_start->format('m/d/y'),
            'date_end'      => $recordingSession->date_end->format('m/d/y'),
            'is_archived'   => $recordingSession->is_archived,
            'created_at'    => $recordingSession->created_at,
            'updated_at'    => $recordingSession->updated_at,
            'deleted_at'    => $recordingSession->deleted_at,
            'devices'       => $recordingSession->devices,
            'patient'       => $recordingSession->patient,
            'users'         => $recordingSession->patient->users->transform(function($user){
                return [
                    'id' => $user->id,
                    'username'          => $user->username,
                    'name_first'        => $user->name_first,
                    'name_last'         => $user->name_last,
                    'email'             => $user->email,
                    'created_at'        => $user->created_at,
                    'updated_at'        => $user->updated_at,
                    'deleted_at'        => $user->deleted_at,
                    'user_type'         => $user->pivot->user_type,
                    'selected_location' => $user->pivot->location_id,
                    'locations'         => $user->locations->transform(function($location){
                        return [
                            'location_id'           => $location->id,
                            'location_street'       => $location->street,
                            'location_street_2'     => $location->street_2,
                            'location_city'         => $location->city,
                            'location_province'     => $location->province,
                            'location_postal_code'  => $location->postal_code,
                            'location_phone'        => $location->phone,
                            'location_fax'          => $location->fax
                        ];
                    }),
                    'roles'             => $user->roles
                ];
            })
        ];
    }

    /**
     * Sort the array
     *
     * @param $items
     * @param null $direction
     * @return mixed
     */
    public function sortArray($items, $direction = null)
    {
        return $items;
    }

}