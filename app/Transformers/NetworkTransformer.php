<?php

namespace App\Transformers;

use Config;

class NetworkTransformer
{

    /**
     * Transform a collection of networks
     * @param $networks
     * @return mixed
     */
    public function transformOutput($networks)
    {
        return
            $networks->transform(function ($item, $key) {

                if(count($item->users))
                    $manager = $item->users->first()->email;
                else
                    $manager = "No Manager Selected";

                if(isset($item->province)) {
                    $province = $this->mapProvince($item->province);
                } else {
                    $province = null;
                }
                return [
                            'id'            => $item->id,
                            'name'          => $item->name,
                            'manager'       => $manager,
                            'user_count'    => $item->users->count(),
                            'contact' => [
                                            'address'       => $item->address,
                                            'address_2'     => $item->address_2,
                                            'city'          => $item->city,
                                            'province'      => $province,
                                            'postal_code'   => $item->postal_code,
                                            'phone'         => $item->phone,
                                            'fax'           => $item->fax,
                                            'details'       => $item->details,
                                        ],

                            'practices' => $item->practices,
                            'created_at'    => $item->created_at->format('m/d/y h:i:s a'),
                     ];
            });
    }

    /**
     * Map a province
     * @param $id
     * @return mixed
     */
    private function mapProvince($id)
    {
        $provinces = collect(Config::get('available_locations'));
        return $provinces->get($id);
    }

}