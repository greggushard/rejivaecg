<?php
namespace App\Transformers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

/**
 * Class AlertTransformer
 * @package App\Transformers
 */
class AlertsTransformer
{

    /**
     * @param $alerts
     * @return mixed
     */
    public function transformOutput($alerts)
    {
        return
            $alerts->transform(function($alert, $key){
                return [
                    'patient' => '#'.$alert[0]->patient_id,
                    'patient_id' => $alert[0]->patient_id,
                    'alert_count' => [
                        'red' => $alert->where('alert_type', 'red')->count(),
                        'yellow'   => $alert->where('alert_type', 'yellow')->count()
                    ],
                    'alerts' => $alert->transform(function($v, $k){

                        if($v->flagged_at !== null)
                            $flagged = "This alert was flagged on " . $v->flagged_at->format('m/d/Y') .' by ' . $v->flagger->name_first . ' ' . $v->flagger->name_last;
                        else
                            $flagged = false;
                        if($v->reviewed_at !== null)
                            $reviewed = "This alert was reviewed on " . $v->reviewed_at->format('m/d/Y') . ' by ' . $v->flagger->name_first . ' ' . $v->flagger->name_last;

                        else
                            $reviewed = false;
                        if($v->alert_type == 'red')
                            $alert_html = '<span class="label label-danger"><strong>RED</strong></span>';
                        elseif($v->alert_type == 'yellow')
                            $alert_html = '<span class="label label-warning"><strong>YELLOW</strong></span>';

                        if(Storage::disk('local')->exists('ecg_graphs_png/' . $v->recording->recording_hash . '.png')) {
                            $img = "data:image/gif;base64,".base64_encode(Storage::disk('local')->get('ecg_graphs_png/' . $v->recording->recording_hash . '.png'));
                        } else {
                            $img = "data:image/gif;base64,".base64_encode(Storage::disk('local')->get('placeholders/data-missing.png'));
                        }
                        return [
                            'alert_id' => $v->id,
                            'tag' => $alert_html,
                            'flagged' => $flagged,
                            'reviewed' => $reviewed,
                            'recording_id' => $v->recording_id,
                            'recording_hash' => $v->recording->recording_hash,
                            'recording_session_id' => $v->recording->recording_session->id,
                            'graph' => $img,
                            'created_at' => $v->created_at->format('m/d/Y h:i:s a'),
                            'updated_at' => $v->updated_at->format('m/d/Y h:i:s a')
                        ];
                    })->toArray()
                ];
            });
    }

    /**
     * Return a preview of the transmission report
     *
     * @param $alerts
     */
    public function transformTransmissionReportPreview($alerts)
    {
        return
            [
                'patient' => $alerts->first()->patient->name_first . ' ' . $alerts->first()->patient->name_last,
                'dob'     => $alerts->first()->patient->dob->format('m/d/y'),
                'age'     => $alerts->first()->patient->dob->diffInYears(Carbon::now()),
                'recording_session' => $alerts->first()->recording->recording_session->id,
                'recordings'  => $alerts->transform(function($alert, $key){
                    return
                        [
                            'recording_date' => $alert->recording->created_at->format('m/d/y h:i:s a'),
                            'strip'          => "data:image/gif;base64,".base64_encode(Storage::disk('local')->get('ecg_graphs_reports/' . $alert->recording->recording_hash . '.png')),
                            'flagged_by'     => $alert->flagger->name,
                            'flagged_at'     => $alert->flagged_at->format('m/d/y h:i:s a')
                        ];

                })
            ];
    }

}