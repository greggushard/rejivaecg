<?php
/**
 * Created by PhpStorm.
 * User: home
 * Date: 11/28/16
 * Time: 11:44 AM
 */

namespace App\Transformers;

/**
 * Class NoteTransformer
 * @package App\Transformers
 */
class NoteTransformer
{
    /**
     * Transform a single note for display on the page
     *
     * @param $note
     * @return array
     */
    public function transformNotes($notes)
    {
        return
                $notes->transform(function ($note, $key) {
                    $role = $note->user->roles->first();
                    if($role->slug == 'admin') {
                        $role_text = '<span class="label label-primary">Administrator</span>';
                    } elseif($role->slug == 'physician') {
                        $role_text = '<span class="label label-default">Physician</span>';
                    } elseif($role->slug == 'network-admin') {
                        $role_text = '<span class="label label-success">Network Administrator</span>';
                    } elseif($role->slug == 'interpreting-cardiologist') {
                        $role_text = '<span class="label label-danger">Interpreting Cardiologist</span>';
                    } elseif($role->slug == 'cardiac-tech') {
                        $role_text = '<span class="label label-warning">Cardiac Technician</span>';
                    } else {
                        $role_text = 'No Role Selected';
                    }
                    return [
                        'id'            => $note->id,
                        'created_at'    => $note->created_at->format('m/d/y h:i:s a'),
                        'user'          => $note->user->name,
                        'user_badge'    => $role_text,
                        'category'      => $note->note_category->description,
                        'content'       => $note->content,
                    ];
            });
    }

    /**
     * Transform a single note
     *
     * @param $note
     * @return array
     */
    public function transformSingleNote($note)
    {
        $role = $note->user->roles->first();
        if($role->slug == 'admin') {
            $role_text = '<span class="label label-primary">Administrator</span>';
        } elseif($role->slug == 'physician') {
            $role_text = '<span class="label label-default">Physician</span>';
        } elseif($role->slug == 'network-admin') {
            $role_text = '<span class="label label-success">Network Administrator</span>';
        } elseif($role->slug == 'interpreting-cardiologist') {
            $role_text = '<span class="label label-danger">Interpreting Cardiologist</span>';
        } elseif($role->slug == 'cardiac-tech') {
            $role_text = '<span class="label label-warning">Cardiac Technician</span>';
        } else {
            $role_text = 'No Role Selected';
        }

        return [
            'id'            => $note->id,
            'created_at'    => $note->created_at->format('m/d/y h:i:s a'),
            'user'          => $note->user->name,
            'user_badge'    => $role_text,
            'category'      => $note->note_category->description,
            'content'       => $note->content,
        ];
    }

}