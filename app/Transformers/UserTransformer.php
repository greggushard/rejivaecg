<?php

namespace App\Transformers;

use Config;

class UserTransformer
{

    /**
     * Transform a collection of notifications
     * @param $posts
     * @return mixed
     */
    public function transformOutput($users)
    {
        return
            $users->transform(function($item, $key) {

             if(count($item->networks) == 0)
                    $network = "No Network Selected.";
                else
                    $network = $item->networks->first();

                if(count($item->roles) == 0)
                    $role = "No role selected.";
                else
                    $role = $item->roles->first();

                return [
                    'id'            =>  $item->id,
                    'name_first'    =>  $item->name_first,
                    'name_last'     =>  $item->name_last,
                    'username'      =>  $item->username,
                    'role'          =>  $role,
                    'email'         =>  $item->email,
                    'network'       =>  $network,
                    'locations'     =>  $item->locations->transform(function($location){
                                            return [
                                                'location_id'           => $location->id,
                                                'location_street'       => $location->street,
                                                'location_street_2'     => $location->street_2,
                                                'location_city'         => $location->city,
                                                'location_province'     => $location->province,
                                                'location_postal_code'  => $location->postal_code,
                                                'location_phone'        => $location->phone,
                                                'location_fax'          => $location->fax
                                            ];
                                        }),
                    'created_at'    =>  $item->created_at->setTimezone('America/New_York')->format('F jS, Y h:i a'),
                    'updated_at'    =>  $item->updated_at->setTimezone('America/New_York')->format('F jS, Y h:i a')
                ];
            });
    }

    /**
     * Transform user output for editing
     * @param $user
     * @return array
     */
    public function transformOutputForEdit($user)
    {
        $role = $user->roles->first();

        if(count($user->networks) == 0)
            $network = "No Network Selected.";
        else
            $network = $user->networks->first();

        return [
            'id'            =>  $user->id,
            'name_first'    =>  $user->name_first,
            'name_last'     =>  $user->name_last,
            'username'      =>  $user->username,
            'role'          =>  $role,
            'email'         =>  $user->email,
            'network'       =>  $network,
            'locations'     =>  $user->locations->transform(function($location){
                return [
                    'location_id'           => $location->id,
                    'location_street'       => $location->street,
                    'location_street_2'     => $location->street_2,
                    'location_city'         => $location->city,
                    'location_province'     => $location->province,
                    'location_postal_code'  => $location->postal_code,
                    'location_phone'        => $location->phone,
                    'location_fax'          => $location->fax
                ];
            })
        ];
    }

    /**
     * Transform role with user badge
     * @param $users
     * @return mixed
     */
    public function transformOutputWithBadge($users)
    {
        return
            $users->transform(function($user, $key) {
                    $role = $user->roles->first();
                    if($role->level == 1) {
                        $role_text = '<span class="label label-primary pull-right">Administrator</span>';
                    } elseif($role->level == 3) {
                        $role_text = '<span class="label label-success pull-right">Network Administrator</span>';
                    } elseif($role->level == 4) {
                        $role_text = '<span class="label label-warning pull-right">Cardiac Technician</span>';
                    } elseif($role->level > 4) {
                        $role_text = '<span class="label label-danger pull-right">Physician</span>';
                    } else {
                        $role_text = 'No Role Selected';
                    }
                    return [
                        'id'        => $user->id,
                        'name'      => $user->name_first . ' ' . $user->name_last,
                        'username'  => $user->username,
                        'role'      => $role_text
                    ];
        });
    }

}