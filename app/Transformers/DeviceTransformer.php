<?php

namespace App\Transformers;


class DeviceTransformer
{

    /**
     * Transform an array of devices
     *
     * @param $device
     * @return mixed
     */
    public function transformOutput($devices)
    {
        return
            $devices->transform(function ($item, $key) {
                if(count($item->active_recording_session)) {
                    $patient = \App\Models\Patient::find($item->active_recording_session->first()->patient_id);
                    $active_recording_session = $item->active_recording_session->first()->id;
                    $status = '<span class="label label-primary"> Active</span>';
                    if(count($item->active_recording_session->first()->recordings))
                        $battery_level = $item->active_recording_session->first()->recordings->last()->battery_level;
                    else
                        $battery_level = 'Unknown';
                    $active = true;
                }
                else {
                    $patient = 'No Active Patient';
                    $active_recording_session = null;
                    $status = '<span class="label label-danger"> Inactive</span>';
                    $active = false;
                    $battery_level = 'Unknown';
                }

                return [
                    'id'            => $item->id,
                    'device_id'     => $item->radio_id,
                    'patient'       => $patient,
                    'active_recording_session' => $active_recording_session,
                    'battery_level' => $battery_level,
                    'status'        => $status,
                    'active'        => $active,
                    'created_at'    => $item->created_at->format('d/m/Y h:i:s'),
                    'updated_at'    => $item->updated_at->format('d/m/Y h:i:s')
                ];
            });
    }

    /**
     * Transofmr a single device for the API
     *
     * @param $device
     * @return array
     */
    public function transformDevice($device)
    {

        if(count($device->active_recording_session)) {
                $patient = \App\Models\Patient::find($device->active_recording_session->patient_id);
                $active_recording_session = $device->active_recording_session->id;
                $status = '<span class="label label-success"> Active</span>';
                $active = true;
        }
        else {
            $patient = 'No Active Patient';
            $active_recording_session = $device->active_recording_session->id;
            $status = '<span class="label label-danger"> Inactive</span>';
            $active = false;
        }

        return [
            'id'            => $device->id,
            'device_id'     => $device->radio_id,
            'patient'       => $patient,
            'active_recording_session' => $active_recording_session,
            'status'        => $status,
            'active'        => $active,
            'created_at'    => $device->created_at->format('d/m/Y h:i:s'),
            'updated_at'    => $device->updated_at->format('d/m/Y h:i:s')
        ];
    }

    /**
     * Transform a pretty looking HTML for the battery level
     *
     * @param $recordings
     */
    protected function getLastRecordingBatteryLevel($battery_level)
    {
        if(!$battery_level)
            return 'Unknown';
        elseif($battery_level < 100 && $battery_level >= 75)
            return '<span  style="color:#2f4f4f"><i class="fa fa-battery-full"></i></span>';
        elseif($battery_level < 75 && $battery_level >= 50)
            return '<span  style="color:#367EF8"><i class="fa fa-battery-3"></i></span>';
        elseif($battery_level < 50 && $battery_level >= 25)
            return '<span style="color:#8fbc8f"><i class="fa fa-battery-2"></i></span>';
        elseif($battery_level < 25 && $battery_level >= 5)
            return '<h4><span style="color:#ff4500"><i class="fa fa-battery-1"></i></span></h4>';
        elseif($battery_level < 5)
            return '<h4><span style="color:#FF0000"><i class="fa fa-battery-empty"></i></span></h4>';
    }

}