<?php
namespace App\Transformers;

use Storage;
use Carbon\Carbon;

class ClipsTransformer
{

    /**
     * Return a preview of the transmission report
     *
     * @param $alerts
     */
    public function transformTransmissionReportPreview($clips)
    {
        return
            [
                'patient' => $clips->first()->recording->patient->name_first . ' ' . $clips->first()->recording->patient->name_last,
                'dob'     => $clips->first()->recording->patient->dob->format('m/d/y'),
                'age'     => $clips->first()->recording->patient->dob->diffInYears(Carbon::now()),
                'recording_session' => $clips->first()->recording->recording_session->id,
                'recordings'  => $clips->transform(function($clip, $key){
                    return
                        [
                            'recording_date' => $clip->recording->created_at->format('m/d/y h:i:s a'),
                            'strip'          => "data:image/gif;base64,".base64_encode(Storage::disk('local')->get('ecg_graphs_reports/' . $clip->recording->recording_hash . '.png')),
                            'flagged_by'     => $clip->clipper->name,
                            'flagged_at'     => $clip->clipped_at->format('m/d/y h:i:s a')
                        ];
                })
            ];
    }

}