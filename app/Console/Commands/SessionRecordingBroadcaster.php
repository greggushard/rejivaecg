<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Patient;
use App\Models\Device;
use App\Models\RecordingSession;
use Illuminate\Console\Command;
use App\Services\NotificationService;
use App\Models\Notification;
use App\Events\GenerateNetworkWideNotification;

class SessionRecordingBroadcaster extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ecg {patient_id} {device_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a private redis broadcasting channel to receive 30 second strips as they come in';

    /**
     * @var
     */
    protected $device;

    /**
     * @var Patient
     */
    protected $patient;


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment("Creating patient / device channel " . $this->argument('patient_id') . ":" . $this->argument('device_id'));

        $session = RecordingSession::where(['patient_id' => $this->argument('patient_id'), 'date_end' => null])->get()->last();
        if($session) {
            $session->date_end = Carbon::now();
            $session->save();
        }

        $device = Device::where('device_id', $this->argument('device_id'))->get()->first();

        $session = (new RecordingSession());
        $session->date_start = Carbon::now();
        $session->date_end = null;
        $session->patient()->associate($this->argument('patient_id'));
        $session->device()->associate($device->id);
        $session->assigned_by = 2;
        $session->save();

        $service = new NotificationService(new Notification());
        $notification = $service->storeNotification('recording_session_started', 'A new recording session was started with device: ' . $device->device_id);
        $service->assignNotificationsToUsersInNetwork($notification, $session->patient->network->users());
        event(new GenerateNetworkWideNotification($notification));
    }
}