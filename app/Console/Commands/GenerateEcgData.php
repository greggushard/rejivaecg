<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Patient;
use App\Services\NotificationService;
use App\Events\GenerateNetworkWideNotification;
use App\Models\Notification;
use App\Models\RecordingSession;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class GenerateEcgData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simulate:ecg';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Simulate a POST response with raw ECG data';

    /**
     * @var
     */
    protected $device;

    /**
     * @var Patient
     */
    protected $patient;


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            $raw_ecg_data = \Storage::disk('local')->get('raw_ecg/95cc8667f0731db169cea9c11b32656b.raw');

            $client     =   new Client;
            try {
                $response = $client->request('POST', env('APP_URL') . 'api/v1/recordings', [
                    'headers'        => [
                        'Accept-Encoding'   => 'application/json',
                        'Authorization'     => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImI2ZWFjNDVkOWVjNjQ4NTYxYWZiZjgyMjVjZjMzYzMzNzdjZjQzNTYzYzcwNzBiNTM1ZjI0ZDczZWU1MTNhYzBhYmM2NGFlZWI5MDViNWUzIn0.eyJhdWQiOiI1IiwianRpIjoiYjZlYWM0NWQ5ZWM2NDg1NjFhZmJmODIyNWNmMzNjMzM3N2NmNDM1NjNjNzA3MGI1MzVmMjRkNzNlZTUxM2FjMGFiYzY0YWVlYjkwNWI1ZTMiLCJpYXQiOjE0Nzg3MDM4NDAsIm5iZiI6MTQ3ODcwMzg0MCwiZXhwIjoxNDc5OTk5ODQwLCJzdWIiOiIyIiwic2NvcGVzIjpbIioiXX0.BlMN8YVaWoSfqTyfhtUnzdG7eINK7sr58AJBCBbwaoDgGDgUis85oM-cowD-AxEG8MtYjUWaIeSQkKc1bU8WJX1Fx7jC4pSwVqhigMoq-lOu7XNSr9V6laq3MNimUeGx9IN8dhipyRFLYu69AysIGNVUifv2AeK6EEyKZ7tDvEib9nafu4ZVx6ZMBqT0sv6Ey0ywMAj55-8sAawlvl63SLw90vD2PTBFJVISPDuf78TeTARvYNav-mD8P5J3N5Szumhx2fZ-d_M3oZ38PMjW_LGWjv98Z6U4UAsFZmHJN6KgqPcpR7Pkxsd7YXD1RMt5NICSgxGizfi0WcO9WPemZApxhZ5q-3ixN7eTviVNWWMu3m2QxwYVCGV0achlY7iXSLt3ulj90TVZoBhvSC0HB2oDnb_VmOJRA8vqWVHcEpASIhOUbPTDn4JNulX5r4CBa0oDr8XhfVwuuvHVqK1EI2s6Vhc5K8hv0eCtPw1XUzgRLxqPM4KAh4sDgLTrpA1C7g4mwn6EogwX6nBViGHJo_i89YPYwxr6F9_LPr1xbY9vDMHRRTlKL-fOYpA_r7dh9eja2Eb0FYssDWxYUHhdYQW1VS7EloWsnn-Lxm95uUvsn2e7QKe1oh9VEjB53Eiznfn9SubXv8QZY_TZLoaPScJLCW1NFrDVSLQ-We2FHRM',
                    ],
                    'form_params' => [
                        'raw_ecg_data'     =>  $raw_ecg_data,
                        'sample_rate'      =>  500,
                        'device_id'        =>  '5544',
                        'phone_id'         => 'abcde000011111222223333',
                        'body_position'    => 'Upright',
                        'heart_rate'       => 86,
                        'battery_level'    => 50,
                        'timestamp'        => time()
                    ]
                ]);
                \Log::info(json_encode($response->getBody()->getContents()));
            } catch(\Exception $e) {
                \Log::error(json_encode($e));
            }
    }

}