<?php

namespace App\Http;

use App\Http\Middleware\ForceSecure;
use App\Http\Middleware\VerifyUserIsAdmin;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \Laravel\Passport\Http\Middleware\CreateFreshApiToken::class,
          //  \App\Http\Middleware\ForceSecure::class
        ],

        /**
         |------------------------------------------------
         |Rejiva API Specific Route Mappings
         |
         **/
        'api' => [
            'throttle:500,1',
            'bindings',
            'auth:api',

        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,

        # Added
        'notifications' => \App\Http\Middleware\UserNotifications::class,
        'allScopes' => \Laravel\Passport\Http\Middleware\CheckScopes::class,


        # Role & Permission Api Verifications
        'verifyAdmin' => \App\Http\Middleware\Api\VerifyUserIsAdmin::class,
        'verifyNetworkAdmin' => \App\Http\Middleware\Api\VerifyUserIsNetworkAdmin::class,
        'verifyCardiacTech' => \App\Http\Middleware\Api\VerifyUserIsCardiacTech::class,
        'verifyCardiologist' => \App\Http\Middleware\Api\VerifyUserIsCardiologist::class,
        'verifyPhysician' => \App\Http\Middleware\Api\VerifyUserIsPhysician::class,

        # ID Guards
        'verifyUserIsInNetwork' => \App\Http\Middleware\Api\VerifyUserIsInNetwork::class,
        'verifyUserIsInPractice' => \App\Http\Middleware\Api\VerifyUserIsInPractice::class,
        'verifyUserHasPatient' => \App\Http\Middleware\Api\VerifyUserHasPatient::class,
        'verifyUserCanRefer' => \App\Http\Middleware\Api\VerifyUserCanRefer::class,
        'decompressJson' => \App\Http\Middleware\Api\DecompressJson::class,

        # Role & Permission Check Middleware for Web Portal
        'webVerifyAdmin' => \App\Http\Middleware\VerifyUserIsAdmin::class,
        'webVerifyNetworkAdmin' => \App\Http\Middleware\VerifyUserIsNetworkAdmin::class,
        'webVerifyPatientPortalAccess' => \App\Http\Middleware\VerifyAccessToPatientPortal::class,
    ];
}
