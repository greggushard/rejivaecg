<?php

namespace App\Http\Controllers\Api;

use App\Models\Recording;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StripRequests\SaveNewStripRequest;

class StripsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(\Auth::guard('api')->user()->recordings()->orderBy('created_at', 'desc')->get());
    }

    /**
     * Filter saved strips by a patients' session
     *
     * @param $patient_id
     * @param $session_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterStripsByPatientSession($patient_id, $session_id)
    {
        $recordings = Recording::where('patient_id', $patient_id)
                                ->where('recording_session_id', $session_id)->whereHas('users', function($q){
                                    $q->where('users.id', \Auth::guard('api')->user()->id);
                                })->orderBy('created_at', 'desc')->get();
        return response()->json($recordings);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveNewStripRequest $request)
    {
        $user = \Auth::guard('api')->user();
        $user->recordings()->sync([$request['recording_id']], false);
        return response()->json([$user->save()]);
    }

}
