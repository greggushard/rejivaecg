<?php

namespace App\Http\Controllers\Api;

use App\Models\Patient;
use Illuminate\Http\Request;
use App\Services\PatientService;
use App\Http\Controllers\Controller;
use App\Transformers\PatientTransformer;
use App\Http\Requests\StorePatientRequest;

class PatientsController extends Controller
{

    /**
     * @var PatientService
     */
    protected $service;

    /**
     * @var PatientTransformer
     */
    protected $transformer;

    /**
     * PatientsController constructor.
     * @param PatientService $service
     */
    public function __construct(PatientService $service, PatientTransformer $transformer)
    {
        $this->service = $service;
        $this->transformer = $transformer;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = request();

        if(request()->has('sort')) {
            $patients = $this->service->sortPatients($request->sort);
        } elseif($request->has('assigned')) {
            $patients = $this->service->getAssignedPatients();
        } else {
            $patients = $this->service->getPatients();
        }

        if($request->exists('filter')) {
            $patients = $this->service->filterPatients($request);
        }

        return response()->json($patients);
    }


    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePatientRequest $request)
    {
        $patient =  $this->service->createPatient($request->all());
        return response()->json(['data' => $patient]);
    }

    /**
     * Returns a JSON response for editing a patient, returning
     * all of their fields
     * @param Request $request
     */
    public function edit($id)
    {
        $patient = Patient::find($id);
        return response()->json($patient);
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patient = Patient::with('network', 'recording_sessions.assignor', 'recording_sessions.devices')->find($id);
        return response()->json($this->transformer->transformPatientData($patient));
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePatientRequest $request, $id)
    {
        $patient = Patient::find($id);
        $patient = $this->service->editPatient($patient, $request->all());
        return response()->json(['data' => $patient]);
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patient = Patient::find($id);

        if($patient->delete()) {
            return response()->json(['data' => 'Complete.']);
        };
        return response()->json(['data' => 'Error deleting patient.']);
    }

    /**
     * List a patients' recording sessions
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function listSessionsByPatient($id)
    {
        $patient = Patient::with('recording_sessions')->find($id);
        return response()->json($patient->recording_sessions);
    }

    /**
     * Get recordings given a patients' ID
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRecordingsByPatient($id, $session_id, $offset = null)
    {
        $data = $this->service->getPatientRecordings($offset, $session_id, $id);
        return response()->json($data);
    }

    /**
     * Get the configurations necessary to create a patient
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getConfigurations()
    {
        return response()->json([
                    'provinces' => config('available_locations'),
                    'status' => config('statuss'),
                    'intRevStatus' => config('int_rev_statuss'),
                    'ethnicities' => config('ethnicitys'),
                    'patientGroups' => config('patient_groups'),
                    'phone_types' => config('phone_types')
                ]);
    }

}
