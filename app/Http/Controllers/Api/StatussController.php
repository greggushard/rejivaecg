<?php

namespace App\Http\Controllers\Api;

use Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatussController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Config::get('statuss'));
    }

}
