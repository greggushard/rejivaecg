<?php
namespace App\Http\Controllers\Api;

use Illuminate\Contracts\View\View;
use Storage;
use Illuminate\Http\Request;
use App\Models\RecordingSession;
use App\Http\Controllers\Controller;
use App\Services\Reports\ReportsService;
use App\Transformers\ReportsTransformer;
use App\Http\Requests\ReportRequests\CreateOrUpdateReportRequest;

/**
 * Class ReportsController
 * @package App\Http\Controllers\Api
 */
class ReportsController extends Controller
{

    /**
     * @var ReportsService
     */
    protected $service;

    /**
     * @var ReportsTransformer
     */
    protected $transformer;

    /**
     * ReportsController constructor.
     * @param ReportsService $service
     */
    public function __construct(ReportsService $service, ReportsTransformer $transformer)
    {
        $this->service = $service;
        $this->transformer = $transformer;
    }

    /**
     * Download a PDF report
     * @param $id
     * @return mixed
     */
    public function downloadReport($id)
    {
        $file =  $this->service->initiateDownloadRequest($id);
        return response()->download($file);
    }

    /**
     * Return a preview for the full disclosure report
     * @param CreateOrUpdateReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function previewFullDisclosureReport($session_id, Request $request)
    {
        $recordingSession = RecordingSession::with(['recordings' => function ($q) {
            $q->whereHas('alerts');
            $q->with('events');
            $q->with('notes');
            $q->with('remeasurements');
        }])->orderBy('id', 'desc')->find($session_id);

        $data = $this->transformer->transformForDisclosureReport($recordingSession);

        return response()->json($data);
    }

    /**
     * Preview the patient report
     * @param CreateOrUpdateReportRequest $request
     */
    public function previewPatientTransmissionReport(CreateOrUpdateReportRequest $request)
    {
        $clips = RecordingSession::with(['patient', 'recording', function($q) {
                                            $q->whereHas('clip');
                                        }])-find($request->recording_session_id);
        if($clips->count() == 0) {
            return response()->json(['error' => 'No recordings have been flagged!'], 422);
        }
        return response()->json($this->transformer->transformTransmissionReportPreview($clips));
    }

    /**
     * Generate a patient transmission report
     * @param CreateOrUpdateReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function generatePatientTransmissionReport(CreateOrUpdateReportRequest $request)
    {
        $recordingSession = RecordingSession::with(['recordings' => function ($q) {
            $q->whereHas('clip');
            $q->with('clip');
            $q->with('alerts.notes');
            $q->with('events');
            $q->with('notes');
            $q->with('remeasurements');
            $q->with('patient');
        }])->find($request->recording_session_id);

        $data = $this->transformer->transformForTransmissionReport($recordingSession);

          try {
              $response = $this->service->generatePatientTransmissionReport($data);
          } catch(\Exception $e) {
              return $e;
          }
          return response()->json([$response]);
    }

    /**
     * Generate a Holter Summary report for a patients' recording cycle
     * @param CreateOrUpdateReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateHolterReport(CreateOrUpdateReportRequest $request)
    {
        $recordingSession = RecordingSession::with(['events_count', 'recordings' => function ($q) {
                                $q->whereHas('alerts', function($q){
                                    $q->where('reviewed_at', null);
                                });
                                $q->with('alerts');
                                $q->with('remeasurements');
                                $q->with('patient');
                            }])->find($request->recording_session_id);
        $data = $this->transformer->transformForHolterReport($recordingSession);

        $response = $this->service->generateHolterReport($data);
        return response()->json([$response]);
    }

    /**
     * Generate a full disclosure report
     * @param CreateOrUpdateReportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateFullDisclosureReport(CreateOrUpdateReportRequest $request)
    {
        set_time_limit(999);
        $recordingSession = RecordingSession::with(['recordings' => function ($q) {
            $q->with('events');
            $q->with('notes');
            $q->with('remeasurements');
        }])->find($request->recording_session_id);

        $data = $this->transformer->transformForDisclosureReport($recordingSession);
        try {
            $response = $this->service->generateFullDisclosureReport($data);
        } catch(\Exception $e) {
            return response()->json(['error' => 'Error creating PDF' . json_encode($e)]);
        }
        return response()->json([$response]);
    }

    /**
     * Generate an optional report header
     * @param Request $request
     * @return View
     */
    public function getReportHeader(Request $request)
    {
        $input = $request->all();
        if($input['type'] == 'patient')
            return view('reports.headers.patient-report-header')->with(compact('input'));
        elseif($input['type'] == 'holter')
            return view('reports.headers.holter-report-header')->with(compact('input'));
        elseif($input['type'] == 'disclosure')
            return view('reports.headers.full-disclosure-report-header')->with(compact('input'));
    }

    /**
     * Generate an optional report footer
     * @param Request $request
     * @return View
     */
    public function getReportFooter(Request $request)
    {
        $input = $request->all();
        return view('reports.headers.patient-report-header')->with(compact('input'));
    }

    /**
     * Temporary PDF Endpoint - Generates a Patient report preview and returns an Patient Report (Preliminary or Final)
     * @return \Exception|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function frontEndPreview()
    {
        $recordingSession = RecordingSession::with(['recordings' => function ($q) {
            $q->whereHas('clip');
            $q->with('clip');
            $q->with('alerts.notes');
            $q->with('events');
            $q->with('notes');
            $q->with('remeasurements');
            $q->with('patient');
        }])->find(1);

        $data = $this->transformer->transformForTransmissionReport($recordingSession);
        try {
            $response = $this->service->previewFullDisclousreReport($data);
            return response()->download($response, 'patient-report-preview.pdf', ['Content-Type: application/pdf']);
        } catch(\Exception $e) {
            return $e;
        }
    }

}
