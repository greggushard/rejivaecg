<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use App\Models\Notification;
use App\Services\UserService;
use App\Events\UserWasSignedUp;
use App\Http\Controllers\Controller;
use App\Services\NotificationService;
use App\Http\Requests\StoreUserRequest;
use App\Events\GeneratePublicAdminNotification;
use App\Http\Requests\UpdateUserAccountRequest;


class UsersController extends Controller
{

    /**
     * @var UserService
     */
    protected $service;

    /**
     * @var UserTransformer
     */
    protected $transformer;

    /**
     * UsersController constructor.
     * @param UserService $service
     */
    public function __construct(UserService $service, UserTransformer $transformer)
    {
        $this->service = $service;
        $this->transformer = $transformer;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = request();

        if(request()->has('sort')) {
            $users = $this->service->sortUsers($request->sort);
        } else {
            $users = $this->service->getUsers();
        }

        if($request->exists('filter')) {
            $users = $this->service->filterUsers($request);
        }

        return response()->json($users);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $result =  $this->service->createUser($request->all());
        event(new UserWasSignedUp($result['user'], $result['clean_pw']));
        return response()->json(['data' => $result['user']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('locations', 'roles', 'networks')->find($id);

        $role = $user->roles->first();

        if($role->level == 1) {
            $role = '<span class="label label-primary pull-right">Administrator</span>';
        } elseif($role->level == 3) {
            $role = '<span class="label label-success pull-right">Network Administrator</span>';
        } elseif($role->level == 4) {
            $role = '<span class="label label-warning pull-right">Cardiac Technician</span>';
        }
        elseif($role->level > 4) {
            $role = '<span class="label label-danger pull-right">Physician</span>';
        }

        $user =  [
            'id'            => $user->id,
            'name'          => $user->name_first . ' ' . $user->name_last,
            'username'      => $user->username,
            'role'          => $role,
            'network'       => $user->networks->first(),
            'locations'     => $user->locations,
            'created_at'    => $user->created_at->format('M. D, Y, h:i:s a'),
            'updated_at'    => $user->updated_at->format('M. D, Y, h:i:s a')
        ];

        return response()->json($user);
    }

    /**
     * Edit a users' details
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        $user = User::with('locations', 'roles', 'networks')->find($id);
        return response()->json($this->transformer->transformOutputForEdit($user));
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUserRequest $request, $id)
    {
        $user = User::with('roles')->find($id);
        $user = $this->service->editUser($user, $request->all());
        return response()->json(['data' => $user]);
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if($user->delete()) {
            return response()->json(['data' => 'Complete.']);
        };

        return response()->json(['data' => 'Error deleting user.']);
    }

    /**
     * Update a users' account
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAccount(UpdateUserAccountRequest $request)
    {
        $user = User::find(\Auth::guard('api')->user()->id);
        $user = $this->service->updateUserAccount($user, $request, false);
        return response()->json(['data' => $user]);
    }

    /**
    |----------------------------------------------------------
    |OAuth User functions
    |----------------------------------------------------------
     */

    /**
     * Generate a personal access token for the logged in user
     * @return mixed
     */
    public function generateOauthToken()
    {
        $user = User::find(auth()->user()->id);
        return $user->createToken('Access Token', ['*'])->accessToken;
    }

    /**
     * Reset a users' password
     * @param $id
     * @return mixed
     */
    public function resetPassword($user_id)
    {
        $user = User::find($user_id);

        $new_password = str_random(8);
        $user->password = bcrypt($new_password);
        $user->update();

        $service = new NotificationService(new Notification());
        $notification = $service->storeNotification('user_password_reset', $user->email . ' password was reset.');
        $service->assignNotificationForRole($notification, 1);
        event(new GeneratePublicAdminNotification($notification));

        return response()->json(['data' => $user]);
    }

    /**
     * Get available locations for a user
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserLocations($user_id)
    {
        $user = User::with('locations')->find($user_id);
        return response()->json(
            $user->locations->transform(function($location){
                return [
                    'location_id'           => $location->id,
                    'location_street'       => $location->street,
                    'location_street_2'     => $location->street_2,
                    'location_city'         => $location->city,
                    'location_province'     => $location->province,
                    'location_postal_code'  => $location->postal_code,
                    'location_phone'        => $location->phone,
                    'location_fax'          => $location->fax
                ];
        }));
    }

    /**
     * Delete a users' location from the database
     * @param $user_id
     * @param $location_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyLocation($user_id, $location_id)
    {
        $user = User::with('locations')->find($user_id);
        return response()->json([$user->locations()->where('id', $location_id)->delete()]);
    }
}
