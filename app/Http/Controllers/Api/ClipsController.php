<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\Patient;
use App\Http\Controllers\Controller;
use App\Transformers\ClipsTransformer;
use App\Services\Clips\ClippingService;
use App\Http\Requests\ClipRequests\CreateOrUpdateClip;

/**
 * Class ClipsController
 * @package App\Http\Controllers\Api
 */
class ClipsController extends Controller
{

    /**
     * @var ClippingService
     */
    protected $service;

    protected $transformer;

    /**
     * ClipsController constructor.
     * @param ClippingService $service
     */
    public function __construct(ClippingService $service, ClipsTransformer $transformer)
    {
        $this->service = $service;
        $this->transformer = $transformer;
    }

    /**
     * Get clips for a recording session
     * @param $patient_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClipsByPatient($patient_id)
    {
        $clips = $this->service->getClipsByPatient($patient_id);
        if(empty($clips) || is_null($clips) || $clips->count() < 1) {
            $patient = Patient::find($patient_id);
            return response()->json(
                [
                    'patient' => $patient->name_first . ' ' . $patient->name_last,
                    'dob'     => $patient->dob->format('m/d/y'),
                    'age'     => $patient->dob->diffInYears(Carbon::now()),
                    'recording_session' => $patient->recording_sessions->last()->id
                ]);
        }

        return response()->json($this->transformer->transformTransmissionReportPreview($clips));
    }

    /**
     * Create or update a new clip
     * @param CreateOrUpdateClip $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateOrUpdateClip $request)
    {
        $clip = $this->service->storeNewClip($request->recording_id, $request->patient_id);
        return response()->json($clip);
    }

}
