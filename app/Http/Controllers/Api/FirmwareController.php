<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\Firmware\FirmwareService;
use App\Http\Requests\FirmwareRequests\GetCurrentFirmwareVersionRequest;
use App\Http\Requests\FirmwareRequests\UpdateCurrentFirmwareVersionRequest;

/**
 * Class FirmwareController
 * @package App\Http\Controllers\Api
 */
class FirmwareController extends Controller
{

    /**
     * @var FirmwareService
     */
    protected $service;

    /**
     * FirmwareController constructor.
     * @param FirmwareService $service
     */
    public function __construct(FirmwareService $service)
    {
        $this->service = $service;
    }

    /**
     * Get the current firmware of a device and return the latest firmware version
     * @param GetCurrentFirmwareVersionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkCurrentFirmware(GetCurrentFirmwareVersionRequest $request)
    {
        $response = $this->service->getCurrentFirmware($request['device_id']);
        return response()->json($response);
    }

    /**
     * Upgrade to the latest firmware version
     * @param UpdateCurrentFirmwareVersionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upgradeFirmware(UpdateCurrentFirmwareVersionRequest $request)
    {
        $response = $this->service->upgradeToNewFirmware($request['device_id'], $request['new_firmware_version']);
        return response()->json($response);
    }

}
