<?php

namespace App\Http\Controllers\Api;

use App\Models\Note;
use App\Models\NoteCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\NoteTransformer;
use App\Services\Notes\CreateNoteService;
use App\Http\Requests\NoteRequests\CreateOrUpdateNoteRequest;

/**
 * Class NotesController
 * @package App\Http\Controllers\Api
 */
class NotesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * List the available note categories
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function listCategories(Request $request)
    {
        if($request->type == 'flags') {
            return response()->json(NoteCategory::where('type', 'holter')->get());
        }

        return response()->json(NoteCategory::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($patient_id, $recording_id, CreateOrUpdateNoteRequest $request)
    {
        $note = new Note();
        $note = (new CreateNoteService($note))->createNewNote($recording_id, $request->all());
        return response()->json((new NoteTransformer())->transformSingleNote($note));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateOrUpdateNoteRequest $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
