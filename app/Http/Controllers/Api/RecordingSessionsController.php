<?php

namespace App\Http\Controllers\Api;

use App\Models\RecordingSession;
use App\Http\Controllers\Controller;
use App\Transformers\RecordingSessionTransformer;
use App\Services\RecordingSessions\RecordingSessionService;
use App\Http\Requests\RecordingSessionRequests\AuthenticateRecordingSession;
use App\Http\Requests\RecordingSessionRequests\CreateOrUpdateRecordingSessionRequest;

class RecordingSessionsController extends Controller
{

    /**
     * @var RecordingSession
     */
    protected $model;

    /**
     * @var RecordingSessionService
     */
    protected $service;

    /**
     * @var RecordingSessionTransformer
     */
    protected $transformer;

    /**
     * RecordingSessionsController constructor.
     * @param RecordingSession $model
     * @param RecordingSessionService $service
     */
    public function __construct(RecordingSession $model, RecordingSessionService $service, RecordingSessionTransformer $transformer)
    {
        $this->model = $model;
        $this->service = $service;
        $this->transformer = $transformer;
    }

    /**
     * Authenticate a recording session given a sessions' PIN
     * @param AuthenticateRecordingSession $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authorizeRecordingSession(AuthenticateRecordingSession $request)
    {
        $recordingSession = $this->service->authenticateRecordingSession($request->session_pin);
        return response()->json($recordingSession);
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = request();

        if(request()->has('sort')) {
            $recordingSessions = $this->service->sortRecordingSessions($request->sort);
        } else {
            $recordingSessions = $this->service->getRecordingSessions();
        }

        if($request->exists('filter')) {
            $recordingSessions = $this->service->filterRecordingSessions($request);
        }

        return response()->json($recordingSessions);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOrUpdateRecordingSessionRequest $request)
    {
        $recordingSession = $this->service->initiateNewRecordingSession($request->all());
        return response()->json($recordingSession);
    }

    /**
     * Get a recording session by ID
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $recordingSession = RecordingSession::with('devices', 'patient.users.roles', 'patient.users.locations')->find($id);
        return response()->json($this->transformer->transformRecordingSession($recordingSession));
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateOrUpdateRecordingSessionRequest $request, $id)
    {
        $recordingSession = $this->service->updateRecordingSession($id, $request->all());
        return response()->json($recordingSession);
    }

    /**
     * Archive and active recording session in the database
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $recording_session = $this->service->archiveRecordingSession($id);
        if($recording_session)
            return response()->json(['data' => 'The recording session has been permanently archived.']);
        else
            return response()->json(['error' => 'The recording session could not be archived']);
    }

}
