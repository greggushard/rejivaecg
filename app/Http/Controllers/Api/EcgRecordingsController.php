<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\Patient;
use App\Models\Recording;
use Illuminate\Http\Request;
use App\Jobs\RenderMoneboGraph;
use App\Http\Controllers\Controller;
use App\Transformers\NoteTransformer;
use App\Services\Events\CreateEventService;
use App\Transformers\ReportedEventTransformer;
use App\Services\Recordings\CreateRecordingService;
use App\Http\Requests\RecordingRequests\CreateRecordingRequest;

/**
 * Class EcgRecordingsController
 * @package App\Http\Controllers\Api
 */
class EcgRecordingsController extends Controller
{

    /**
     * @var CreateRecordingService
     */
    protected $service;

    /**
     * EcgRecordingsController constructor.
     *
     * @param CreateRecordingService $service
     */
    public function __construct(CreateRecordingService $service)
    {
        $this->service = $service;
    }

    /**
     * Return a list of all recordings given a patient ID
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if($request->has('id')) {
            return response()->json(Patient::with('recordings.notes', 'recordings.events', 'recordings.remeasurements', 'recordings.clip')->get($request->id));
        } else {
            return response()->json(Recording::all());
        }
    }

    /**
     * Get recording data for a hash
     *
     * @param Request $request
     * @return mixed
     */
    public function getRecordingByHash($recording_hash)
    {
        if($recording_hash) {
            if(\Storage::disk('local')->get('raw_graph_file/'. $recording_hash .'.gph')) {
                $data = \Storage::disk('local')->get('raw_graph_file/'. $recording_hash .'.gph');
            }
            $recording_data = explode(',', $data);
            $value = 0;
            foreach($recording_data as $k => $v){
                $recording_data[$k] = [$value, floatval($v)];
                $value+=4;
            }
            $recording_id = Recording::with('alerts', 'notes', 'events', 'patient', 'remeasurements', 'clip')->where('recording_hash', $recording_hash)->get()->first();
            return response()->json(
                [
                    'recording_events' => $recording_id->events,
                    'recording_remeasurements' => $recording_id->remeasurements,
                    'alerts' => is_null($recording_id->alerts) ? null : [
                                                                            'significant_event' => $recording_id->alerts->significant_event,
                                                                            'alert_type'        => $recording_id->alerts->alert_type,
                                                                            'event_id'          => $recording_id->alerts->event_id,
                                                                            'flagged_at'        => $recording_id->alerts->flagged_at,
                                                                            'flagged_by'        => is_null($recording_id->alerts->flagged_at) ? null : $recording_id->alerts->flagger->name_first . ' ' . $recording_id->alerts->flagger->name_last,
                                                                            'patient_id'        => $recording_id->alerts->patient_id,
                                                                            'reviewed_at'       => $recording_id->alerts->reviewed_at,
                                                                            'reviewed_by'       => is_null($recording_id->alerts->reviewed_at) ? null : $recording_id->alerts->reviewer->name_first . ' ' . $recording_id->alerts->reviewer->name_last,
                                                                            'id'                => $recording_id->alerts->id,
                                                                        ],
                    'reported_events' => (new ReportedEventTransformer())->transformOutput($this->service->mapExistingReportedEvents($recording_id->recording_hash), $recording_id),
                    'recording_notes' => (new NoteTransformer())->transformNotes($recording_id->notes),
                    'recording_data' => $recording_data,
                    'body_position' => $recording_id->body_position,
                    'breathing_rate' => $recording_id->breathing_rate,
                    'is_clipped' => $recording_id->clip,
                    'id' => $recording_id->id,
                ]);
        }
        else
            return response()->json(['data' => 'No recording identified']);
    }

    /**
     * Get a list of recording events by ID
     *
     * @param $id
     * @return mixed
     */
    public function getRecordingEvents($id)
    {
        return response()->json(Recording::with('events')->find($id));
    }

    /**
     * Return a list of recordings with a specific event ID
     *
     * @param $event_id
     */
    public function listRecordingsWithEvent(Request $request)
    {
        $event_payload = [];
        $reported_recordings = null;
        $recordings = null;
        foreach($request['events'] as $event) {
            if($event < 200) {
                array_push($event_payload, $event);
            }
        }
        $session_id = $request['session_id'];
        $patients = Patient::with('recordings', 'recordings.remeasurements', 'recordings.clip')->whereHas('recording_sessions', function($query) use($session_id) {
                        $query->where('recording_sessions.id', $session_id);
                    })->orderBy('created_at', 'DESC')->get();

        if(count($event_payload) > 0) {
            $reported_recordings = $this->service->filterReportedEvents($patients[0]->recordings, $event_payload, $patients[0]->id);
        }

        $recordings = $this->service->filterMoneboEvents($patients[0]->recordings, $request['events']);

        if(!is_array($reported_recordings)) {
            $reported_recordings = [];
        }
        if(!is_array($recordings)) {
             $recordings = [];
        }
        $recordingsCollection = collect($recordings);
        $reportedRecordingsCollection = collect($reported_recordings);
        $merged = $recordingsCollection->merge($reportedRecordingsCollection);

        if($request['offset']) {
            $merged = $merged->slice($request['offset']);
        }
        return response()->json($merged->take(30)->unique());
    }

    /**
     * Store a VitalVue Request
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        if($request->header('Content-Encoding') == 'gzip') {
            $json = file_get_contents('php://input');
            $request = json_decode(gzdecode($json), true);
        } else {
            $request = $request->all();
        }

        if($request['timestamp'] < 1)
            return false;

        // Check for sample rate
        if (isset($request['sample_rate'])) {
            $sampleRate = $request['sample_rate'];
        } else {
            $sampleRate = 250;
        }

        $sessionHasDevice = $this->service->getRecordingSessionDevice($request);
        (dechex((int)$request['device_id']));

        if(!$sessionHasDevice)
            return response()->json(['error' => 'This device, 7581' . (dechex((int)$request['device_id'])) .' is not registered, or has not been assigned to a patient.']);

        $lastRecording = $sessionHasDevice->first()->recordings->first();
        if($lastRecording !== null) {
            $difference = $lastRecording->timestamp->diffInSeconds(Carbon::createFromTimestamp($request['timestamp']));
            if($difference > 8)
                $missing_strips_count = round($difference/8, 0);
            else
                $missing_strips_count = 0;
        } else {
            $missing_strips_count = 0;
        }

        $request['raw_ecg_data'] = trim($request['raw_ecg_data']);

        $recording_id = (new CreateRecordingService())->createNewRecording($sessionHasDevice, $request, $missing_strips_count);

        // Algorithm run y/n
        if(env('APP_ENV') !== 'local') {
            $this->service->saveRawFile($recording_id->recording_hash, $request['raw_ecg_data']);
            $this->service->saveRawGraphFile($recording_id->recording_hash, $request['raw_ecg_data']);
            $this->service->normalizeFileForEvents($recording_id->recording_hash, $request['raw_ecg_data']);
            $results = $this->service->generateNormalizedResults($recording_id->recording_hash, $sampleRate);
            (new CreateEventService())->createNewEvent($recording_id->id, $results);
            dispatch(new RenderMoneboGraph($recording_id));
        } else {
            $results = "Local environment, algorithm did not run....";
        }

        // Response
        return response()->json([
                                    'data' => 'The recording has been submitted',
                                    'recording_results' => [
                                        'recording_hash' => $recording_id->recording_hash,
                                        'monebo_output' => $results
                                    ],
                                    'session_id' => $sessionHasDevice->first()->id,
                                    'missed_recordings' => $missing_strips_count
                                ]);
    }

    /**
     * Get a payload of graphs including their recording_id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRecordingGraphs(Request $request)
    {
        $images = [];
        foreach($request['hashes'] as $hash) {
            if(\Storage::disk('local')->exists('ecg_graphs_png/' . $hash . '.png')) {
                array_push($images,
                    [
                        'image' =>"data:image/gif;base64,".base64_encode(\Storage::disk('local')->get('ecg_graphs_png/' . $hash . '.png')),
                        'hash' => $hash,
                        'recording' => Recording::with('alerts')->where('recording_hash', $hash)->get()->transform(function($recording){
                            return [
                                'id' => $recording->id,
                                'created_at' => $recording->created_at->diffForHumans(),
                                'updated_at' => $recording->updated_at->diffForHumans(),
                                'alerts' => is_null($recording->alerts) ? null : [
                                                                                    'significant_event' => $recording->alerts->significant_event,
                                                                                    'alert_type' => $recording->alerts->alert_type,
                                                                                    'event_id' => $recording->alerts->event_id,
                                                                                    'flagged_at' => $recording->alerts->flagged_at,
                                                                                    'flagged_by' => $recording->alerts->flagger->name_first . ' ' . $recording->alerts->flagger->name_last,
                                                                                    'patient_id' => $recording->alerts->patient_id,
                                                                                    'reviewed_at' => $recording->alerts->reviewed_at,
                                                                                    'reviewed_by' => ($recording->alerts->reviewed_at == null ? null : $recording->alerts->reviewer->name_first . ' ' . $recording->alerts->reviewer->name_last),
                                                                                    'id' => $recording->alerts->id,
                                                                                ],
                                'clip' => $recording->clip
                            ];
                        })
                    ]
                );
            }
        }
        return response()->json($images);
    }

}
