<?php

namespace App\Http\Controllers\Api;

use Config;
use Carbon\Carbon;
use App\Models\Network;
use Illuminate\Http\Request;
use App\Models\RecordingSession;
use App\Http\Controllers\Controller;
use App\Services\Networks\SortNetworkService;
use App\Services\Networks\CreateNetworkService;
use App\Services\Networks\UpdateNetworkService;
use App\Http\Requests\NetworkRequests\NetworkCreateRequest;
use App\Http\Requests\NetworkRequests\NetworkUpdateRequest;
/**
 * Class NetworksController
 * @package App\Http\Controllers\Api
 */
class NetworksController extends Controller
{

    /**
     * @var SortNetworkService
     */
    protected $service;

    /**
     * NetworksController constructor.
     * @param SortNetworkService $service
     */
    public function __construct(SortNetworkService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = request();

        if(request()->has('sort')) {
            $networks = $this->service->sortNetworks($request->sort);
        } else {
            $networks = $this->service->getNetworks();
        }

        if($request->exists('filter')) {
            $networks = $this->service->filterNetworks($request);
        }

        return response()->json($networks);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NetworkCreateRequest $request)
    {
        $response = (new CreateNetworkService(new Network))->storeNetwork($request->all());
        return response()->json(['data' => $response]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $network = Network::with(['practices', 'users', 'patients'])->find($id);

        if(isset($network->province)) {
            $provinces = collect(Config::get('available_locations'));
            $province = $provinces->get($network->province);
        } else {
            $province = null;
        }

        $network = [
                'id' => $network->id,
                'name' => $network->name,
                'practices' => $network->practices,
                'contact' => [
                    'address'       => $network->address,
                    'address_2'     => $network->address_2,
                    'city'          => $network->city,
                    'province'      => $province,
                    'postal_code'   => $network->postal_code,
                    'phone'         => $network->phone,
                    'fax'           => $network->fax,
                    'details'       => $network->details,
                ],
        ];
        return response()->json($network);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NetworkUpdateRequest $request, $id)
    {
        $response = (new UpdateNetworkService(Network::find($id)))->updateNetwork($request->all());
        return response()->json(['data' => $response]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $network = Network::find($id);

        if($network->delete())
            return response()->json(['data' => 'Complete.']);

        return response()->json(['data' => 'Error deleting user.']);
    }

    /**
     * Return a list of members for a network
     *
     * @param Network $network_id
     */
    public function getMembers(Request $request, $id)
    {
        $network = Network::with('users.roles', 'users.locations')->find($id);
        $users = $network->users;

        $network_admins = $users->filter(function($user){
                                if($user->roles->first()->id == 3) {
                                   return true;
                                }
                            })->transform(function($network_admin){
                                    return [
                                        'id'         => $network_admin->id,
                                        'name'       => $network_admin->name_first . ' ' . $network_admin->name_last,
                                        'locations'  => $network_admin->locations,
                                        'created_at' => $network_admin->created_at,
                                        'updated_at' => $network_admin->updated_at,
                                    ];
                            })->values();

        $cardiac_technicians = $users->filter(function($user){
                                    if($user->roles->first()->id == 4) {
                                        return true;
                                    }
                                })->transform(function($network_admin){
                                    return [
                                        'id'         => $network_admin->id,
                                        'name'       => $network_admin->name_first . ' ' . $network_admin->name_last,
                                        'locations'  => $network_admin->locations,
                                        'created_at' => $network_admin->created_at,
                                        'updated_at' => $network_admin->updated_at,
                                    ];
                                })->values();

        $cardiologists = $users->filter(function($user){
                                if($user->roles->first()->id == 5 || $user->roles->first()->id == 6) {
                                    return true;
                                }
                         })->transform(function($network_admin){
                                return [
                                    'id'         => $network_admin->id,
                                    'name'       => $network_admin->name_first . ' ' . $network_admin->name_last,
                                    'locations'  => $network_admin->locations,
                                    'created_at' => $network_admin->created_at,
                                    'updated_at' => $network_admin->updated_at,
                                ];
                        })->values();

        $physicians = $users->filter(function($user){
                                if($user->roles->first()->id == 6 || $user->roles->first()->id == 5) {
                                    return true;
                                }
                            })->transform(function($network_admin){
                                return [
                                    'id'         => $network_admin->id,
                                    'name'       => $network_admin->name_first . ' ' . $network_admin->name_last,
                                    'locations'  => $network_admin->locations,
                                    'created_at' => $network_admin->created_at,
                                    'updated_at' => $network_admin->updated_at,
                                ];
                            })->values();
        
        return response()->json(
            [
                'network_admins'        => $network_admins,
                'cardiac_technicians'   => $cardiac_technicians,
                'cardiologists'         => $cardiologists,
                'physicians'            => $physicians
            ]);
    }

    /**
     * Get a list of available physicians in a network
     *
     * @param Network $network_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPhysicians($id)
    {
        $network = Network::with(['users' => function($q) {
                        $q->select('users.id', 'users.name_first', 'users.name_last')->whereHas('roles', function($q){
                            $q->where('roles.id', 6);
                        });
                    }])->find($id);
        return response()->json($network->users);
    }

    /**
     * Get patients in a network that are not assigned to a device
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPatients(Request $request, $id)
    {
        $patients = $this->service->getAvailablePatients($id);
        return response()->json($patients);
    }

    /**
     * Get devices in a network that are not assigned to a patient
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDevices(Request $request, $id)
    {
        $devices = $this->service->getAvailableDevices($id);
        return response()->json($devices);
    }

    /**
     * Return the active recording sessions within a network
     *
     * @param Request $request
     * @param $id
     */
    public function getActiveRecordingSessions(Request $request, $id)
    {
        $activeRecordingSessions = RecordingSession::with(['devices', 'patient', 'last_recording'])->where('date_end', '>', Carbon::now())->get();

        return $activeRecordingSessions->transform(function($item){

                if(!empty($item->last_recording))
                    $last_recording = $item->last_recording->created_at->format('M. d, Y h:i:s a');
                else
                    $last_recording = "No recordings submitted yet.";

                    return [
                            'id' => $item->id,
                            'date_start' => $item->date_start->format('M. d, Y'),
                            'date_end' => $item->date_end->format('M. d, Y'),
                            'patient_id' => $item->patient->id,
                            'patient' => $item->patient->name_first . ' ' . $item->patient->name_last,
                            'devices' => $item->devices->each(function($device){
                                return $device->radio_id;
                            }),
                            'last_recording' => $last_recording
                    ];
            });
    }

    /**
     * Assign a user to a network
     *
     * @param $network_id
     * @param $user_id
     */
    public function assignUser(Network $network_id, $user_id)
    {
        $network_id->users()->attach($user_id);
        $network_id->save();
        return response()->json($network_id->users);
    }

}
