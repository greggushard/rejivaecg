<?php

namespace App\Http\Controllers\Api;

use DB;
use App\Models\Device;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Models\ReportedEvent;
use App\Http\Controllers\Controller;
use App\Services\NotificationService;
use App\Services\Events\ReportedEventService;
use App\Events\GenerateNetworkWideNotification;
use App\Http\Requests\EventRequests\CreateOrUpdateEventRequest;

class EcgEventsController extends Controller
{

    protected $service;

    /**
     * EcgEventsController constructor.
     * @param ReportedEventService $service
     */
    public function __construct(ReportedEventService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(\Config::get('monebo_ecg'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a patient recorded symptom
     *
     * @param CreateOrUpdateEventRequest $request
     */
    public function storePatientReportedEvent(Request $request)
    {
        if($request->header('Content-Encoding') == 'gzip') {
            $json = file_get_contents('php://input');
            $request = json_decode(gzdecode($json), true);
        } else {
            $request = $request->all();
        }

        $device = Device::with('active_recording_session.patient')->where('device_id', $request['device_id'])->get()->first();

        if(!$device)
            return response()->json(['error' => 'No device with the device ID ' . $request['device_id' . ' was found']]);

        $reported_event = new ReportedEvent();
        $reported_event->patient_id = $device->active_recording_session->first()->patient->id;
        $reported_event->device_id = $device->id;
        $reported_event->event_id = $request['event_type'];
        $reported_event->breathing_rate = $request['breathing_rate'];
        $reported_event->battery_level = $request['battery_level'];
        $reported_event->body_position = $request['body_position'];
        $reported_event->phone_id = $request['phone_id'];
        $reported_event->timestamp = $request['timestamp'];
        $reported_event->heart_rate = $request['heart_rate'];
        $reported_event->save();
        $this->service->incrementPatientEvents($device->active_recording_session->first(), $reported_event->event_id);
        if($reported_event->event_id == 9)
            $this->alertNetworkOfFallDetection($reported_event, $device);
        return response()->json(['message' => 'Your event has been recorded!']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Create and send an alert that a fall was detected
     *
     * @param $event
     * @param $device
     */
    protected function alertNetworkOfFallDetection($event, $device)
    {
        /**
        $patient = \App\Models\Patient::find($device->active_recording_session->first()->patient_id);
        $patient = $patient->name_first . ' ' . $patient->name_last;
        $service = new NotificationService(new Notification());
        $notification = $service->storeNotification('fall_detected', 'FALL DETECTED for ' . $patient . ' <br /><a style="color:#FF0000" href="tel:911">Call Emergency Services</a>');
        $service->assignNotificationsToUsersInNetwork($notification, \App\Models\Network::with('users')->get()->first()->users());
        event(new GenerateNetworkWideNotification($notification));
         * **/

        return false;
    }
}
