<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Interpretation;
use App\Http\Controllers\Controller;
use App\Http\Requests\InterpretationRequests\CreateOrUpdateInterpretationRequest;

/**
 * Class InterpretationsController
 * @package App\Http\Controllers\Api
 */
class InterpretationsController extends Controller
{

    /**
     * @param CreateOrUpdateInterpretationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createInterpretation(CreateOrUpdateInterpretationRequest $request)
    {
        $interpretation = new Interpretation();
        $interpretation->content = $request->body_content;
        $interpretation->recording_session_id = $request->recording_session_id;
        $interpretation->recording_id = $request->recording_id;
        $interpretation->created_by = auth()->guard('api')->user()->id;
        $interpretation->save();
        return response()->json(['data' => $interpretation]);
    }

    /**
     * @param CreateOrUpdateInterpretationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateInterpretation(CreateOrUpdateInterpretationRequest $request, $id)
    {
        $interpretation = Interpretation::find($id);
        $interpretation->content = $request->body_content;
        $interpretation->recording_session_id = $request->recording_session_id;
        $interpretation->recording_id = $request->recording_id;
        $interpretation->created_by = auth()->guard('api')->user()->id;
        $interpretation->save();
        return response()->json(['data' => $interpretation]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteInterpretation($id)
    {
        $interpretation = Interpretation::find($id);
        $interpretation->destroy();
        return response()->json(['data' => $interpretation]);
    }
}
