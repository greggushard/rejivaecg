<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Metrics\MetricsService;

/**
 * Class MetricsController
 * @package App\Http\Controllers\Api
 */
class MetricsController extends Controller
{

    /**
     * @var MetricsService
     */
    protected $service;

    /**
     * MetricsController constructor.
     * @param MetricsService $service
     */
    public function __construct(MetricsService $service)
    {
        $this->service = $service;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNetworkRecordingsMetric($id)
    {
        $recordings = $this->service->getNetworkRecordingsMetric($id);
        return response()->json($recordings);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNetworkReportsMetric($id)
    {
        $reports = $this->service->getNetworkReportsMetric($id);
        return response()->json($reports);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNetworkUserActivityMetric($id)
    {
        $userActivity = $this->service->getNetworkUserActivityMetric($id);
        return response()->json($userActivity);
    }

    public function getNetworkActivityLogMetric($id)
    {
        $activityLog = $this->service->getActivityLog($id);
        return response()->json($activityLog);
    }

}
