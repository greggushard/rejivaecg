<?php

namespace App\Http\Controllers\Api;

use Config;
use Carbon\Carbon;
use App\Models\Alert;
use App\Models\Recording;
use App\Models\NoteCategory;
use Illuminate\Http\Request;
use App\Jobs\RenderReportGraph;
use App\Http\Controllers\Controller;
use App\Services\Alerts\AlertService;
use App\Transformers\AlertsTransformer;
use App\Http\Requests\AlertRequests\CreateAlertRequest;
use App\Http\Requests\AlertRequests\FlagOrClearAlertsRequest;

/**
 * Class AlertsController
 * @package App\Http\Controllers\Api
 */
class AlertsController extends Controller
{

    /**
     * @var AlertTransformer
     */
    protected $transformer;

    /**
     * @var AlertService
     */
    protected $service;

    /**
     * AlertsController constructor.
     * @param AlertService $service
     * @param AlertTransformer $transformer
     */
    public function __construct(AlertService $service, AlertsTransformer $transformer)
    {
        $this->service = $service;
        $this->transformer = $transformer;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $alerts = Alert::with('patient', 'recording.recording_session', 'recording.clip', 'event')
                        ->where('reviewed_at', null)
                        ->orderBy('id', 'desc')
                        ->get();
        if($request->has('filter')) {
            if(is_array($request['filter'])) {
                $alerts = $alerts->whereIn('alert_type',$request['filter']);
            } else {
                $alerts = $alerts->where('alert_type', $request['filter']);
            }
        }
        return response()->json(
            [
                'data' => $this->transformer->transformOutput($alerts->groupBy(function($item, $key) use ($alerts){
                    return $item->patient->name_first . ' ' . $item->patient->name_last;
                }, false
                )),
                'red_alerts' => $alerts->where('alert_type', 'red')->count(),
                'yellow_alerts' => $alerts->where('alert_type', 'yellow')->count()
            ]);
    }

    /**
     * Get flagged alerts by a patient id
     * @param $patient_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFlaggedAlertsByPatient($patient_id = null)
    {
        if(!$patient_id) {
            $alerts = Alert::with('patient', 'recording.clip', 'event')
                ->where('reviewed_at', null)
                ->orderBy('id', 'desc')
                ->get();
        } else {
            $alerts = Alert::with('patient', 'recording.clip', 'event')
                ->where('patient_id', $patient_id)
                ->where('reviewed_at', null)
                ->orderBy('id', 'desc')
                ->get();
        }

        if($alerts->count() == 0) {
            return response()->json(['error' => 'No recordings have been flagged!'], 422);
        }
        return response()->json($this->transformer->transformTransmissionReportPreview($alerts));
    }

    /**
     * Clear an array of alerts
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function clearAlerts(FlagOrClearAlertsRequest $request)
    {
        $ids = [];
        $result = $this->service->updateExistingAlert($request->alerts, 'clear');
        if($result)
            array_push($ids, $result['id']);
        return response()->json($ids);
    }

    /**
     * Flag an array of alerts
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function flagAlerts(FlagOrClearAlertsRequest $request)
    {
        $ids = [];
        $result = $this->service->updateExistingAlert($request->alerts, 'flag');
        if($result)
            array_push($ids, $result['id']);
        return response()->json($ids);
    }

    /**
     * Store a new alert
     * @param CreateAlertRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeAlert(CreateAlertRequest $request)
    {
        $recording = Recording::with('events', 'recording_session', 'patient')->findOrFail($request->recording_id);
        $alert = $recording->alerts()->create([
                'recording_id' => $recording->id,
                'event_id' => $recording->events->id,
                'patient_id' => $recording->recording_session->patient->id,
                'flagged_at' => Carbon::now(),
                'flagged_by' => \Auth::guard('api')->user()->id,
                'alert_type' => 'red',
                'significant_event' => NoteCategory::find($request->category_id)->description
            ]
        );
        dispatch(new RenderReportGraph($alert->recording));
        return response()->json($alert);
    }

}
