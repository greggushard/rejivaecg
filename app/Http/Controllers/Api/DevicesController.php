<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\Device;
use App\Models\Timezone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Devices\SortDeviceService;
use App\Services\Devices\CreateOrUpdateDeviceService;
use App\Http\Requests\DeviceRequests\CreateOrUpdateDeviceRequest;

class DevicesController extends Controller
{

    /**
     * @var SortDeviceService
     */
    protected $service;

    /**
     * DevicesController constructor.
     * @param SortDeviceService $service
     */
    public function __construct(SortDeviceService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = request();

        if(request()->has('sort')) {
            $devices = $this->service->sortDevices($request->sort);
        } else {
            $devices = $this->service->getDevices();
        }

        if($request->exists('filter')) {
            $devices = $this->service->filterDevices($request);
        }

        return response()->json($devices);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOrUpdateDeviceRequest $request)
    {
        $response = (new CreateOrUpdateDeviceService(new Device))->createDevice($request->all());
        return response()->json(['data' => $response]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $device = Device::with('network')->find($id);
        return response()->json($device);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateOrUpdateDeviceRequest $request, $id)
    {
        $response = (new CreateOrUpdateDeviceService(Device::find($id)))->updateDevice($id, $request->all());
        return response()->json(['data' => $response]);
    }

    /**
     * @param CreateOrUpdateDeviceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function disableDeviceByRadioId(CreateOrUpdateDeviceRequest $request)
    {
        $device = Device::where('radio_id', $request->radio_id)->whereHas('networks', function($q) use ($request) {
            $q->where('networks.id', $request->network_id);
        })->get()->first();
        if(!empty($device)) {
            $device->enabled = 0;
            $device->save();
            return response()->json(['data' => 'Disabled ' . $device->first()->radio_id]);
        } else {
            return response()->json(['error' => 'Device not found']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Device::destroy($id);
        return response()->json($result);
    }

    /**
     * Get the patient assigned to a device
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCurrentPatient($id)
    {
        $device_id = $id;
        $patients = Device::where('device_id', $device_id)->whereHas('patients', function($query) {
                        $query->where('deactivated_at', null);
                    })->with('patients')->get();
        return response()->json($patients);
    }

    /**
     * Get the current recording session details given a device ID
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCurrentSession($id, Request $request)
    {
        // If the timezone is not set set a default timezone to -5 (EST)
        if(!$request['timezone'])
            $timezone = '-5';
        else
            $timezone = $request['timezone'];

        $device_id = $id;
        $device = Device::where('device_id', $device_id)->with(['recording_sessions' => function($query){
                                    $query->where('date_end', '>', Carbon::now());
                            }])->get()->first();
        if(!is_null($device)) {
            if($device->recording_sessions->count() > 0) {
                $timezone = Timezone::where('utc', $timezone)
                    ->get()
                    ->first();
                $now = Carbon::now()->setTimezone($timezone->timezone_key);
                $start = $device->recording_sessions->first()->date_start->setTimezone($timezone->timezone_key);
                $end = $device->recording_sessions->first()->date_end->setTimezone($timezone->timezone_key);
                $total_days = $start->diffInDays($end);
                $cur_day = $start->diffInDays($now);
                return response()->json(['days_count' => 'Day ' . $cur_day .' of ' .  $total_days]);
            } else {
                return response()->json(['error' => 'This device has no active recording session']);
            }
        } else {
            return response()->json(['error' => 'This device was not found.']);
        }
    }

}
