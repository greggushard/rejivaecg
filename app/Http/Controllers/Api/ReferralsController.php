<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Referrals\ReferralService;
use App\Http\Requests\ReferralRequests\CreateOrUpdateReferralRequest;

/**
 * Class ReferralsController
 * @package App\Http\Controllers\Api
 */
class ReferralsController extends Controller
{

    /**
     * @var ReferralService
     */
    protected $service;

    /**
     * ReferralsController constructor.
     * @param ReferralService $service
     */
    public function __construct(ReferralService $service)
    {
        $this->service = $service;
    }

    /**
     * Get referrals scoped to the logged in user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $referrals = $this->service->getUserReferrals();
        return response()->json($referrals);
    }

    /**
     * Store a referral in the database
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateOrUpdateReferralRequest $request)
    {
        $this->service->storeNewReferral($request->all());
        return response()->json(['message' => 'Your referral has been submitted.']);
    }

    /**
     * Update a referral in the database
     *
     * @param CreateOrUpdateReferralRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CreateOrUpdateReferralRequest $request, $id)
    {
        $referral = $this->service->updateExistingReferral($id, $request->all());
        return response()->json($referral);
    }

    /**
     * Clear an existing referral in the database
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $referral = $this->service->clearExistingReferral($id);
        return response()->json($referral);
    }

}
