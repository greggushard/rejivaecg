<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Remeasurements\RemeasurementService;
use App\Http\Requests\RemeasurementRequests\CreateOrUpdateRemeasurementRequest;

/**
 * Class RemasurementsController
 * @package App\Http\Controllers\Api
 */
class RemeasurementsController extends Controller
{

    /**
     * @var RemeasurementService
     */
    protected $service;

    /**
     * RemasurementsController constructor.
     * @param RemeasurementService $service
     */
    public function __construct(RemeasurementService $service)
    {
        $this->service = $service;
    }

    /**
     * Retrieve a list of remeasurements by user id
     */
    public function index($user_id)
    {
        #TODO
    }

    /**
     * Create or update a measurement request
     *
     * @param CreateOrUpdateRemeasurementRequest $request
     */
    public function store(CreateOrUpdateRemeasurementRequest $request)
    {
        $remeasurement = $this->service->createRemeasurement($request->all());
        return response()->json($remeasurement);
    }

    /**
     * Update the values for an existing remeasurement
     *
     * @param CreateOrUpdateRemeasurementRequest $request
     * @param $id
     */
    public function update(CreateOrUpdateRemeasurementRequest $request, $id)
    {
        $remeasurement = $this->service->updateRemeasurement($id, $request->all());
        return response()->json($remeasurement);
    }

    /**
     * Return a list of remeasreuments by recording id
     *
     * @param $recording_id
     * @param Request $request
     */
    public function listRemeasurementsByRecording($recording_id, Request $request)
    {
        #TODO
    }

}
