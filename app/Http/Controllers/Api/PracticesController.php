<?php

namespace App\Http\Controllers\Api;

use App\Models\Practice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Practices\SortPracticeService;
use App\Services\Practices\CreatePracticeService;
use App\Services\Practices\UpdatePracticeService;
use App\Http\Requests\PracticeRequests\PracticeCreateRequest;

class PracticesController extends Controller
{

    /**
     * @var SortPracticeService
     */
    protected $service;

    /**
     * PracticesController constructor.
     * @param SortPracticeService $service
     */
    public function __construct(SortPracticeService $service)
    {

        $this->service = $service;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = request();

        if(request()->has('sort')) {
            $practices = $this->service->sortPractices($request->sort);
        } else {
            $practices = $this->service->getPractices();
        }

        if($request->exists('filter')) {
            $practices = $this->service->filterPractices($request);
        }

        return response()->json($practices);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PracticeCreateRequest $request)
    {
        $response = (new CreatePracticeService(new Practice()))->storePractice($request->all());
        return response()->json(['data' => $response]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        response()->json(Practice::with('network', 'users')->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = (new UpdatePracticeService(Practice::find($id)))->updatePractice($request->all());
        return response()->json(['data' => $response]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(Practice::destroy($id));
    }

    /**
     * Return a list of staff in the practice
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStaff(Request $request, $id)
    {
        $practice = Practice::with('users.roles')->find($id);

        if($request->has('lists'))
            return response()->json($practice->users->pluck('id'));


        return response()->json($practice->users);
    }
}
