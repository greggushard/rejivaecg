<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\NotificationService;
use App\Transformers\NotificationTransformer;

/**
 * Class NotificationsController
 * @package App\Http\Controllers\Api
 */
class NotificationsController extends Controller
{


    /**
     * @var NotificationService
     */
    protected $service;

    /**
     * @var NotificationTransformer
     */
    protected $transformer;

    /**
     * NotificationsController constructor.
     * @param NotificationService $service
     */
    public function __construct(NotificationService $service, NotificationTransformer $transformer)
    {
        $this->service = $service;

        $this->transformer = $transformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $user_id)
    {
        if($request->viewed) {
            $notifications = $this->service->getLatestNotificationsForUser($user_id);
        } else {
            $notifications = $this->service->getNewNotificationsForUser($user_id);
        }

        return response()->json(['data' =>
                                            $this->transformer
                                                 ->transformOutput($notifications)
                                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id, $id)
    {
        $notification = $this->service->markNotificationAsReadForUser($id, $user_id);
        return response()->json(['data' => $notification]);
    }

    /**
     * Mark a batch of notifications as read
     *
     * @param Request $request
     */
    public function markAllNotificationsAsRead(Request $request, $user_id)
    {
        $notifications = $this->service->markNotificationAsReadForUser($request['notifications'], $user_id);
        return response()->json(['data' => $notifications]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
