<?php

namespace App\Http\Requests\RecordingRequests;

use Illuminate\Foundation\Http\FormRequest;

use App\PatientRecording;

class CreateRecordingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
            'phone_id'      => 'required',
            'device_id'     => 'required',
            'raw_ecg_data'  => 'required',
            'sample_rate'   => 'required',
            'body_position' => 'required',
            'battery_level' => 'required',
            'timestamp'     => 'required',
            'breathing_rate'=> 'required',
            'heart_rate'    => 'required',
        ];
    }

}