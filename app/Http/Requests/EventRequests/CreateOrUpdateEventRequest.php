<?php

namespace App\Http\Requests\EventRequests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateOrUpdateEventRequest
 * @package App\Http\Requests\EventRequests
 */
class CreateOrUpdateEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_id' =>  'string|required',
			'device_id' => 'int|required',
			'event_type' => 'int|required',
            'heart_rate' =>  'required',
            'body_position' => 'string|required',
            'battery_level' =>  'int|required',
            'breathing_rate' => 'int|required',
            'timestamp' => 'required'
        ];
    }
}
