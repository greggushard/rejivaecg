<?php

namespace App\Http\Requests\FirmwareRequests;

use Illuminate\Foundation\Http\FormRequest;

class GetCurrentFirmwareVersionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'device_id' => 'required'
        ];
    }
}
