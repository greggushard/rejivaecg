<?php

namespace App\Http\Requests\InterpretationRequests;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrUpdateInterpretationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'recording_id' => 'int|required',
            'recording_session_id' => 'int|required',
            'body_content' => 'string|required'
        ];
    }
}
