<?php

namespace App\Http\Requests\ReferralRequests;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrUpdateReferralRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'physician_id'  => 'required|int',
            'priority'      => 'required|string',
            'recording_id'  => 'required|int',
            'cleared_at'    => 'boolean',
            'note'          => 'string'
        ];
    }
}
