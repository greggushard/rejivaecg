<?php

namespace App\Http\Requests\RemeasurementRequests;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrUpdateRemeasurementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'measurement_start'     => 'required',
            'measurement_end'       => 'required',
            'measurement_length'    => 'required',
            'measurement_type'      => 'required',
            'measurement_points'    => 'required',
            'recording_id'          => 'required',
        ];
    }
}
