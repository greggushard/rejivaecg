<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePatientRequest extends FormRequest
{
    /**
     * Determine if the patient is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_first'                => 'required',
            'name_last'                 => 'required',
            'dob'                       => 'required',
            'address_country'           => 'required',
            'address_postal_code'       => 'required',
            'address_province'          => 'required',
            'address_city'              => 'required',
            'address_street1'           => 'required',
            'health_card_expiration'    => 'required',
            'health_card_number'        => 'required',
            'gender'                    => 'required',
            'phone1'                    => 'required',
            'phone1_type'               => 'required',
            'network_id'                => 'required'
        ];
    }

}
