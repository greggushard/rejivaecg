<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class ForceSecure
{

    public function handle(Request $request, Closure $next)
    {
        // WIPE for prod test
        return $next($request);
    }
}