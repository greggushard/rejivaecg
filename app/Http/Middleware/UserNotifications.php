<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\NotificationService;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserNotifications
 * @package App\Http\Middleware
 */
class UserNotifications
{

    /**
     * @var NotificationService
     */
    protected $service;

    /**
     * UserNotifications constructor.
     * @param NotificationService $service
     */
    public function __construct(NotificationService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if(Auth::user()) {
            if ($response->headers->get('content-type') == 'application/json') {
                $notifications = $this->service->getNewNotificationsForUser(Auth::user()->id);
                $response->header(
                    'User-Notifications',
                    json_encode([
                        'data' => $notifications
                    ])
                );
            }
        }

        return $response;
    }
}
