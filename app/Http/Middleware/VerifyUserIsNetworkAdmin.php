<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyUserIsNetworkAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->hasRole('network-admin') ||
            auth()->user()->hasRole('admin')) {
            return $next($request);
        }

        return response()->json(['data' => 'Unauthorized access.'], 403);

    }
}
