<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class VerifyAccessToPatientPortal
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->hasRole('cardiac-tech')
                || auth()->user()->hasRole('physician')
                || auth()->user()->hasRole('interpreting-cardiologist')) {
                    return $next($request);
        }

        return response()->json(['data' => 'Unauthorized access.'], 403);
    }

}
