<?php

namespace App\Http\Middleware\Api;

use Closure;
use Auth;

class VerifyUserCanRefer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (
            Auth::guard('api')->user()->isRole('network-admin')
            || Auth::guard('api')->user()->isRole('interpreting-cardiologist')
            || Auth::guard('api')->user()->isRole('cardiac-tech')
            || Auth::guard('api')->user()->isRole('physician')
        ) {
            return $next($request);
        }

        return response()->json(['data' => 'Unauthorized access.'], 403);
    }
}
