<?php

namespace App\Http\Middleware\Api;

use Closure;
use App\Models\User;

class VerifyUserHasPatient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::guard('api')->user();
        if(!$user->isRole('admin') && !$user->isRole('network-admin') && $request->route('id')) {
            # Make sure user is scoped to Network
            $result = User::where('id', $user->id)
                    ->whereHas('patients', function($q) use ($request){
                        $q->where('patients.id', $request->route('id'));
                    })->get();

            # User has network
            if($result->count() > 0) {
                return $next($request);
            }

            return $next($request);

        }

        # Return response for super admin **/
        return $next($request);
    }

}
