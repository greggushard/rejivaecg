<?php

namespace App\Http\Middleware\Api;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyUserIsNetworkAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(isset(Auth::guard('api')->user()->id)) {
            if (Auth::guard('api')->user()->isRole('network-admin')
                || Auth::guard('api')->user()->hasRole('admin')) {
                return $next($request);
            }
        }

        return response()->json(['data' => 'Unauthorized access.'], 403);
    }
}
