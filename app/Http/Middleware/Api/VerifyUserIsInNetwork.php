<?php

namespace App\Http\Middleware\Api;

use Closure;
use App\Models\User;
use Auth;

class VerifyUserIsInNetwork
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        return $next($request);

    }
}
