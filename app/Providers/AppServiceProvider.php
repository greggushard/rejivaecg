<?php

namespace App\Providers;

use App\Models\Clip;
use App\Models\Note;
use App\Models\Alert;
use App\Models\Report;
use App\Models\Remeasurement;
use App\Observers\ClipObserver;
use App\Observers\NoteObserver;
use App\Observers\AlertObserver;
use App\Models\RecordingSession;
use App\Observers\ReportObserver;
use Illuminate\Support\ServiceProvider;
use App\Observers\RemeasurementObserver;
use App\Observers\RecordingSessionObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Note::observe(NoteObserver::class);
        Clip::observe(ClipObserver::class);
        Alert::observe(AlertObserver::class);
        Remeasurement::observe(RemeasurementObserver::class);
        RecordingSession::observe(RecordingSessionObserver::class);
        Report::observe((ReportObserver::class));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
