<?php

namespace App\Jobs;

use Monolog\Logger;
use App\Models\Recording;
use Illuminate\Bus\Queueable;
use Monolog\Handler\StreamHandler;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RenderReportGraph implements ShouldQueue
{
    use InteractsWithQueue, Queueable;

    protected $log;
    protected $recording;

    const ECG_SAMPLE_RATE = 250;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($recording)
    {
        $this->recording = $recording;
        $this->log = new Logger('ECGGraphLog');
        $this->log->pushHandler(new StreamHandler(storage_path().'/logs/ECGGraphs.log', Logger::INFO));
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->log->addInfo("Generating report graphics... for " . $this->recording['recording_hash'] );
        $json = Storage::get('ecg_graphs_json/' . $this->recording['recording_hash'] . '.json');
        $new_json = str_replace('sourceWidth:1200,sourceHeight:180', 'sourceWidth:4586,sourceHeight:333', $json);
        $new_json = str_replace('left:\'20px\',top:\'155px\'', 'left:\'5px\',top:\'5px\'', $new_json);
        $new_json = str_replace('tickInterval:0.1,labels:{enabled:false}', 'visible:false,startOnTick:false,endOnTick:false,tickPixelInterval:25,labels:{enabled:true,formatter: function() { var label = this.value * 4/1000; if(label % 1 == 0) { return label + \'s\'; } else { return \'\' } }}', $new_json);
        $new_json = str_replace('yAxis:{', 'yAxis:{visible:false,', $new_json);
        $decoded = json_decode($new_json, true);
        $jsonToGraph = [];
        $jsonToGraph['infile'] = $decoded['infile'] . ';';
        //if($this->recording['remeasurements']->count() > 0) {
            $remeasurementStrings = $this->buildRemeasurements($this->recording['remeasurements']);
            $jsonToGraph["callback"] = 'function(chart) { ' . $remeasurementStrings . ' }';
       // }
        $newJson = json_encode($jsonToGraph);
        Storage::put('ecg_graphs_json/' . $this->recording['recording_hash']  . '-lg.json', $newJson);
        $process = new Process('curl http://127.0.0.1:3003 -H "Content-Type: application/json" -X POST --data-binary "@'.env('PHANTOMJS_DIR').'storage/app/ecg_graphs_json/'.$this->recording['recording_hash'] .'-lg.json"');
        $process->run();
        if (!$process->isSuccessful()) {
            $this->log->addError($process->getErrorOutput());
        } else {
            Storage::put('ecg_graphs_reports/' . $this->recording['recording_hash'] . '.png', base64_decode($process->getOutput()));
        }
    }

    /**
     * @param $remeasurements
     */
    protected function buildRemeasurements($remeasurements)
    {
        $functionString = '';
        $functionString .= 'chart.renderer.image(\'http://dev.rejiva.com/img/chart-fill-report.svg\', 0, 0, \'4586\', \'333\').css({zIndex: 1}).add();';
        foreach ($remeasurements as $remeasurement) {
            if ($remeasurement->measurement_type == 'qt') {
                $color = '#505fe5';
                $topOffset = 210;
                $verticalLine = $topOffset - 80;
            } else if ($remeasurement->measurement_type == 'pr') {
                $color = '#f78922';
                $topOffset = 135;
                $verticalLine = $topOffset + 95;
            } else if ($remeasurement->measurement_type == 'qrs') {
                $color = '#ef563b';
                $topOffset = 100;
                $verticalLine = $topOffset + 135;
            } else if ($remeasurement->measurement_type == 'rr') {
                $color = '#c542f4';
                $topOffset = 275;
                $verticalLine = $topOffset - 100;
            }
            $startPoint = $remeasurement->measurement_start;
            $endPoint = $remeasurement->measurement_end;
            $textStart = $startPoint - 15;
            $textOffset = $topOffset - 25;
            $functionString .= 'chart.renderer.path([\'M\', chart.xAxis[0].toPixels(' . $startPoint/4 . '), ' . $topOffset . ', \'V\', ' . $verticalLine . ']).attr({\'stroke-width\': 2,stroke: \'#ccc\',zIndex: 8,dashstyle: \'dash\'}).add();';
            $functionString .= 'chart.renderer.path([\'M\', chart.xAxis[0].toPixels(' . $endPoint/4 . '), '.$topOffset.', \'V\', '.$topOffset.', '.$verticalLine.']).attr({\'stroke-width\': 2,stroke: \'#ccc\',zIndex: 8,dashstyle: \'dash\'}).add();';
            $functionString .= 'chart.renderer.path([\'M\', chart.xAxis[0].toPixels('. $startPoint/4 .'), '.$topOffset.', \'L\', chart.xAxis[0].toPixels(' . $endPoint/4 . '), '.$topOffset.']).attr({fill: \''. $color . '\',\'stroke-width\': 4,stroke: \''. $color . '\',zIndex: 6}).add();';
            $functionString .= 'chart.renderer.text(\'' .strtoupper($remeasurement->measurement_type). ' - ' .$remeasurement->measurement_length. 'ms\',chart.xAxis[0].toPixels(' . $textStart/3.898 . '),(' . $textOffset . ')).attr({zIndex:6}).add();';
        }
        return $functionString;
    }


}
