<?php

namespace App\Jobs;

use Storage;
use Monolog\Logger;
use App\Models\Recording;
use Illuminate\Bus\Queueable;
use League\Flysystem\Exception;
use Monolog\Handler\StreamHandler;
use Illuminate\Queue\SerializesModels;
use Symfony\Component\Process\Process;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RenderJpgraph implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Recording
     */
    public $recording;

    /**
     * @var
     */
    public $rawData;

    /**
     * @var Logger
     */
    public $log;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Recording $recording)
    {
        $this->log = new Logger('ECGGraphLog');
        $this->log->pushHandler(new StreamHandler(storage_path().'/logs/ECGGraphsSmall.log', Logger::INFO));
        $this->recording = $recording;
        $this->rawData = Storage::disk('local')->get('ecg_normalized_shifted/'.$this->recording->recording_hash.'.gph');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $process = new Process('php /var/www/html/vhosts/dev.rejiva.com/exporter/ecg_process.php ' . $this->rawData . ' ' . $this->recording->recording_hash);
        try {
            $process->run();
            $this->log->addInfo("Rendered graph image for " . $this->recording->recording_hash);
        } catch(Exception $e) {
            dd($e);
        }
    }

}