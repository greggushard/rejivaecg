<?php
namespace App\Observers;

use Auth;
use App\Models\Note;
use App\Models\ActivityLog;

class NoteObserver
{

    /**
     * @param Note $note
     */
    public function created(Note $note)
    {
        $user = Auth::guard('api')->user();
        $activity = new ActivityLog();
        $activity->user_id = $user->id;
        $activity->activity_content = $user->name_first . ' ' . $user->name_last . ' created a note';
        $activity->save();
    }

    /**
     * @param Note $note
     */
    public function updated(Note $note)
    {
        $user = Auth::guard('api')->user();
        $activity = new ActivityLog();
        $activity->user_id = $user->id;
        $activity->activity_content = $user->name_first . ' ' . $user->name_last . ' created a note';
        $activity->save();
    }

    /**
     * @param Note $note
     */
    public function deleted(Note $note)
    {
        $user = Auth::guard('api')->user();
        $activity = new ActivityLog();
        $activity->user_id = $user->id;
        $activity->activity_content = $user->name_first . ' ' . $user->name_last . ' created a note';
        $activity->save();
    }

}