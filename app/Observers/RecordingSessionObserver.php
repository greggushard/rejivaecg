<?php
namespace App\Observers;

use Auth;
use App\Models\ActivityLog;
use App\Models\EcgEventsCount;
use App\Models\RecordingSession;

class RecordingSessionObserver
{

    /**
     * Create an empty events count record associated with the recording session
     * @param RecordingSession $recordingSession
     */
    public function created(RecordingSession $recordingSession)
    {
        EcgEventsCount::create([
            'recording_session_id' => $recordingSession->id
        ]);

        $user = Auth::guard('api')->user();
        $activity = new ActivityLog();
        $activity->user_id = $user->id;
        $activity->activity_content = $user->name_first . ' ' . $user->name_last . ' started a recording session for ' . $recordingSession->patient->name_first . ' ' . $recordingSession->patient->name_last;
        $activity->save();
    }

    /**
     * When a recording session is updated, log the activity
     * @param RecordingSession $recordingSession
     */
    public function updated(RecordingSession $recordingSession)
    {
        if(!is_null($recordingSession->is_archived)) {
            $user = Auth::guard('api')->user();
            $activity = new ActivityLog();
            $activity->user_id = $user->id;
            $activity->activity_content = $user->name_first . ' ' . $user->name_last . ' archived the active recording session for ' . $recordingSession->patient->name_first . ' ' . $recordingSession->patient->name_last;
        } else {
            $user = Auth::guard('api')->user();
            $activity = new ActivityLog();
            $activity->user_id = $user->id;
            $activity->activity_content = $user->name_first . ' ' . $user->name_last . ' updated the active recording session for ' . $recordingSession->patient->name_first . ' ' . $recordingSession->patient->name_last;
        }
    }

    /**
     * When a recording session is deleted, log the activity
     * @param RecordingSession $recordingSession
     */
    public function deleted(RecordingSession $recordingSession)
    {
        $user = Auth::guard('api')->user();
        $activity = new ActivityLog();
        $activity->user_id = $user->id;
        $activity->activity_content = $user->name_first . ' ' . $user->name_last . ' deleted a recording session for ' . $recordingSession->patient->name_first . ' ' . $recordingSession->patient->name_last;

    }

}