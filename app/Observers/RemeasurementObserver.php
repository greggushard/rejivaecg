<?php
namespace App\Observers;

use Auth;
use App\Models\ActivityLog;
use App\Models\Remeasurement;

class RemeasurementObserver
{

    /**
     * @param Remeasurement $remeasurement
     */
    public function created(Remeasurement $remeasurement)
    {
        $user = Auth::guard('api')->user();
        $activity = new ActivityLog();
        $activity->user_id = $user->id;
        $activity->activity_content = $user->name_first . ' ' . $user->name_last . ' remeasured an ' . $remeasurement->measurement_type . ' interval on a recording for ' . $remeasurement->recording->patient->name_first . ' ' . $remeasurement->recording->patient->name_last;
        $activity->save();
    }

    /**
     * @param Remeasurement $remeasurement
     */
    public function updated(Remeasurement $remeasurement)
    {
        $user = Auth::guard('api')->user();
        $activity = new ActivityLog();
        $activity->user_id = $user->id;
        $activity->activity_content = $user->name_first . ' ' . $user->name_last . ' updated a remeasurement for an ' . $remeasurement->measurement_type . ' interval on a recording for ' . $remeasurement->recording->patient->name_first . ' ' . $remeasurement->recording->patient->name_last;
        $activity->save();
    }

    /**
     * @param Remeasurement $remeasurement
     */
    public function deleted(Remeasurement $remeasurement)
    {
        $user = Auth::guard('api')->user();
        $activity = new ActivityLog();
        $activity->user_id = $user->id;
        $activity->activity_content = $user->name_first . ' ' . $user->name_last . ' deleted a remeasurement for an ' . $remeasurement->measurement_type . ' interval on a recording for ' . $remeasurement->recording->patient->name_first . ' ' . $remeasurement->recording->patient->name_last;
        $activity->save();
    }

}