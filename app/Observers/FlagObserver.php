<?php
namespace App\Observers;

use Auth;
use App\Models\Alert;
use App\Models\ActivityLog;

class AlertObserver
{

    /**
     * @param Alert $alert
     */
    public function created(Alert $alert)
    {
        if($alert->flagged_at != null) {
            $user = Auth::guard('api')->user();
            $activity = new ActivityLog();
            $activity->user_id = $user->id;
            $activity->activity_content = $user->name_first . ' ' . $user->name_last . ' flagged an alerted recording';
            $activity->save();
        }
    }

    /**
     * @param Alert $alert
     */
    public function updated(Alert $alert)
    {
        if($alert->flagged_at != null) {
            $user = Auth::guard('api')->user();
            $activity = new ActivityLog();
            $activity->user_id = $user->id;
            $activity->activity_content = $user->name_first . ' ' . $user->name_last . ' flagged an alerted recording';
            $activity->save();
        }
    }

}