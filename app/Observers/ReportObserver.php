<?php
namespace App\Observers;

use Auth;
use Carbon\Carbon;
use App\Models\Report;
use App\Models\ActivityLog;

/**
 * Class ReportObserver
 * @package App\Observers
 */
class ReportObserver
{

    /**
     * @param Report $report
     */
    public function created(Report $report)
    {
        $report_type = '';
        $report_class = '';

        if($report->recording_session->date_end > Carbon::now()) {
            $report_class = 'Preliminary';
        } else {
            $report_class = 'Final';
        }

        if($report->report_type == 'holter-summary') {
            $report_type = $report_class . " Holter";
        } elseif($report->report_type == 'patient-transmission') {
            $report_type = $report_class. " Patient";
        } elseif($report->report_type == 'full-disclosure') {
            $report_type = 'Full Disclosure';
        }
        $user = Auth::guard('api')->user();
        $activity = new ActivityLog();
        $activity->user_id = $user->id;
        $activity->activity_content = $user->name_first . ' ' . $user->name_last . ' generated a ' . $report_type . ' report for ' . $report->patient->name_first . ' ' . $report->patient->name_last ;
        $activity->save();
    }

}