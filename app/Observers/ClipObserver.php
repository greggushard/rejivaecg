<?php
namespace App\Observers;

use Auth;
use App\Models\Clip;
use App\Models\ActivityLog;

class ClipObserver
{

    public function created(Clip $clip)
    {
        $user = Auth::guard('api')->user();
        $activity = new ActivityLog();
        $activity->user_id = $user->id;
        $activity->activity_content = $user->name_first . ' ' . $user->name_last . ' clipped a recording';
        $activity->save();
    }

}