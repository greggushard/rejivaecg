<?php namespace App\Scopes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Scope;

/**
 * Class PatientScope
 * @package App\Scopes
 */
class PatientScope implements Scope {

    /**
     * Apply the scope based on the users' role
     *
     * @param Builder $builder
     * @param Model $model
     * @return Builder
     */
    public function apply(Builder $builder, Model $model)
    {

        if(\Auth::guard('api')->user()) {
            if (\Auth::guard('api')->user()->hasRole('network-admin')) {
                $this->applyNetworkStaffScope($builder, \Auth::guard('api')->user());
            }
            elseif ( \Auth::guard('api')->user()->hasRole('cardiac-tech')) {
                $this->applyNetworkStaffScope($builder, \Auth::guard('api')->user());
            }
            elseif(\Auth::guard('api')->user()->hasRole('physician')) {
                $this->applyAnalysisScope($builder, \Auth::guard('api')->user());
            }
        } else {
            if (auth()->user()->hasRole('network-admin')
                || auth()->user()->hasRole('cardiac-tech')) {
                $this->applyNetworkStaffScope($builder, auth()->user());
            }
            elseif ( auth()->user()->hasRole('physician')) {
                $this->applyAnalysisScope($builder, auth()->user());
            }
        }

        return $builder;
    }

    /**
     * Apply the network administrator scope
     *
     * @param Builder $builder
     * @return Builder
     */
    private function applyNetworkStaffScope(Builder $builder, $user)
    {
        $builder->where('patients.network_id', $user->networks()->first()->id);
        return $builder;
    }

    /**
     * Apply the analysis patient scope
     *
     * @param Builder $builder
     * @return Builder
     */
    private function applyAnalysisScope(Builder $builder, $user)
    {
        $builder->whereHas('users', function($builder) use($user) {
            $builder->where('user_id', $user->id);
        });
        return $builder;
    }

    /**
     * @param Builder $builder
     * @param Model $model
     */
    public function remove(Builder $builder, Model $model)
    {
        # Address this when we need to remove query scopes.
    }

}
