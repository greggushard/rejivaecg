<?php namespace App\Scopes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Scope;

/**
 * Class AlertScope
 * @package App\Scopes
 */
class AlertScope implements Scope {

    /**
     * Apply the scope based on the users' role
     *
     * @param Builder $builder
     * @param Model $model
     * @return Builder
     */
    public function apply(Builder $builder, Model $model)
    {

        if (
            auth()->user()->hasRole('network-admin')
            ||
            auth()->user()->hasRole('cardiac-tech')) {
            $this->applyNetworkAdminScope($builder);
        }
        elseif (auth()->user()->hasRole('cardiologist')
            || auth()->user()->hasRole('physician')) {
            $this->applyAnalysisScope($builder);
        }

        return $builder;
    }

    /**
     * Apply the network administrator scope
     *
     * @param Builder $builder
     * @return Builder
     */
    private function applyNetworkAdminScope(Builder $builder)
    {
        if(auth()->user()) {
            $user = auth()->user();
        } elseif(\Auth::guard('api')->user()) {
            $user = \Auth::guard('api')->user();
        }
        $builder->whereHas('patient', function($q) use($user) {
            $q->where('patients.network_id', $user->networks()->first()->id);
        });
        return $builder;
    }

    /**
     * Apply the analysis patient scope
     *
     * @param Builder $builder
     * @return Builder
     */
    private function applyAnalysisScope(Builder $builder)
    {
        $builder->whereHas('patient', function($builder) {
            $builder->whereHas('users', function($builder) {
                $builder->where('user_id', auth()->user()->id);
            });
        });
        return $builder;
    }

    /**
     * @param Builder $builder
     * @param Model $model
     */
    public function remove(Builder $builder, Model $model)
    {
        # Address this when we need to remove query scopes.
    }

}
