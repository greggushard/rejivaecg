<?php namespace App\Scopes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Scope;

/**
 * Class ReferralScope
 * @package App\Scopes
 */
class ReferralScope implements Scope {

    /**
     * Apply the users scope
     *
     * @param Builder $builder
     * @param Model $model
     * @return Builder
     */
    public function apply(Builder $builder, Model $model)
    {
        $this->applyUsersScope($builder);
        return $builder;
    }

    /**
     * Apply the users scope
     *
     * @param Builder $builder
     * @return Builder
     */
    private function applyUsersScope(Builder $builder)
    {
        $builder->whereHas('users', \Auth::guard('api')->user()->id);
        return $builder;
    }

    /**
     * @param Builder $builder
     * @param Model $model
     */
    public function remove(Builder $builder, Model $model)
    {
        # Address this when we need to remove query scopes.
    }

}
