<?php

return [
    ['id' => 1, 'name' => 'Ontario'],
    ['id' => 2, 'name' => 'British Columbia'],
    ['id' => 3, 'name' => 'Quebec'],
    ['id' => 4, 'name' => 'Alberta'],
    ['id' => 5, 'name' => 'Saskatchewan'],
    ['id' => 6, 'name' => 'Nova Soctia'],
    ['id' => 7, 'name' => 'Newfoundland'],
    ['id' => 8, 'name' => 'Manitoba'],
    ['id' => 9, 'name' => 'Prince Edward Island'],
    ['id' => 10, 'name' => 'New Brunswick']
];