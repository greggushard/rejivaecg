<?php

return [

    'bad_events' => [
        400 => 'Noise',
        401 => 'Unclassified rhythm',
        402 => 'UNCLASSIFIED BEATS',
    ],
    'normal_events' => [
        200 => 'NSR',
        201 => 'SinBRADY',
        202 => 'SinTACHY',
        203 => 'NSR+IVCD',
        204 => 'SinBrady+IVCD',
        205 => 'SinTachy+IVCD'
    ],
    'arythmias' => [
        300 => 'PAUSE',
        301 => 'SVC',
        302 => 'JuncTACHY',
        303 => '1degr.AVBlock+NSR',
        304 => '1degr.AVBlock+SinTACHY',
        305 => '1degr.AVBlock+SinBRADY',
        306 => 'Mobitz I',
        307 => 'Mobitz II',
        308 => 'PAC',
        309 => 'SVTA',
        310 => 'AFib slow',
        311 => 'AFib normal',
        312 => 'AFib rapid',
        313 => 'PVC',
        314 => 'VCoup',
        315 => 'VTrip',
        316 => 'VBig',
        317 => 'VTrig',
        318 => 'IVR',
        319 => 'VT',
        320 => 'Slow VT',
        321 => 'VF'
    ],
    'patient_events' => [
        1 => [
                'id' => 1,
                'description' => 'Light Headed / dizzy',
                'icon' => 'icon-rejiva_patient_event_dizzy'
             ],

        2 => [
                'id' => 2,
                'description' => 'Shortness of breath',
                'icon' => 'icon-rejiva_patient_event_shortbreath'
             ],

        3 => [
                'id' => 3,
                'description' => 'Fatigue / weakness',
                'icon' => 'icon-rejiva_patient_event_fatigue'
             ],
        4 => [
                'id' => 4,
                'description' => 'Chest pain',
                'icon' => 'icon-rejiva_patient_event_chestpain'
             ],
        5 => [
                'id' => 5,
                'description' => 'Heart palpitations / skipping a beat',
                'icon' => 'icon-rejiva_patient_event_skipbeat'
             ],
        6 => [
                'id' => 6,
                'description' => 'Audo submission',
                'icon' => 'icon-rejiva_patient_event_recording'
             ],
        9 => [
            'id' => 9,
            'description' => 'Fall Detected',
            'icon' => 'icon-rejiva_patient_event_fatigue'
        ],
        101 => [
                'id' => 101,
                'description' => 'Start baseline mode',
                'icon' => 'icon-rejiva_patient_event_fatigue'
            ],
        102 => [
            'id' => 102,
            'description' => 'End baseline mode',
            'icon' => 'icon-rejiva_patient_event_fatigue'
        ],
        111 => [
                'id' => 111,
                'description' => 'Audio only event',
                'icon' => 'icon-rejiva_patient_event_recording'
                ]
    ]
];



/**
 * Additional events from Monebo Algorithm Documentation
 */

// Good Events
#SINUS RHYTHM	                                NSR	                    all
#SINUS BRADYCARDIA	                            SinBRADY	            all
#SINUS TACHYCARDIA	                            SinTACHY	            all
#NSR + IVCD	                                    NSR+IVCD	            all
#SINUS BRADYCARDIA + IVCD	                    SinBrady+IVCD	        all
#SINUS TACHYCARDIA + IVCD	                    SinTachy+IVCD	        all

// Arythmias
#PREMATURE SUPRAVENTRICULAR CONTRACTION	        SVC	                    No measurements
#JUNCTIONAL TACHYCARDIA	                        JuncTACHY	            HR only
#FIRST DEGREE HEART BLOCK + SINUS RHYTHM	    1degr.AVBlock+NSR	    all
#FIRST DEGREE HEART BLOCK + SINUS TACHYCARDIA	1degr.AVBlock+SinTACHY	all
#FIRST DEGREE HEART BLOCK + SINUS BRADYCARDIA	1degr.AVBlock+SinBRADY	all
#SECOND DEGREE HEART BLOCK TYPE I	            Mobitz I	            all
#SECOND DEGREE HEART BLOCK TYPE II	            Mobitz II	            all
#PREMATURE ATRIAL CONTRACTION	                PAC	                    No measurements
#ATRIAL/SUPRAVENTRICULAR TACHYCARDIA	        SVTA	                all
#ATRIAL FIBRILLATION/FLUTTER SVR	            AFib slow	            No PR
#ATRIAL FIBRILLATION/FLUTTER CVR	            AFib normal	            No PR
#ATRIAL FIBRILLATION/FLUTTER RVR	            AFib rapid	            No PR
#PAUSE	                                        Pause	                No measurements
#PREMATURE VENTRICULAR CONTRACTION	            PVC             	    No measurements
#VENTRICULAR COUPLET	                        VCoup	                HR only
#VENTRICULAR TRIPLET	                        VTrip	                HR only
#VENTRICULAR BIGEMINY	                        VBig	                HR only
#VENTRICULAR TRIGEMINY	                        VTrig	                HR only
#IDIOVENTRICULAR RHYTHM	                        IVR	                    HR only
#VENTRICULAR TACHYCARDIA	                    VT	                    HR only
#SLOW VENTRICULAR TACHYCARDIA	                Slow VT	                HR only
#VENTRICULAR FLUTTER	                        VF                  	No measurements

// No
#ARTIFACT	                                    Noise	                No measurements
#UNCLASSIFIED RHYTHM	                        Unclassified rhythm	    No measurements
#UNCLASSIFIED BEATS	                            Unclassified beats.     No measurements