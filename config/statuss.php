<?php

return [
['id' => 1, 'name' => 'Active'],
['id' => 2, 'name' => 'Monitoring'],
['id' => 3, 'name' => 'Completed'],
['id' => 4, 'name' => 'Deceased'],
['id' => 5, 'name' => 'Transferred'],
['id' => 6, 'name' => 'LTF'],
['id' => 7, 'name' => 'Inactive']

];