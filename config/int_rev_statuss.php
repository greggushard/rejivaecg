<?php

return [

['id' => 1, 'name' => 'Being reviewed by Cardiac Technician'],
['id' => 2, 'name' => 'Review by Cardiac Technician Complete'],
['id' => 3, 'name' => 'Being interpreted by Interpreting Cardiologists'],
['id' => 4, 'name' => 'Interpretation by Cardiologist Complete']
];