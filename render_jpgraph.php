<?php
require_once ('/var/www/html/vhosts/dev.rejiva.com/public/jpgraph-4.0.2/src/jpgraph.php');
require_once ('/var/www/html/vhosts/dev.rejiva.com/public/jpgraph-4.0.2/src/jpgraph_line.php');
set_time_limit(60);
define("ECG_SAMPLE_RATE", 125);
$ecg_recording_hash = $argv[1];
$inputPath = "/var/www/html/vhosts/dev.rejiva.com/storage/app/raw_ecg/";
$outputPath = "/var/www/html/vhosts/dev.rejiva.com/storage/app/ecg_graphs_small/";
$ecg_raw_string = file_get_contents($inputPath.$ecg_recording_hash.'.raw');
$ecg_raw_array = explode(",", $ecg_raw_string);
$ecg_raw_min = min($ecg_raw_array);
$ecg_raw_max = max($ecg_raw_array);
$ecg_nrm_array = array();
foreach($ecg_raw_array as $ecg_raw_val) {
    $ecg_nrm_val = ($ecg_raw_val)/($ecg_raw_max);
    array_push($ecg_nrm_array,$ecg_nrm_val);
}
$sec_per_row = 31;
$ecg_array_size = count($ecg_nrm_array);
$samples_per_row = $sec_per_row * ECG_SAMPLE_RATE;
$num_of_rows = ceil($ecg_array_size/$samples_per_row);
$msPerRow = $samples_per_row*(1000/ECG_SAMPLE_RATE);
for ($x = 0; $x < $num_of_rows; $x++) {
    $array_segment_bgn = $x * $samples_per_row;
    $ecg_nrm_array_segments_array[$x] = array_slice($ecg_nrm_array, $array_segment_bgn, $samples_per_row);
    drawEcgGraph($ecg_nrm_array_segments_array[$x],$ecg_recording_hash."_part".$x.".png",$outputPath,($msPerRow*$x));
}

function drawEcgGraph ($datay,$fileName,$outputPath,$msStart) {
        echo "count(datay): ".count($datay)."\n";
        echo "msStart: ".$msStart."\n";
        $xTextLabelInterval=5;
        $yGraphHeight = 65;
        $xGraphWidth = 1875;
        $xSegmentsPerSecond=5;
        $xSamplesPerSegment=ECG_SAMPLE_RATE/$xSegmentsPerSecond;
        $xSecondsPerRow=30;
        $xSamplesPerRow=$xSecondsPerRow*ECG_SAMPLE_RATE;
        echo "xGraphWidth: ".$xGraphWidth."\n";
        echo "xSamplesPerSegment: ".$xSamplesPerSegment."\n";
        echo "xSecondsPerRow: ".$xSecondsPerRow."\n";
        echo "xSamplesPerRow: ".$xSamplesPerRow."\n";
        $graph = new Graph($xGraphWidth, $yGraphHeight);
        $graph->SetGridDepth(DEPTH_BACK);
        $graph->img->SetMargin(55,20,10,10);
        $graph->img->SetAntiAliasing();
        $graph->SetScale("linlin",0.75,1.05,0,$xSamplesPerRow);
        $graph->Box();
        $graph->SetBox(false);
        $graph->xaxis->SetFont(FF_COURIER,FS_BOLD,16);
        $graph->yaxis->SetFont(FF_COURIER,FS_BOLD,14);
        $graph->xaxis->SetTextLabelInterval($xTextLabelInterval);
        $yTickPositions = array(
            0   => 0.0,
            1   => 0.1,
            2   => 0.2,
            3   => 0.3,
            4   => 0.4,
            5   => 0.5,
            6   => 0.6,
            7   => 0.7,
            8   => 0.8,
            9   => 0.9,
            10  => 1.0,
            11  => 1.1
        );
        $yTickLabels = array(
            0   => "",
            1   => "0.1",
            2   => "0.2",
            3   => "0.3",
            4   => "0.4",
            5   => "0.5",
            6   => "0.6",
            7   => "1.7",
            8   => "0.8",
            9   => "0.9",
            10  => "1.0",
            11  => "1.1"
        );
        $graph->yaxis->SetMajTickPositions($yTickPositions,$yTickLabels);
        $graph->yaxis->scale->ticks->Set(0.1);
        $p1 = new LinePlot($datay);
        $graph->xscale->SetAutoTicks(false);
        $graph->xgrid->Show(false,false);
        $graph->ygrid->Show(false,false);
        $graph->yaxis->Hide(true,true);
        $graph->xaxis->Hide(true,true);
        $p1->SetCenter();
        $graph->Add($p1);
        $p1->SetWeight(2);
        $p1->SetColor('#076C16');
        $p1->SetStyle("solid");
        $gdImgHandler = $graph->Stroke(_IMG_HANDLER);
        $graph->img->Stream($outputPath.$fileName);
        echo "Rendered graph " . $outputPath . '/' . $fileName;
    }