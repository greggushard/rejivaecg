var app = require('https').createServer(options, handler);
var io = require('socket.io')(app);
var Redis = require('ioredis');
var redis = new Redis();

app.listen(3000, function() {
    console.log('Server is running!');
});

function handler(req, res) {
    res.writeHead(200);
    res.end('');
}
var pkey = fs.readFileSync('/var/www/html/vhosts/dev.rejiva.com/keys/privkey.pem');
var pcert = fs.readFileSync('/var/www/html/vhosts/dev.rejiva.com/keys/cert.pem');

var options = {
    key:    pkey,
    cert:   pcert,
};

io.on('connection', function(socket) {});

redis.psubscribe('*', function(err, count) {});

redis.on('pmessage', function(subscribed, channel, message) {
    message = JSON.parse(message);
    io.emit(channel + ':' + message.event, message.data);
});