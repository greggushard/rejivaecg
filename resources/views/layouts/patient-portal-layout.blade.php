<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Rejiva ECG') }}</title>

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/img/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/img/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/img/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/img/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="/img/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="/img/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="/img/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="/img/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="/img/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="CAI Live Stream"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="/img/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="/img/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="/img/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="/img/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="/img/mstile-310x310.png" />

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/patient-portal.css" rel="stylesheet">

    <!-- Remotes -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        // CSRF Token
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>

        <?php  $channels = [];

                if(auth()->user()) {
                    foreach(auth()->user()->patients as $patient) {
                        array_push($channels, md5('patient-channel-'.$patient->id).':App\\Events\\GeneratePatientNotification');
                    }
                }
        ?>

        window.User = <?php echo json_encode([
                                'user' => auth()->user(),
                                'roles' => auth()->user()->roles()->first(),
                                'network' => auth()->user()->networks()->first(),
                                'new_notifications' => auth()->user()->unread_notifications,
                                'read_notifications' => auth()->user()->read_notifications,
                                'channels' => $channels
                ]); ?>
    </script>
</head>
<body id="patient-portal">
<!-- Feel free to put this in it's own Blade -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div style="margin:0 25px 0 25px;">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="/img/cal-live-stream-logo.png">
                {{--<i class="ss-leaf"></i>--}}
                {{--{{ config('app.name', 'Flipmin') }}--}}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <notification-window :notifications="notifications" :user="user"></notification-window>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->username }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="#" v-link="{ path: '/account' }">
                                    <i class="ss-settings"></i>
                                    My Account
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off"></i>
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
@yield('content')

<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.0.0/socket.io.js"></script>

@if(env('APP_ENV') == 'local')
    <!--
    <script>
        var socket = io('http://192.168.10.10:3000');
    </script>
    -->
@elseif(env('APP_ENV') == 'test')
    <!--
    <script>
        var socket = io('http://dev.rejiva.com:3000');
    </script>
    -->
@else
    <!--
    <script>
        var socket = io('https://staging.rejiva.com:3000');
    </script>
    -->
@endif
<script src="/js/patient-portal.js"></script>
@stack('scripts')
</body>
</html>