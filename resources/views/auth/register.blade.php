@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Registration Disabled</h3>
                    </div>
                    <div class="panel-body">
                        <p>Registration has been disabled for this site.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
