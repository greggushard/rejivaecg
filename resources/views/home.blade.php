@extends('layouts.app')

@section('content')
    @if(auth()->user()->hasRole('admin'))
        <navigation-menu :user="user"></navigation-menu>
    @elseif(auth()->user()->hasRole('network-admin'))
        <network-admin-menu :user="user"></network-admin-menu>
    @elseif(auth()->user()->hasRole('cardiac-tech')
            || auth()->user()->hasRole('cardiologist')
            || auth()->user()->hasRole('physician'))
        <patient-portal-menu :user="user"></patient-portal-menu>
    @endif

    <div id="wrapper">
        <div id="page-content-wrapper">
            <router-view :user="user"></router-view>
        </div>
    </div>
@endsection

