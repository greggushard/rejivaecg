<html>
<head>
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Helvetica', 'Arial', sans-serif;
            font-weight: 100;
            height: 100vh;
        }
    </style>
</head>
<body>
<div style="width:100%;background-color:#eee;height:auto;padding:15px">
    <h1>Your Password Has Been Reset,</h1>
    <p>Your account's password was recently reset.</p>
    <p>You can access your account using the username {{ $user->username }} and the following temporary password::</p>
    <p><strong>{{ $user->clean_pw }}</strong></p>
    <p>Visit <a href="{{ env('APP_URL') }}/login">Rejiva.com</a> to login to your account.</p>
    <p>- The Rejiva Team</p>
</div>
</body>
</html>