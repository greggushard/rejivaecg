<html>
<head>
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Helvetica', 'Arial', sans-serif;
            font-weight: 100;
            height: 100vh;
        }
    </style>
</head>
    <body>
        <div style="width:100%;background-color:#eee;height:auto;padding:15px">
            <h1>Welcome to Rejiva ECG,</h1>
            <p>The username {{ $user->username }}, was recently registered with Rejiva ECG.</p>
            <p>You can access your account using your email address and the following temporary password:</p>
            <p><strong>{{ $user->clean_pw }}</strong></p>
            <p>Please <a href="{{ env('APP_URL') }}/login">login</a> to set up your Rejiva account.</p>
            <p>- The Rejiva Team</p>
        </div>
    </body>
</html>