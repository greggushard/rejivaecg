<html>
<head>
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Helvetica', 'Arial', sans-serif;
            font-weight: 100;
            height: 100vh;
        }
    </style>
</head>
<body>
<div style="width:100%;background-color:#eee;height:auto;padding:15px">
    @if($referral->priority == 3)
        <h1>Urgent Referral Received</h1>
    @elseif($referral->priority == 2)
        <h1>Medium Priority Referral Received</h1>
    @elseif($referral->priority == 1)
        <h1>Low Priortiy Referral Received</h1>
    @endif

    <p>
        You have received a new referral from {{ $referral->creator->name }} of  {{ $referral->creator->networks->first()->name }}.<br /><br />
        Please <a href="{{ env('APP_URL') }}patient-portal/recordings/{{ $referral->recording->patient->id }}/{{ $referral->recording->recording_session->id }}?view_recording={{ $referral->recording->recording_hash }}">visit Rejiva ECG</a> to login and view this recording.

        <h3>Notes:</h3>
        <blockquote>
            {{ $referral->notes()->first()->content }}
        </blockquote>
        <h3>Recording Snapshot:</h3>
        <img
             src="data:image/svg+xml;base64,{{ Storage::disk('local')->get('ecg_graphs_svg/' . $referral->image_file) }}"
             title="ECG Recording for patient {{ $referral->recording->patient->id }}"
             alt="ECG Recording: {{ $referral->recording->recording_hash }}" />
</div>
</body>
</html>