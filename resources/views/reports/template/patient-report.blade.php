<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head lang="en">
    <meta charset="UTF-8">
    <title>Rijuven - CAI Report</title>
    <link rel="stylesheet" href="{{ asset('css/reports/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/reports/bootstrap-theme.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/reports/cai-rijuven-report.css') }}">
    <style>

        .measurements td {
            text-align: center;
        }

        .measurements td.qrs {
            background-color: #ef563b;
            color: #fff;
            font-weight: 800;
            text-align: center;
        }

        .measurements td.qt {
            background-color: #505fe5;
            color: #fff;
            font-weight: 800;
            text-align: center;
        }

        .measurements td.pr {
            background-color: #f78922;
            color: #fff;
            font-weight: 800;
            text-align: center;
        }

        .measurements td.rr {
            background-color: #c542f4;
            color: #fff;
            font-weight: 800;
            text-align: center;
        }

        .measurements td.br {
            background-color: #2ca02c;
            color: #fff;
            font-weight: 800;
        }

    </style>
</head>
<body>
<div class="container-fluid report-container">
    @yield('content')
</div>
</body>
</html>