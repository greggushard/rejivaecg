@extends('reports.template.patient-report')

@section('content')
    <div class="row">
        <div class="col-xs-12 report-table">
            <table>
                <thead>
                <tr>
                    <th colspan="4">Monitoring Information</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th class="width-15">Start Date:</th>
                    <td>{{ $data['cover_sheet_data']['monitoring_data']['start_date'] }}</td>
                    <th class="width-15">End/Return:</th>
                    <td>{{ $data['cover_sheet_data']['monitoring_data']['end_date'] }} {{ $data['cover_sheet_data']['monitoring_data']['return_date'] }}</td>
                </tr>
                <tr>
                    <th>Interpreting Physician:</th>
                    <td>{{ $data['cover_sheet_data']['interpreting_physician']['name'] }}</td>
                    <th>Facility:</th>
                    <td>
                        {{ $data['cover_sheet_data']['interpreting_physician']['street_address'] }}<br />
                        {{ $data['cover_sheet_data']['interpreting_physician']['city'] }} {{ $data['cover_sheet_data']['referring_physician']['province'] }},
                        {{ $data['cover_sheet_data']['interpreting_physician']['postal_code'] }}<br />
                        p: {{ $data['cover_sheet_data']['interpreting_physician']['phone'] }} / f: {{ $data['cover_sheet_data']['interpreting_physician']['fax'] }}
                    </td>
                </tr>
                <tr>
                    <th>Model:</th>
                    <td>Rejiva</td>
                    <th>Serial Numbers:</th>
                    <td>
                        @foreach($data['cover_sheet_data']['monitoring_data']['devices'] as $device)
                            {{ $device['radio_id'] }}<br />
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Indication(s):</th>
                    <td colspan="3">

                    </td>
                </tr>
                <tr>
                    <th>Interpretation:</th>
                    <td colspan="3">
                        @foreach($data['cover_sheet_data']['monitoring_data']['interpretations'] as $interpretation)
                            {{ $interpretation->content }}<br />
                            <small>- Interpreted by {{ $interpretation->creator->name_first }} {{ $interpretation->creator->name_last }}</small><br /><br />
                        @endforeach
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <h1>Summary</h1>
        <table style="border:none;table-layout:fixed;width:100%;">
            <tr>
                <td>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Patient Event Totals</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <strong>Chest Pain:</strong> {{ $data['holter_summary']['patient_event_totals']['chest_pain'] }}
                                </li>
                                <li class="list-group-item">
                                    <strong>Shortness of Breath:</strong> {{ $data['holter_summary']['patient_event_totals']['shortness_of_breath'] }}
                                </li>
                                <li class="list-group-item">
                                    <strong>Fatigue/Weakness:</strong>  {{ $data['holter_summary']['patient_event_totals']['fatigue'] }}
                                </li>
                                <li class="list-group-item">
                                    <strong>Heart Palpitation:</strong>  {{ $data['holter_summary']['patient_event_totals']['heart_palpitation'] }}
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Significant Event Totals</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <strong>Rapid Atrial Fibrillation:</strong> {{ $data['holter_summary']['signficiant_event_totals']['rapid_atrial_fibrillation'] }}
                                </li>
                                <li class="list-group-item">
                                    <strong>Third-Degree Block:</strong> {{ $data['holter_summary']['signficiant_event_totals']['third_degree_block'] }}
                                </li>
                                <li class="list-group-item">
                                    <strong>Ventricular Tachycardia:</strong> {{ $data['holter_summary']['signficiant_event_totals']['ventricular_tachycardia'] }}
                                </li>
                                <li class="list-group-item">
                                    <strong>Atrial Fibrillation:</strong> {{ $data['holter_summary']['signficiant_event_totals']['atrial_fibrillation'] }}
                                </li>
                                <li class="list-group-item">
                                    <strong>SVT:</strong> {{ $data['holter_summary']['signficiant_event_totals']['svta'] }}
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">HR Variability</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <strong>Min: {{ $data['holter_summary']['hr_variability']['min'] }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <strong>Max:  {{ $data['holter_summary']['hr_variability']['max'] }}</strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
       </div>
    <div class="row">
        <h1>Significant Event Summary</h1>
        <table style="border:none;table-layout:fixed;width:100%;">
            <tr>
                <td>
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title">Rapid AFib</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <strong>Total:</strong> {{ $data['holter_summary']['signficiant_event_totals']['rapid_atrial_fibrillation'] }}
                                </li>
                                <li class="list-group-item">
                                    <strong>Min HR:  {{ $data['holter_summary']['significant_event_summary']['rapid_atrial_fibrillation']['min_hr'] }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <strong>Max HR:  {{ $data['holter_summary']['significant_event_summary']['rapid_atrial_fibrillation']['max_hr'] }}</strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title">3<sup>rd</sup> Degree Block</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <strong>Total:</strong> {{ $data['holter_summary']['signficiant_event_totals']['third_degree_block'] }}
                                </li>
                                <li class="list-group-item">
                                    <strong>Min HR:  {{ $data['holter_summary']['significant_event_summary']['third_degree_block']['min_hr'] }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <strong>Max HR:  {{ $data['holter_summary']['significant_event_summary']['third_degree_block']['max_hr'] }}</strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <table style="border:none;table-layout:fixed;width:100%;">
            <tr>
                <td>
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title">Ventricular Tachycardia</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <strong>Total:</strong> {{ $data['holter_summary']['significant_event_summary']['ventricular_tachycardia']['total'] }}
                                </li>
                                <li class="list-group-item">
                                    <strong>Min HR:  {{ $data['holter_summary']['significant_event_summary']['ventricular_tachycardia']['min_hr'] }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <strong>Max HR:  {{ $data['holter_summary']['significant_event_summary']['ventricular_tachycardia']['max_hr'] }}</strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title">Atrial Fibrillation</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <strong>Total:</strong> {{ $data['holter_summary']['significant_event_summary']['atrial_fibrillation']['total'] }}
                                </li>
                                <li class="list-group-item">
                                    <strong>Min HR:  {{ $data['holter_summary']['significant_event_summary']['atrial_fibrillation']['min_hr'] }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <strong>Max HR:  {{ $data['holter_summary']['significant_event_summary']['atrial_fibrillation']['max_hr'] }}</strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
            <tr><td><div style="page-break-after:always"></div></td></tr>
            <tr>
                <td>
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title">SVT</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <strong>Total:</strong> {{ $data['holter_summary']['significant_event_summary']['svta']['total'] }}
                                </li>
                                <li class="list-group-item">
                                    <strong>Min HR:  {{ $data['holter_summary']['significant_event_summary']['svta']['min_hr'] }}</strong>
                                </li>
                                <li class="list-group-item">
                                    <strong>Max HR:  {{ $data['holter_summary']['significant_event_summary']['svta']['max_hr'] }}</strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="row">
        <h1>Significant Events</h1>
        <table style="border:none;table-layout:fixed;width:100%;" class="measurements">
            <tr>
                <th>
                    <h3>Afib Rapid</h3>
                </th>
            </tr>
            <tr>
                <td>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <strong>Total:  {{ $data['holter_summary']['significant_event_summary']['rapid_atrial_fibrillation']['total'] }}</strong>
                        </li>
                        <li class="list-group-item">
                            <strong>Min HR:  {{ $data['holter_summary']['significant_event_summary']['rapid_atrial_fibrillation']['min_hr'] }}</strong>
                        </li>
                        <li class="list-group-item">
                            <strong>Max HR:  {{ $data['holter_summary']['significant_event_summary']['rapid_atrial_fibrillation']['max_hr'] }}</strong>
                        </li>
                    </ul>
                </td>
                <td>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="qrs">
                                        QRS:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['rapid_atrial_fibrillation']['max_qrs'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['rapid_atrial_fibrillation']['min_qrs'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="qt">
                                        QT:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['rapid_atrial_fibrillation']['max_qt'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['rapid_atrial_fibrillation']['min_qt'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="pr">
                                        PR:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['rapid_atrial_fibrillation']['max_pr'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['rapid_atrial_fibrillation']['min_pr'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="rr">
                                        RR:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['rapid_atrial_fibrillation']['max_hr'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['rapid_atrial_fibrillation']['min_hr'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
        @foreach($data['holter_summary']['significant_event_summary']['rapid_atrial_fibrillation']['additional_data']['strips'] as $strip)
            <img class='ecg-strip' src="data:image/png;base64,{{ $strip['img'] }}" alt="base64 test">
        @endforeach
        <div style="page-break-after:always;"></div>
        <table style="border:none;table-layout:fixed;width:100%;" class="measurements">
            <tr>
                <th>
                    <h3>Third Degree Block</h3>
                </th>
            </tr>
            <tr>
                <td>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <strong>Total:  {{ $data['holter_summary']['significant_event_summary']['third_degree_block']['total'] }}</strong>
                        </li>
                        <li class="list-group-item">
                            <strong>Min HR:  {{ $data['holter_summary']['significant_event_summary']['third_degree_block']['min_hr'] }}</strong>
                        </li>
                        <li class="list-group-item">
                            <strong>Max HR:  {{ $data['holter_summary']['significant_event_summary']['third_degree_block']['max_hr'] }}</strong>
                        </li>
                    </ul>
                </td>
                <td>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="qrs">
                                        QRS:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['third_degree_block']['max_qrs'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['third_degree_block']['min_qrs'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="qt">
                                        QT:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['third_degree_block']['max_qt'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['third_degree_block']['min_qt'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="pr">
                                        PR:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['third_degree_block']['max_pr'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['third_degree_block']['min_pr'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="rr">
                                        RR:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['third_degree_block']['max_hr'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['third_degree_block']['min_hr'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
        @foreach($data['holter_summary']['significant_event_summary']['third_degree_block']['additional_data']['strips'] as $strip)
            <img class='ecg-strip' src="data:image/png;base64,{{ $strip['img'] }}" alt="base64 test">
        @endforeach
        <div style="page-break-after:always;"></div>
        <table style="border:none;table-layout:fixed;width:100%;" class="measurements">
            <tr>
                <th>
                    <h3>Ventricular Tachycardia</h3>
                </th>
            </tr>
            <tr>
                <td>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <strong>Total:  {{ $data['holter_summary']['significant_event_summary']['ventricular_tachycardia']['total'] }}</strong>
                        </li>
                        <li class="list-group-item">
                            <strong>Min HR:  {{ $data['holter_summary']['significant_event_summary']['ventricular_tachycardia']['min_hr'] }}</strong>
                        </li>
                        <li class="list-group-item">
                            <strong>Max HR:  {{ $data['holter_summary']['significant_event_summary']['ventricular_tachycardia']['max_hr'] }}</strong>
                        </li>
                    </ul>
                </td>
                <td>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="qrs">
                                        QRS:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['ventricular_tachycardia']['max_qrs'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['ventricular_tachycardia']['min_qrs'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="qt">
                                        QT:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['ventricular_tachycardia']['max_qt'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['ventricular_tachycardia']['min_qt'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="pr">
                                        PR:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['ventricular_tachycardia']['max_pr'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['ventricular_tachycardia']['min_pr'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="rr">
                                        RR:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['ventricular_tachycardia']['max_hr'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['ventricular_tachycardia']['min_hr'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
        @foreach($data['holter_summary']['significant_event_summary']['ventricular_tachycardia']['additional_data']['strips'] as $strip)
            <img class='ecg-strip' src="data:image/png;base64,{{ $strip['img'] }}" alt="base64 test">
        @endforeach
        <div style="page-break-after:always;"></div>
        <table style="border:none;table-layout:fixed;width:100%;" class="measurements">
            <tr>
                <th>
                    <h3>AFib Slow</h3>
                </th>
            </tr>
            <tr>
                <td>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <strong>Total:  {{ $data['holter_summary']['significant_event_summary']['atrial_fibrillation']['total'] }}</strong>
                        </li>
                        <li class="list-group-item">
                            <strong>Min HR:  {{ $data['holter_summary']['significant_event_summary']['atrial_fibrillation']['min_hr'] }}</strong>
                        </li>
                        <li class="list-group-item">
                            <strong>Max HR:  {{ $data['holter_summary']['significant_event_summary']['atrial_fibrillation']['max_hr'] }}</strong>
                        </li>
                    </ul>
                </td>
                <td>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="qrs">
                                        QRS:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['atrial_fibrillation']['max_qrs'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['atrial_fibrillation']['min_qrs'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="qt">
                                        QT:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['atrial_fibrillation']['max_qt'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['atrial_fibrillation']['min_qt'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="pr">
                                        PR:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['atrial_fibrillation']['max_pr'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['atrial_fibrillation']['min_pr'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="rr">
                                        RR:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['atrial_fibrillation']['max_hr'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['atrial_fibrillation']['min_hr'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
        @foreach($data['holter_summary']['significant_event_summary']['atrial_fibrillation']['additional_data']['strips'] as $strip)
            <img class='ecg-strip' src="data:image/png;base64,{{ $strip['img'] }}" alt="base64 test">
        @endforeach
        <div style="page-break-after:always;"></div>
        <table style="border:none;table-layout:fixed;width:100%;" class="measurements">
            <tr>
                <th>
                    <h3>SVT</h3>
                </th>
            </tr>
            <tr>
                <td>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <strong>Total:  {{ $data['holter_summary']['significant_event_summary']['svta']['total'] }}</strong>
                        </li>
                        <li class="list-group-item">
                            <strong>Min HR:  {{ $data['holter_summary']['significant_event_summary']['svta']['min_hr'] }}</strong>
                        </li>
                        <li class="list-group-item">
                            <strong>Max HR:  {{ $data['holter_summary']['significant_event_summary']['svta']['max_hr'] }}</strong>
                        </li>
                    </ul>
                </td>
                <td>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="qrs">
                                        QRS:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['svta']['max_qrs'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['svta']['min_qrs'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="qt">
                                        QT:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['svta']['max_qt'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['svta']['min_qt'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="pr">
                                        PR:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['svta']['max_pr'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['svta']['min_pr'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li class="list-group-item">
                            <table>
                                <tr>
                                    <td class="rr">
                                        RR:
                                    </td>
                                    <td>
                                        {{ $data['holter_summary']['significant_event_summary']['svta']['max_hr'] }} (High)<br />
                                        {{ $data['holter_summary']['significant_event_summary']['svta']['min_hr'] }} (Low)<br />
                                    </td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
        @foreach($data['holter_summary']['significant_event_summary']['svta']['additional_data']['strips'] as $strip)
            <img class='ecg-strip' src="data:image/png;base64,{{ $strip['img'] }}" alt="base64 test">
        @endforeach
        <div style="page-break-after:always;"></div>
</div>
@endsection
