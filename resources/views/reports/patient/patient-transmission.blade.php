@extends('reports.template.patient-report')

@section('content')
        <div class="row">
            <div class="col-xs-12 report-table">
                <table>
                    <thead>
                        <tr>
                            <th colspan="4">Monitoring Information</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th class="width-15">Start Date:</th>
                            <td>{{ $data['cover_sheet_data']['monitoring_data']['start_date'] }}</td>
                            <th class="width-15">End/Return:</th>
                            <td>{{ $data['cover_sheet_data']['monitoring_data']['end_date'] }} {{ $data['cover_sheet_data']['monitoring_data']['return_date'] }}</td>
                        </tr>
                        <tr>
                            <th>Interpreting Physician:</th>
                            <td>{{ $data['cover_sheet_data']['interpreting_physician']['name'] }}</td>
                            <th>Facility:</th>
                            <td>
                                {{ $data['cover_sheet_data']['interpreting_physician']['street_address'] }}<br />
                                {{ $data['cover_sheet_data']['interpreting_physician']['city'] }} {{ $data['cover_sheet_data']['referring_physician']['province'] }},
                                {{ $data['cover_sheet_data']['interpreting_physician']['postal_code'] }}<br />
                                p: {{ $data['cover_sheet_data']['interpreting_physician']['phone'] }} / f: {{ $data['cover_sheet_data']['interpreting_physician']['fax'] }}
                            </td>
                        </tr>
                        <tr>
                            <th>Model:</th>
                            <td>Rejiva</td>
                            <th>Serial Numbers:</th>
                            <td>
                                @foreach($data['cover_sheet_data']['monitoring_data']['devices'] as $device)
                                    {{ $device['radio_id'] }}<br />
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Indication(s):</th>
                            <td colspan="3">

                            </td>
                        </tr>
                        <tr>
                            <th>Interpretation:</th>
                            <td colspan="3">
                                @foreach($data['cover_sheet_data']['monitoring_data']['interpretations'] as $interpretation)
                                    {{ $interpretation->content }}<br />
                                    <small>- Interpreted by {{ $interpretation->creator->name_first }} {{ $interpretation->creator->name_last }}</small><br /><br />
                                @endforeach
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 report-table">
                <table>
                    <thead>
                        <tr>
                            <th colspan="4">Present Transmission</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>Rate:</th>
                            <td>{{ $data['present_transmission']['rr']['max'] }}</td>
                            <th>To</th>
                            <td>{{ $data['present_transmission']['rr']['min'] }}</td>
                        </tr>
                        <tr>
                            <th>PR:</th>
                            <td>{{ $data['present_transmission']['pr']['max'] }}</td>
                            <th>To</th>
                            <td>{{ $data['present_transmission']['pr']['min'] }}</td>
                        </tr>
                        <tr>
                            <th>QRS:</th>
                            <td>{{ $data['present_transmission']['qrs']['max'] }}</td>
                            <th>To</th>
                            <td>{{ $data['present_transmission']['qrs']['min'] }}</td>
                        </tr>
                        <tr>
                            <th>QT:</th>
                            <td>{{ $data['present_transmission']['qt']['max'] }}</td>
                            <th>To</th>
                            <td>{{ $data['present_transmission']['rr']['min'] }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-8 sub-title strip-title">
                <h2>ECG Strips</h2>
            </div>
        </div>
        <?php $i = 0; ?>
        @foreach($data['strips'] as $strip)
            <div class="row">
                <div class="col-xs-12 report-table report-strip-list">
                    <table>
                        <thead>
                            <tr>
                                <th colspan="8">Strip</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(isset($strip['measurements']))
                            <tr>
                                <th class="width-5">PR:</th>
                                <td>
                                    <strong>High:</strong> {{ $strip['measurements']['pr']['max'] }} ms<br />
                                    <strong>Low:</strong> {{ $strip['measurements']['pr']['min'] }} ms
                                </td>
                                <th class="width-5">QRS:</th>
                                <td>
                                    <strong>High:</strong> {{ $strip['measurements']['qrs']['max'] }} ms<br />
                                    <strong>Low:</strong> {{ $strip['measurements']['qrs']['min'] }} ms
                                </td>
                                <th class="width-5">QT:</th>
                                <td>
                                    <strong>High:</strong> {{ $strip['measurements']['qt']['max'] }} ms<br />
                                    <strong>Low:</strong> {{ $strip['measurements']['qt']['min'] }} ms
                                </td>
                                <th class="width-5">Rate:</th>
                                <td>
                                    <strong>High:</strong> {{ $strip['measurements']['rr']['max'] }} bpm<br />
                                    <strong>Low:</strong> {{ $strip['measurements']['rr']['min'] }} bpm
                                </td>
                            </tr>
                            <tr>
                                <th class="width-5">Patient Events:</th>
                                <td colspan="7">
                                    @foreach($strip['reported_events'] as $event)
                                        &nbsp;{{ $event['description'] }},
                                    @endforeach
                                </td>
                            </tr>
                        @endif
                        @if(count($strip['notes']['recording_notes']) > 0)
                            <tr>
                                <th class="th-text-center" colspan="8">Notes</th>
                            </tr>
                        @endif
                        @foreach($strip['notes']['recording_notes'] as $note)
                            <tr>
                                <td colspan="2">{{$note['user']}}&nbsp;{!! $note['user_badge']!!} <br>&nbsp;</td>
                                <td colspan="1">{{$note['category']}}</td>
                                <td colspan="5">{{$note['content']}}</td>
                            </tr>
                        @endforeach
                        <!-- End ECG Strip Loop -->
                        </tbody>
                    </table>
                    <br><br>
                    <div class="strip-wrap">
                        <img class='ecg-strip first-image' src="data:image/png;base64,{{$strip['recording_img']}}" alt="base64 test">
                    </div>
                </div>
            </div>
            <?php $i++; ?>
            @if(($i % 2) === 0)
                <div style="page-break-after: always;"></div>
            @endif
        @endforeach
        <div class="row">
            <div class="col-xs-12 report-table">
                <table>
                    <thead>
                        <tr>
                            <th colspan="4">Transmission History</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Date and Time</td>
                            <td>Symptoms</td>
                            <td>Findings</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
@endsection