<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head lang="en">
    <meta charset="UTF-8">
    <title>Rijuven - CAI Report</title>

    <link rel="stylesheet" href="{{ asset('css/reports/cai-rijuven-report.css') }}">
</head>
<body>
<!--And not before time! Did you intend to leave us standing on the doorstep all day? We're drenched!-->
<div class="container-fluid page-last-item">

    <div class="row report-header">
        <div class="col-xs-8 title">
            <h1>Canadian Arrhythmia Institute </h1>
        </div>
        <!-- Now look! I've gone and caught a sniffle.-->
        <div class="col-xs-4 address">
            <p><strong>{{$data['cover_sheet_data']['network']['name']}}</strong><br/>{{$data['cover_sheet_data']['network']['address']}}<br/>{{$data['cover_sheet_data']['network']['city']}} {{$data['cover_sheet_data']['network']['province']}} {{$data['cover_sheet_data']['network']['postal_code']}}<br/>{{$data['cover_sheet_data']['network']['country']}} +1 {{$data['cover_sheet_data']['network']['phone']}}</p>
        </div>
        <!-- Are you expected?-->
    </div>
    <!--Don't take that tone with me, my good man. Now buttle off and tell Baron Brunwald that Lord Clarence MacDonald and his lovely assistant are here to view the tapestries.-->
    <div class="row">
        <div class="col-xs-8 sub-title">
            <h2>Patient Tapestry Report</h2>
        </div>
        <!-- Tapestries?-->
        <div class="col-xs-4 timestamp">
            <p><strong>Generated: {{$data['cover_sheet_data']['generated']}}</strong><br/></p>
            <!--<p><strong>Printed: 19-Apr-2016 at 12:27 PM</strong><br/><strong>Page 1 of 3</strong></p>-->
        </div>
    </div>
    <!-- Dear me, the man is dense. This is a castle, isn't it? There are tapestries?-->
    <div class="row">
        <div class="col-xs-12 report-table">
            <table>
                <thead>
                <tr>
                    <th colspan="2">Patient Information</th>
                    <th colspan="2">Physician Information</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th class="width-15">Name:</th>
                    <td>{{$data['cover_sheet_data']['patient']['name']}}</td>
                    <th class="width-15">Physician:</th>
                    <td>{{$data['cover_sheet_data']['physician']['name']}}</td>
                </tr>
                <tr>
                    <th>Patient ID:</th>
                    <td>#{{$data['cover_sheet_data']['patient']['id']}}</td>
                    <th>Office:</th>
                    <td>{{$data['cover_sheet_data']['physician']['office']}}</td>
                </tr>
                <tr>
                    <th>Home:</th>
                    <td>{{$data['cover_sheet_data']['patient']['home']}}</td>
                    <th>Address:</th>
                    <td>{{$data['cover_sheet_data']['physician']['address']}}</td>
                </tr>
                <tr>
                    <th>DOB:</th>
                    <td>{{$data['cover_sheet_data']['patient']['dob']}}</td>
                    <td></td>
                    <td>{{$data['cover_sheet_data']['physician']['city']}}, {{$data['cover_sheet_data']['physician']['province']}}</td>
                </tr>
                <tr>
                    <th>Gender:</th>
                    <td>{{$data['cover_sheet_data']['patient']['gender']}}</td>
                    <td></td>
                    <td>{{$data['cover_sheet_data']['physician']['postal_code']}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!--This is a castle. And we have many tapestries. But if you're a Scottish lord, then I am Mickey Mouse -->
<!--How dare he?!-->



@for($i=0; $i<count($data['strips']); $i++)
    @if(($i % 120) == 0)
        <table class='tapestry-table page-last-item' cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="3" class="header">
                <table class="inner-tapestry">
                    <tr>
                        <td class='inner-left'>
                            {{$data['cover_sheet_data']['patient']['name']}}
                        </td>
                        <td class='inner-center'>
                            <strong>Patient Full Disclosure Report</strong>
                        </td>
                        <td class='inner-right'>
                            <strong>{{$data['strips'][$i]['timestamp']}}</strong>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    @endif
    @if(($i % 10) == 0)
        <tr >
            <td style="width:6%" rowspan="5" class='time tapestry-td'>{{$data['strips'][$i]['timestamp']}}</td>
            <td style="width:47%"><img src="{{$data['strips'][$i]['base64']}}" ></td>
    @endif
    @if(($i % 10) == 1)
            <td style="width:47%"><img src="{{$data['strips'][$i]['base64']}}" ></td>
        </tr>
    @endif
    @if(($i % 10) == 2)
        <tr >
            <td style="width:47%"><img src="{{$data['strips'][$i]['base64']}}" ></td>
    @endif
    @if(($i % 10) == 3)
                <td style="width:47%"><img src="{{$data['strips'][$i]['base64']}}" ></td>
        </tr>
    @endif
    @if(($i % 10) == 4)
        <tr >
            <td style="width:47%"><img src="{{$data['strips'][$i]['base64']}}" ></td>
    @endif
    @if(($i % 10) == 5)
                <td style="width:47%"><img src="{{$data['strips'][$i]['base64']}}" ></td>
        </tr>
    @endif
    @if(($i % 10) == 6)
        <tr >
            <td style="width:47%"><img src="{{$data['strips'][$i]['base64']}}" ></td>
    @endif
    @if(($i % 10) == 7)
                <td style="width:47%"><img src="{{$data['strips'][$i]['base64']}}" ></td>
        </tr>
    @endif
    @if(($i % 10) == 8)
        <tr >
            <td style="width:47%"><img src="{{$data['strips'][$i]['base64']}}" ></td>
    @endif
    @if(($i % 10) == 9)
                <td style="width:47%"><img src="{{$data['strips'][$i]['base64']}}" ></td>
        </tr>
    @endif
    @if(($i % 120) == 119)
        <tr>
            <td colspan="3" class="footer inner-center">
                Page {{round($i / 120)}} of {{ceil(count($data['strips'])/120)}}
            </td>
        </tr>
        </table>
    @endif
@endfor
@if(count($data['strips'])%120 != 0)
    @if((count($data['strips']) % 2) == 1)
        </tr>
    @endif
    @if((count($data['strips']) % 10) == 1)
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>
    @endif
    @if((count($data['strips']) % 10) == 3)
        <tr></tr>
        <tr></tr>
        <tr></tr>
    @endif
    @if((count($data['strips']) % 10) == 5)
        <tr></tr>
        <tr></tr>
    @endif
    @if((count($data['strips']) % 10) == 7)
        <tr></tr>
    @endif
    <tr>
        <td colspan="3" class="footer inner-center">
            Page {{ceil(count($data['strips'])/120)}} of {{ceil(count($data['strips'])/120)}}
        </td>
    </tr>
    </table>
@endif
</body>
</html>