import { Bar } from 'vue-chartjs'
import Axios from 'axios';

if(window.User.network == null) {
    var network_id = 1;
} else {
    var network_id = window.User.network.id;
}

export default Bar.extend({
    ready () {
        var chart = this;
        Axios.defaults.headers.common['X-CSRF-TOKEN'] = window.Laravel.csrfToken;
        Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        Axios.get('/api/v1/networks/' + network_id + '/metrics/reports')
            .then(function (response) {
                var data = response.data;
                chart.render({
                    labels: ['Light Headed', 'Shortness of Breath', 'Fatigue', 'Heart Palpitation', 'Fall Detected'],
                    datasets: [
                        {
                            label: 'Reported Events',
                            backgroundColor: '#204670',
                            data: [data.light_headed, data.shortness_of_breath, data.fatigue, data.heart_palpitation, data.fall_detected]
                        }
                    ]
                }, {height: 320, responsive: true, maintainAspectRatio: false})
            })
            .catch(function (error) {
                console.error(error);
            });
    },
})