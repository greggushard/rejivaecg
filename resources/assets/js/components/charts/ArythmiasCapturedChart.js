import { Doughnut } from 'vue-chartjs'
import Axios from 'axios';

if(window.User.network == null) {
    var network_id = 1;
} else {
    var network_id = window.User.network.id;
}

export default Doughnut.extend({
    ready () {
        var chart = this;
        Axios.defaults.headers.common['X-CSRF-TOKEN'] = window.Laravel.csrfToken;
        Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        Axios.get('/api/v1/networks/' + network_id + '/metrics/users')
            .then(function (response) {
                var data = response.data;
                chart.render({
                    labels: ['AFib Rapid', 'Mobitz I', 'Mobitz II', 'VT', 'VF', 'SVTA', 'Pause', 'AFib Normal', 'AFib Slow'],
                    datasets: [
                        {
                            label: ['AFib Rapid', 'Mobitz I', 'Mobitz II', 'VT', 'VF', 'SVTA', 'Pause', 'AFib Normal', 'AFib Slow'],
                            backgroundColor: [
                                '#204670',
                                '#7daed3',
                                '#363d68',
                                '#8592e2',
                                '#8b5da0',
                                '#3e2f44',
                                '#567c7c',
                                '#4c8472',
                                '#844c4c'
                            ],
                            data: [
                                data.afib_rapid,
                                data.pause,
                                data.vt,
                                data.vf,
                                data.mobitz_i,
                                data.mobitz_ii,
                                data.afib_slow,
                                data.afib_normal,
                                data.svta
                            ]
                        }
                    ]
                }, {height: 320, responsive: true, maintainAspectRatio: false})
            })
            .catch(function (response) {

            });
    },
})