import { Bar } from 'vue-chartjs'

export default Bar.extend({
    ready () {
        this.render({
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            datasets: [
                {
                    label: 'Users Registered',
                    backgroundColor: '#00b050',
                    data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11]
                }
            ]
        }, {height: 320, responsive: true, maintainAspectRatio: false})
    },
})