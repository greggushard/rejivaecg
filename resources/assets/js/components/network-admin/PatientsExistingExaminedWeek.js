import { Line } from 'vue-chartjs'

export default Line.extend({
    ready () {
        this.render({
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            datasets: [
                {
                    label: 'Patient Examinations',
                    backgroundColor: '#00b050',
                    data: [22, 43, 21, 18, 11, 22, 98, 56, 43, 17, 84, 97]
                }
            ]
        }, {height: 320, responsive: true, maintainAspectRatio: false})
    },
})