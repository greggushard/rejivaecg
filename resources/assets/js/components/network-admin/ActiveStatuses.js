import { Doughnut } from 'vue-chartjs'

export default Doughnut.extend({
    ready () {
        this.render({
            labels: ['Networks', 'Practices'],
            datasets: [
                {
                    label: ['Network Usage', 'Practice Usage'],
                    backgroundColor: [
                        '#41B883',
                        '#E46651',
                    ],
                    data: [53, 27]
                }
            ]
        }, {height: 320, responsive: true, maintainAspectRatio: false})
    },
})