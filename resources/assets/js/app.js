/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */
var VueRouter = require('vue-router');
var VueFilter = require('vue-filter');

var _ = require('underscore');
Vue.use(VueRouter);
Vue.use(VueFilter);

var router = new VueRouter({
    history: true,
});

// Dashboards
import AdminDashboard from './components/AdminDashboard.vue'; // Admin
import NetworkAdminDashboard from './components/NetworkAdminDashboard.vue'; // Network Admin
import PatientDashboard from './components/PatientDashboard.vue'; // All other roles

import LoadingMask from './components/misc/LoadingMask.vue';//loading mask


// Indexes
import UserIndex from './components/users/UserIndex.vue';
import PatientIndex from './components/patients/PatientIndex.vue';
import NetworkIndex from './components/networks/NetworkIndex.vue';
import PracticeIndex from './components/practices/PracticeIndex.vue';
import DeviceIndex from './components/devices/DeviceIndex.vue';
import RecordingIndex from './components/recordings/RecordingIndex.vue';
import RecordingSessionsOverview from './components/recording-sessions/RecordingSessionsIndex.vue';

// Components with bindings
import RecordingGraphsPanel from './components/recordings/RecordingGraphsPanel.vue';
import SavedStrips from './components/strips/SavedStrips.vue';

// Laravel Passport
import Clients from './components/passport/Clients.vue';
import AuthorizedClients from './components/passport/AuthorizedClients.vue';
import PersonalAccessTokens from './components/passport/PersonalAccessTokens.vue';

// Utilities by Role
import NavigationMenu from './components/utilities/NavigationMenu.vue';
import NetworkAdminMenu from './components/utilities/NetworkAdminMenu.vue';
import PatientPortalMenu from './components/utilities/PatientPortalMenu.vue';

// Utilities for all users
import NotificationWindow from './components/notifications/NotificationWindow.vue';
import VuetablePagination from 'vuetable/src/components/VuetablePagination.vue';
import EditAccount from './components/utilities/EditAccount.vue';

/**
 * Staff components
 */
import CreateUser from './components/users/CreateUser.vue';
import ShowUser from './components/users/ShowUser.vue';
import EditUser from './components/users/EditUser.vue';

/**
 * Patient components
 */
import CreatePatient from './components/patients/CreatePatient.vue';
import EditPatient from './components/patients/EditPatient.vue';
import ShowPatient from './components/patients/ShowPatient.vue';

/**
 * Practice components
 */
import EditPractice from './components/practices/EditPractice.vue';
import ShowPractice from './components/practices/ShowPractice.vue';
import CreatePractice from './components/practices/CreatePractice.vue';

/**
 * Recording session components
 */
import CreateRecordingSession from './components/recording-sessions/CreateRecordingSession.vue';
import UpdateRecordingSession from './components/recording-sessions/UpdateRecordingSession.vue';

// Initialize pagination component
Vue.component('vuetable-pagination', VuetablePagination);

router.map({

    // User Routes & Utils

     // User Utilities
     '/account' : {
        component: EditAccount,
        props: ['user']
     },

     /*
     |-----------------------------------------------------------
     | ADMINISTRATIVE DASHBOARD ROUTES
     |-----------------------------------------------------------
     */
    '/mastermind': {
        component: AdminDashboard,
        props: ['user']
    },
    '/mastermind/users': {
        component: UserIndex,
        props: ['user']
    },
    '/mastermind/patients': {
        component: PatientIndex,
        props: ['patient']
    },
    '/mastermind/patients/edit/:patientID': {
        component: EditPatient,
        props: ['patient']
    },
    '/mastermind/patients/show/:patientID': {
        component: ShowPatient,
        props: ['patient']
    },
    '/mastermind/patients/create/': {
        component: CreatePatient,
        props: ['patient']
    },
    '/mastermind/networks' : {
        component: NetworkIndex,
        props: ['user']
    },
    '/mastermind/practices': {
        component: PracticeIndex,
        props: ['user']
    },
    '/mastermind/practices/edit/:practiceID': {
        component: EditPractice,
        props: ['user']
    },
    '/mastermind/practices/show/:practiceID': {
        component: ShowPractice,
        props: ['user']
    },
    '/mastermind/practices/create/': {
        component: CreatePractice,
        props: ['user']
    },
    '/mastermind/clients': {
        component: Clients,
        props: ['user']
    },
    '/mastermind/clients/authorized': {
        component: AuthorizedClients,
        props: ['user']
    },
    '/mastermind/tokens': {
        component: PersonalAccessTokens,
        props: ['user']
    },

    /*
     |-----------------------------------------------------------
     | NETWORK ADMIN ROUTES
     |-----------------------------------------------------------
     */
    '/network-portal': {
        component: NetworkAdminDashboard,
        props: ['user']
    },
    '/network-portal/patients': {
        component: PatientIndex,
        props: ['user']
    },
    '/network-portal/patients/create': {
        component: CreatePatient,
        props: ['user']
    },
    '/network-portal/patients/edit/:patientID': {
        component: EditPatient,
        props: ['patient']
    },
    '/network-portal/patients/show/:patientID': {
        component: ShowPatient,
        props: ['patient']
    },
    '/network-portal/sessions': {
        component: RecordingSessionsOverview,
        props: ['user']
    },
    '/network-portal/sessions/create': {
        component: CreateRecordingSession,
        props: ['user']
    },
    '/network-portal/sessions/:session_id/edit': {
        component: UpdateRecordingSession,
        props: ['user']
    },
    '/network-portal/recordings': {
        component: RecordingIndex,
        props: ['user']
    },
    '/network-portal/recordings/:patient_id/:session_id': {
        name: 'network-recordings.show',
        component: RecordingGraphsPanel,
        props: ['user']
    },
    '/network-portal/practices': {
        component: PracticeIndex,
        props: ['user']
    },
    '/network-portal/practices/edit/:practiceID': {
        component: EditPractice,
        props: ['user']
    },
    '/network-portal/practices/show/:practiceID': {
        component: ShowPractice,
        props: ['user']
    },
    '/network-portal/practices/create/': {
        component: CreatePractice,
        props: ['user']
    },
    '/network-portal/staff': {
        component: UserIndex,
        props: ['user']
    },
    'network-portal/staff/create': {
        component: CreateUser,
        props: ['user']
    },
    '/network-portal/staff/show/:userID': {
        component: ShowUser,
        props: ['user']
    },
    '/network-portal/staff/edit/:userID': {
        component: EditUser,
        props: ['user']
    },
    '/network-portal/devices': {
        component: DeviceIndex,
        props: ['user']
    },
    '/network-portal/strips': {
        component: SavedStrips,
        props: ['user']
    }

});

//var socket = io('http://192.168.10.10:3000');

var app = Vue.extend({
    /**
     |---------------------------------------------------------
     |COMPONENTS
     |---------------------------------------------------------
     */
    components: {
                    // Admin Sidebar Nav
                    NavigationMenu,
                    // Network Admin Sidebar Nav
                    NetworkAdminMenu,
                    // Patient Portal Sidebar Nav
                    PatientPortalMenu,
                    // Additional utils
                    NotificationWindow,
                    LoadingMask

    },

    /**
     |---------------------------------------------------------
     |DATA
     |---------------------------------------------------------
     */
    data(){
        return {
            showLoadingMask: false,
            user: window.User,
            notifications: {
                new: window.User.new_notifications,
                read: window.User.read_notifications
            },
            access_token: null,
            refresh_token: null,
            token_expires: null
        }
    },

    /**
     |---------------------------------------------------------
     |INIT
     |---------------------------------------------------------
     */
    created: function() {
        var vm = this;
       /**window.User.channels.forEach(function(channel){
           socket.on(channel, function(message) {
               vm.notifications.new.push(message.notification);
           });
        });**/

    },

    /**
     |---------------------------------------------------------
     |WATCHERS
     |---------------------------------------------------------
     */
    watch: {
        'notifications' : function(val, oldVal) {
            // If there are no new notifications populate the notification window
            // with the latest notifications in the database
            if(this.notifications) {
                if(!this.notifications.length) {
                    this.getLatestNotifications();
                }
            }
        },
        'accessToken': function(val, oldVal) {

        }
    },

    /**
     |---------------------------------------------------------
     |EVENTS
     |---------------------------------------------------------
     */
    events: {
        'update-user-table': function() {
            this.$broadcast('update-user-table');
        },
        'reset-notifications': function(notifications) {
            this.notifications.new = [];
            this.notifications.read = notifications.notifications;
        },
        'push-notifications': function(notifications) {
            var notifications = JSON.parse(notifications);
        },
        'show-user': function(user) {
            this.$route.router.go('users');
            this.$broadcast('fire-show-modal', user);
        },
        'show-network': function(network) {
            this.$route.router.go('networks');
            this.$broadcast('fire-network-modal', network);
        },
        'showLoadingMask' : function(){
            this.showLoadingMask = true;
        },
        'hideLoadingMask' : function(){
            this.showLoadingMask = false;
        },
        'go-back': function() {
            this.$route.router.go(window.history.back());
        },
        'loadSelects': function() {

        }
    },

    /**
     |---------------------------------------------------------
     |METHODS
     |---------------------------------------------------------
     */
    methods: {

        /**
         * Check for new notifications after a request
         * @param response
         */
        checkForNotifications: function(response) {
            if(response.headers.hasOwnProperty('User-Notifications'))
                this.$emit('push-notifications', response.headers['User-Notifications']);
        },

        /**
         * Get the latest unread notifications
         */
        getLatestNotifications: function() {
            this.$http.get('/api/v1/user/' + this.user.user.id + '/notifications?viewed=true').then((response) => {
                this.handleSuccess(response);
        }, (response) => {
                this.handleError(response);
            });
        },

        /**
         * Set the users' access token
         * @param token
         */
        setAccessToken: function(token) {
            this.access_token = token;
        },

        /**
         * Handle a success response
         * @param response
         */
        handleSuccess: function(response) {
            this.notifications = response.data.data
        },

        /**
         * Handle an error response
         * @param response
         */
        handleError: function(response) {
            this.notifications = [];
            alert("There was an error retrieving your notifications." + JSON.stringify(response));
        }

    }
});

router.start(app, 'body');
