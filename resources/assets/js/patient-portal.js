/**
 * If you have any questions on this shit let me know
 *
 */

require('./bootstrap');
var VueRouter = require('vue-router');
var VueFilter = require('vue-filter');
var _ = require('underscore');
Vue.use(VueRouter);
Vue.use(VueFilter);

var router = new VueRouter({
    history: true,
});

// A users' saved strips
import StripsPatient from './components/strips/StripsPatient.vue';

// Temporary interface for the triage view
import TriageInterface from './components/triage/TriageInterface.vue';

// Full Disclosure Report preview
import FullDisclosureReportPreview from './components/reports/FullDisclosureReportPreview.vue';

// Utilities
import EditAccount from './components/utilities/EditAccount.vue';
import NotificationWindow from './components/notifications/NotificationWindow.vue';
import PatientDashboard from './components/PatientDashboard.vue';
import PatientPortal from './components/patient-portal/PatientPortal.vue';
import VuetablePagination from 'vuetable/src/components/VuetablePagination.vue';

// Initialize pagination component
Vue.component('vuetable-pagination', VuetablePagination);

router.map({

    // USser Routes & Utils
    // User Utilities
    '/account': {
        component: EditAccount,
        props: ['user']
    },
    '/patient-portal': {
        component: PatientDashboard,
        props: ['user']
    },

    '/patient-portal/triage': {
        component: TriageInterface,
        props: ['user']
    },

    'patient-portal/patient/:patient_id': {
        name: 'patient.show',
        component: StripsPatient,
        props: ['user']
    },

    '/patient-portal/recordings/:patient_id/:session_id': {
        name: 'recordings.show',
        component: PatientPortal,
        props: ['user']
    },

    'patient-portal/report/:patient_id/:session_id': {
        name: 'reports.show',
        component: FullDisclosureReportPreview,
        props: ['user']
    },
    'patient-portal/strips' : {
        component: StripsPatient,
        props: ['user']
    }

    // Other routes here
});

/**
 * Patient Portal App
 */
var app = Vue.extend({
    /**
     |---------------------------------------------------------
     |COMPONENTS
     |---------------------------------------------------------
     */
    components: {

        // Other components here

        // Utilities
        NotificationWindow,

    },

    /**
     |---------------------------------------------------------
     |DATA
     |---------------------------------------------------------
     */
    data(){
        return {
            user: window.User,
            notifications: {
                new: window.User.new_notifications,
                read: window.User.read_notifications
            },
            access_token: null,
            refresh_token: null,
            token_expires: null
        }
    },

    /**
     |---------------------------------------------------------
     |INIT
     |---------------------------------------------------------
     */
    created: function() {
        var vm = this;
       /** window.User.channels.forEach(function(channel){
            socket.on(channel, function(message) {
                vm.notifications.new.push(message.notification);
            });
        }); **/
    },

    /**
     |---------------------------------------------------------
     |WATCHERS
     |---------------------------------------------------------
     */
    watch: {
        'notifications' : function(val, oldVal) {
            // If there are no new notifications populate the notification window
            // with the latest notifications in the database
            if(this.notifications) {
                if(!this.notifications.length) {
                    this.getLatestNotifications();
                }
            }
        },
        'accessToken': function(val, oldVal) {

        }
    },

    /**
     |---------------------------------------------------------
     |EVENTS
     |---------------------------------------------------------
     */
    events: {
        'update-user-table': function() {
            this.$broadcast('update-user-table');
        },
        'reset-notifications': function(notifications) {
            this.notifications.new = [];
            this.notifications.read = notifications.notifications;
        },
        'push-notifications': function(notifications) {
            var notifications = JSON.parse(notifications);
        },
        'show-user': function(user) {
            this.$route.router.go('users');
            this.$broadcast('fire-show-modal', user);
        },
        'show-network': function(network) {
            this.$route.router.go('networks');
            this.$broadcast('fire-network-modal', network);
        },
    },

    /**
     |---------------------------------------------------------
     |METHODS
     |---------------------------------------------------------
     */
    methods: {

        /**
         * Check for new notifications after a request
         * @param response
         */
        checkForNotifications: function(response) {
            if(response.headers.hasOwnProperty('User-Notifications'))
                this.$emit('push-notifications', response.headers['User-Notifications']);
        },

        /**
         * Get the latest unread notifications
         */
        getLatestNotifications: function() {
            this.$http.get('/api/v1/user/' + this.user.user.id + '/notifications?viewed=true').then((response) => {
                this.handleSuccess(response);
            }, (response) => {
                this.handleError(response);
            });

        },

        /**
         * Set the users' access token
         * @param token
         */
        setAccessToken: function(token) {
            this.access_token = token;
        },

        /**
         * Handle a success response
         * @param response
         */
        handleSuccess: function(response) {
            this.notifications = response.data.data
        },

        /**
         * Handle an error response
         * @param response
         */
        handleError: function(response) {
            this.notifications = [];
            alert("There was an error retrieving your notifications." + JSON.stringify(response));
        }

    }
});

router.start(app, 'body');