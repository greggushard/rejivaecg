module.exports = {
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                },
                include: [__dirname + '../', __dirname],
                exclude: [__dirname + '/node_modules', __dirname + '/dist']

            },
            {test: /\.json$/, loader: "json"}


        ]
    }
}
