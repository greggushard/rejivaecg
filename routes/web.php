<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    if(auth()->user()) {
        if(auth()->user()->hasRole('admin')) {
            return redirect('/mastermind');
        }
        elseif(auth()->user()->hasRole('network-admin')) {
            return redirect('/network-portal');
        }
        else {
            return redirect('/patient-portal');
        }
    }
    return view('welcome');
});

Route::get('/home', function(){
    return redirect('/');
});

Auth::routes();

/*
|--------------------------------------------------------------------------
| Device authentication
|--------------------------------------------------------------------------
*/
Route::post('/authenticate/patient', 'Api\RecordingSessionsController@authorizeRecordingSession');

 /*
 |--------------------------------------------------------------------------
 |AUTH PROTECTED ROUTES
 |--------------------------------------------------------------------------
 */
Route::group(['middleware' => 'auth'], function() {

    Route::get('/report-preview', 'Api\ReportsController@frontEndPreview');

    # Account Editor
    Route::get('/account', 'HomeController@index');

    /*
    |--------------------------------------------------------------------------
    |ADMINISTRATOR DASHBOARD /mastermind
    |--------------------------------------------------------------------------
    */
    Route::group(['prefix' => 'mastermind', 'middleware' => 'webVerifyAdmin'], function () {
        Route::get('/patient/{id}/report', 'Api\PatientsController@pdfReport');

        # Vue.js Catch-all
        Route::any('/{any?}/{any2?}/{any3?}', 'Admin\DashboardController@index');

    });

    # System Administration & Debugging
    Route::group([ 'prefix' => 'system', 'middleware' => ['webVerifyAdmin']], function() {
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
        Route::get('php', function(){
            echo phpinfo();
        });
    });

     /*
     |--------------------------------------------------------------------------
     |NETWORK ADMINISTRATOR DASHBOARD /network-portal
     |--------------------------------------------------------------------------
     */
    Route::group(['prefix' => 'network-portal', 'middleware' => ['webVerifyNetworkAdmin']], function() {

        # Vue.js Catch-all
        Route::any('/{any?}/{any2?}/{any3?}', 'NetworkAdmin\DashboardController@index');

    });

    /*
     |--------------------------------------------------------------------------
     | ALL OTHER ROLES /patient-portal
     |--------------------------------------------------------------------------
     */
    Route::group(['prefix' => 'patient-portal', 'middleware' => ['webVerifyPatientPortalAccess']], function() {

        Route::get('/reports/{id}', 'Api\ReportsController@downloadReport');

        # Vue.js Catch-all
        Route::any('/{any?}/{any2?}/{any3?}', 'PatientPortal\DashboardController@index');

    });

});

Route::get('/reports/reportHeader', 'Api\ReportsController@getReportHeader');

Route::get('/reports/reportFooter', 'Api\ReportsController@getReportFooter');