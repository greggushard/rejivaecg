<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/authenticate/patient', 'Api\RecordingSessionsController@authorizeRecordingSession');

Route::group(['prefix' => '/v1', 'middleware' => ['api']], function() {

    # Canadian Provinces
    Route::resource('/provinces', 'Api\ProvincesController');

    Route::resource('/phoneTypes', 'Api\PhoneTypesController');
    Route::resource('/titles', 'Api\TitlesController');
    Route::resource('/statuss', 'Api\StatussController');
    Route::resource('/ethnicitys', 'Api\EthnicitysController');
    Route::resource('/intRevStatuss', 'Api\IntRevStatussController');
    Route::resource('/patientGroups', 'Api\PatientGroupsController');

    # Users Restful Routes
    Route::resource('/users', 'Api\UsersController');
    Route::post('/users/account', 'Api\UsersController@updateAccount');
    Route::get('/users/{id}/locations', 'Api\UsersController@getAvailableLocations');

    # Role Restful Routes
    Route::resource('/roles', 'Api\RolesController');


    /*
    |-----------------------------------------------------------------------
    |NETWORK ROUTES
    |-----------------------------------------------------------------------
    */
    Route::group(['middleware' => 'verifyUserIsInNetwork'], function(){

        # Networks Restful Data
        Route::resource('/networks', 'Api\NetworksController');

        # Return a list of members for a network
        Route::get('/networks/{id}/staff', 'Api\NetworksController@getMembers');

        Route::get('/networks/{id}/patients', 'Api\NetworksController@getPatients');

        Route::get('/networks/{id}/devices', 'Api\NetworksController@getDevices');

        Route::get('/networks/{id}/active_sessions', 'Api\NetworksController@getActiveRecordingSessions');

        Route::get('/networks/{id}/physicians', 'Api\NetworksController@getPhysicians');

        # Assign a user to a Network
        Route::group(['middleware' => 'verifyNetworkAdmin'], function(){

            # Assign a user to a Network
            Route::post('/networks/{id}/assign/{user_id}', 'Api\NetworksController@assignUser');

            Route::get('/networks/{id}/metrics/recordings', 'Api\MetricsController@getNetworkRecordingsMetric');

            Route::get('/networks/{id}/metrics/reports', 'Api\MetricsController@getNetworkReportsMetric');

            Route::get('/networks/{id}/metrics/users', 'Api\MetricsController@getNetworkUserActivityMetric');

            Route::get('/networks/{id}/metrics/activity_log', 'Api\MetricsController@getNetworkActivityLogMetric');

        });

    });

    /*
    |-----------------------------------------------------------------------
    |PRACTICE RESOURCES
    |-----------------------------------------------------------------------
    */
    Route::group(['middleware' => 'verifyUserIsInPractice'], function(){

        # Practices Restful Routes
        Route::resource('/practices', 'Api\PracticesController');

        # Return a list of members for a network
        Route::get('/practices/{id}/staff', 'Api\PracticesController@getStaff');

        # Return a list of members for a network
        Route::post('/practices/{id}/assign/{user_id}', 'Api\PracticesController@assignUser');

    });

    # Devices resource
    Route::resource('/devices', 'Api\DevicesController');

    Route::post('/devices/disable', 'Api\DevicesController@disableDeviceByRadioId');

    # Get current firmware for device
    Route::post('/firmware_check', 'Api\FirmwareController@checkCurrentFirmware');

    # Upgrade to latest firmware for device
    Route::post('/firmware_upgrade', 'Api\FirmwareController@upgradeFirmware');

    # Changed to GET for mobile app
    Route::post('/devices/{id}/session', 'Api\DevicesController@getCurrentSession');

    Route::get('/devices/{id}/current_patient', 'Api\DevicesController@getCurrentPatient');

    Route::resource('/recording_sessions', 'Api\RecordingSessionsController');

    /*
    |-----------------------------------------------------------------------
    |PATIENT RESOURCES
    |-----------------------------------------------------------------------
    */
    Route::group(['middleware' => 'verifyUserHasPatient'], function(){

        Route::resource('/patient/{id}/report', 'Api\PatientsController@pdfReport');

        # Patients Restful Data
        Route::resource('/patients', 'Api\PatientsController');

        # Return a full manifest of patient data for the edit form
        Route::get('/patients/{id}/edit', 'Api\PatientsController@edit');

        Route::get('/patients/{id}/sessions', 'Api\PatientsController@listSessionsByPatient');

        Route::get('/patients/{id}/recordings/{session_id}/{offset?}', 'Api\PatientsController@getRecordingsByPatient');

        Route::resource('/patients/{id}/recordings/{recording_id}/notes', 'Api\NotesController@store');

        Route::post('/interpretations', 'Api\InterpretationsController@createInterpretation');

        Route::put('/interpretations/{id}', 'Api\InterpretationsController@updateInterpretation');

        Route::delete('/interpretations/{id}', 'Api\InterpretationsController@deleteInterpretation');

    });

    /*
    |-----------------------------------------------------------------------
    |REPORTED EVENT RESOURCE
    |-----------------------------------------------------------------------
    */
    Route::post('/events', 'Api\EcgEventsController@storePatientReportedEvent');

    /*
    |-----------------------------------------------------------------------
    |RECORDING RESOURCES
    |-----------------------------------------------------------------------
    */
    Route::resource('/recordings', 'Api\EcgRecordingsController');

    Route::get('/raw/{recording_hash}', 'Api\EcgRecordingsController@calculateRaw');

    Route::get('/notes/categories', 'Api\NotesController@listCategories');

    Route::resource('/ecg_events', 'Api\EcgEventsController');

    Route::post('/recordings/graphs', 'Api\EcgRecordingsController@getRecordingGraphs');

    Route::post('/recordings/events', 'Api\EcgRecordingsController@listRecordingsWithEvent');

    Route::get('/recording/{recording_hash}', 'Api\EcgRecordingsController@getRecordingByHash');

    Route::get('/recording/{id}/events', 'Api\EcgRecordingsController@getRecordingEvents');

    # User Associated Recordings "Strips"
    Route::resource('/strips', 'Api\StripsController');

    # Get strips by a patient ID and a session ID
    Route::get('/strips/{patient_id}/{session_id}', 'Api\StripsController@filterStripsByPatientSession');

    /*
    |-----------------------------------------------------------------------
    |REMEASREUMENT RESOURCES
    |-----------------------------------------------------------------------
    */
    Route::post('/remeasurements', 'Api\RemeasurementsController@store');

    Route::put('/remeasurements/{id}', 'Api\RemeasurementsController@update');

    /*
    |-----------------------------------------------------------------------
    |PROTECTED USER DATA RESTFUL ROUTES BY ID
    |-----------------------------------------------------------------------
    */
    Route::group(['prefix' => '/user/{user_id}'], function () {

        # User notification routes
        Route::resource('/notifications', 'Api\NotificationsController');

        # Clear user notifications
        Route::post('/notifications/all', 'Api\NotificationsController@markAllNotificationsAsRead');

        # Reset a user's password
        Route::put('/reset_password', 'Api\UsersController@resetPassword');

        # Get a users' available locations
        Route::get('/locations', 'Api\UsersController@getUserLocations');

        # Delete a users' location
        Route::delete('/locations/{location_id}', 'Api\UsersController@destroyLocation');

    });

    /*
    |-----------------------------------------------------------------------
    |REFERRALS ROUTES
    |-----------------------------------------------------------------------
    */
    Route::group(['middleware' => 'verifyUserCanRefer'], function(){

        Route::post('/referrals/', 'Api\ReferralsController@store');

        Route::post('/reports/patient_transmission', 'Api\ReportsController@generatePatientTransmissionReport');

        Route::post('/reports/holter_summary', 'Api\ReportsController@generateHolterReport');

        Route::post('/reports/full_disclosure', 'Api\ReportsController@generateFullDisclosureReport');

    });

    # Triage related alerts
    Route::get('/alerts', 'Api\AlertsController@index');

    Route::get('/alerts/patient/{patient_id}', 'Api\AlertsController@getFlaggedAlertsByPatient');

    Route::post('/alerts/clear', 'Api\AlertsController@clearAlerts');

    Route::post('/alerts', 'Api\AlertsController@storeAlert');

    Route::post('/clips', 'Api\ClipsController@store');

    Route::get('/clips/patient/{patient_id}', 'Api\ClipsController@getClipsByPatient');

    # Return configs
    Route::get('/configs', 'Api\PatientsController@getConfigurations');

});
